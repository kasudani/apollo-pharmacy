define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function ($, customerData) {
	

      var updateCart = function(evt) {
 
          var form = $('form#form-validate');
          $.ajax({
              url: form.attr('action'),
              data: form.serialize(),
              showLoader: true,
              success: function (res) {
                  var parsedResponse = $.parseHTML(res);
                  var result = $(parsedResponse).find("#form-validate");
                  var priceCount = $(parsedResponse).find(".price-count");
                  var amountPaybil = $(parsedResponse).find(".amount-paybil");
                  var sections = ['cart'];

                  $("#form-validate").replaceWith(result);
                  $(".price-count").replaceWith(priceCount);
                  $(".amount-paybil").replaceWith(amountPaybil);


                  // The mini cart reloading
                  customerData.reload(sections, true);

                  showLoader: false;

                  $(".plus").on("click", function(){
                     $(this).prev().val(parseInt($(this).prev().val())+1);
                     updateCart();
                     //setTimeout(function(){ $("#update_cart_action").click(); }, 3000);
                  });
                  $(".minus").on("click", function(){
                    if(parseInt($(this).next().val()) > 1){
                       $(this).next().val(parseInt($(this).next().val())-1);

                        updateCart();
                       //setTimeout(function(){ $("#update_cart_action").click(); }, 3000);
                     }
                  });

                  // The totals summary block reloading
                  //var deferred = $.Deferred();
                  //getTotalsAction([], deferred);
              },
              error: function (xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                  console.log(err.Message);
                  showLoader: false;
              }
          });
      }


	

    $(document).ready(function($) {

      $(".plus").on("click", function(){
         $(this).prev().val(parseInt($(this).prev().val())+1);
         updateCart();
         //setTimeout(function(){ $("#update_cart_action").click(); }, 3000);
		  });
      $(".minus").on("click", function(){
        if(parseInt($(this).next().val()) > 1){
           $(this).next().val(parseInt($(this).next().val())-1);

            updateCart();
           //setTimeout(function(){ $("#update_cart_action").click(); }, 3000);
         }
      });
		
    });





});