<?php

/*
  @author -prajakta.shirsat@wwindia.com
  @description - Defined to get the system config values and data from models
 */

namespace Netcore\Smartech\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    protected $_quoteItems;
    protected $_orderItems;
    protected $_customer;
    private $_scriptLocation;
    public function __construct(
        \Netcore\Smartech\Model\Customer $customer,
        \Netcore\Smartech\Model\QuoteItems $quoteItems,
        \Netcore\Smartech\Model\OrderItems $orderItems,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_quoteItems = $quoteItems;
        $this->_orderItems = $orderItems;
        $this->_scopeConfig = $scopeConfig;
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        $this->_scriptLocation = "//tw.netcore.co.in/smartechclient.js";
    }

    /* Get the web tracking id enabled */

    public function getWebTrackEnable()
    {
        $isEnable = $this->_scopeConfig->getValue('smartech/general/enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $isEnable;
    }

    /* Get the assigned web tracking id value from config assigned field */

    public function getWebId()
    {
        $webId = $this->_scopeConfig->getValue('smartech/general/web_tracking_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $webId;
    }

    /* Get the assigned site tracking id value from config assigned field */

    public function getSiteId()
    {
        $siteId = $this->_scopeConfig->getValue('smartech/general/site_tracking_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $siteId;
    }

    /* Get Registration primary field  */

    public function getRegisterionId()
    {
        $siteId = $this->_scopeConfig->getValue('smartech/general/registration_primary_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return strtolower($siteId);
    }

    /* Get bpn notification flag */

    public function getBpnTrackEnable()
    {
        $bpnIsEnable = $this->_scopeConfig->getValue('smartech/push_notification/bpn_enable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $bpnIsEnable;
    }

    /* Get bpn sender id */

    public function getBpnSenderId()
    {
        $bpnSenderId = $this->_scopeConfig->getValue('smartech/push_notification/bpn_sender_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $bpnSenderId;
    }

    /* Get bpn api key */

    public function getBpnApiKey()
    {
        $bpnApiKey = $this->_scopeConfig->getValue('smartech/push_notification/bpn_api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $bpnApiKey;
    }

    /* Get tracker script to include */

    public function getTrackingScript()
    {
        $str = '';
        $webTrackinId = $this->getWebId();
        $getSiteId = $this->getSiteId();
        // Get customer key identity 
        $str = '<script src="' . $this->_scriptLocation . '"></script>
				<script>smartech("create","' . $webTrackinId . '");
				smartech("register","' . $getSiteId . '");
				</script>';
        return $str;
    }

    // Page Browse
    public function getScriptPageBrowse($track)
    {
        $str = '';
        $identity = $this->getCustomerIdentity();
        $data['s^action'] = $track['action'];
        $json = json_encode($data);
        $str = "<script>"
            . "smartech('identify', '" . $identity . "');"
            . "smartech('dispatch', 1 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Page load success');"
            . "</script>";
        return $str;
    }

    // Add to cart
    public function getScriptAddToCart($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 2 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Add to cart success');"
            . "</script>";
        return $str;
    }

    // Remove from cart
    public function getScriptRemoveFromCart($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 5 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Remove from cart success');"
            . "</script>";
        return $str;
    }

    // Checkout
    public function getScriptCheckout($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 3 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Checkout success');"
            . "</script>";
        return $str;
    }

    // Product purchase
    public function getScriptPurchase($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 30 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Order success');"
            . "</script>";
        return $str;
    }

    // Add to wishlist
    public function getScriptWishlistAdd($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 35 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Add to wishlist success');"
            . "</script>";
        return $str;
    }

    // Remove from wishlist
    public function getScriptWishlistRemove($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 36 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Remove from wishlist success');"
            . "</script>";
        return $str;
    }

    // Product search
    public function getScriptSearch($json)
    {
        $str = '';
        $str = "<script>"
            . "smartech('dispatch', 37 , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Search success');"
            . "</script>";
        return $str;
    }

    // Contact
    public function getContactTrack($json)
    {
        $str = '';
        $listId = 5;
        $str = "<script>"
            . "smartech('contact', " . $listId . " , " . $json . ");"
            . "console.log(" . $json . ");"
            . "console.log('Customer Register Success');"
            . "</script>";
        return $str;
    }

    /* To get product data from product id */

    public function getProductData($product_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
        return $product;
    }

    /* To get product data from product sku */

    public function getProductDataFromSku($sku)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        $product = $productRepository->get($sku);
        return $product;
    }

    /* Get category name from id */

    public function getCategoryName($catId)
    {
        $catName = '';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $catName = $objectManager->create('Magento\Catalog\Model\Category')->load($catId)->getName();
        return $catName;
    }

    public function getSearchJson($search)
    {
        $data['s^type'] = 'Search';
        $data['s^key'] = $search;
        $json = json_encode($data);
        return $json;
    }

    /* Build checkout json to pass */

    public function getCheckoutJson($quoteId)
    {
        $json = '';
        $getAllItems = $this->_quoteItems->getQuoteItems($quoteId);
        $data['s^event'] = 'Checkout event';
        foreach ($getAllItems as $key => $item) {
            $product = $this->getProductData($item->getProductId());
            $data[$key]['i^prid'] = $product->getId();
            $data[$key]['s^name'] = $this->getStringReplacedText($product->getName());
            $data[$key]['f^price'] = $product->getPrice();
            $data[$key]['i^qty'] = (int) $item->getQty();
            $categoryIds = $product->getCategoryIds();
            $data[$key]['s^category'] = $this->getCategoryName($categoryIds[0]);
        }
        if (!empty($data)) {
            $json = json_encode($data);
        }
        return $json;
    }

    /* Build order purchase json to pass */

    public function getPurchaseOrderJson($orderId)
    {
        $json = '';
        $order = $this->_orderItems->getOrder($orderId);
        $getAllItems = $this->_orderItems->getOrderItems($orderId);
        $orderData['i^orderId'] = $order->getIncrementId();
        $orderData['f^amount'] = $order->getBasegrandTotal();
        $orderData['s^currency'] = $order->getOrderCurrencyCode();
        $orderData['s^dateofpurchase'] = $order->getCreatedAt();
        foreach ($getAllItems as $key => $item) {
            $product = $this->getProductData($item->getProductId());
            $productData[$key]['i^prid'] = $product->getId();
            $productData[$key]['s^name'] = $this->getStringReplacedText($product->getName());
            $productData[$key]['f^price'] = $product->getPrice();
            $productData[$key]['i^qty'] = (int) $item->getQty();
            $categoryIds = $product->getCategoryIds();
            $productData[$key]['s^category'] = $this->getCategoryName($categoryIds[0]);
        }
        $orderData['items'] = $productData;
        if (!empty($orderData)) {
            $json = json_encode($orderData);
        }
        return $json;
    }

    /* Build customer registration json data */

    public function getRegisterJson($email)
    {
        $json = '';
        // Get customer's smartech identity key set
        $key = $this->getSmartechCustomerIdentityKey();
        $customerData = $this->_customer->getCustomer($email);
        $primaryKey = 'pk^' . $key;
        $data[$primaryKey] = $customerData->getData($key);
        $data['FIRST_NAME'] = $customerData->getFirstname();
        $data['LAST_NAME'] = $customerData->getLastname();
        $json = json_encode($data);
        return $json;
    }

    /* Build cart product json data */

    public function getCartJson($prod, $type = '', $qty = 1)
    {
        $json = '';
        $product = $this->getProductData($prod);
        $data['i^prid'] = $product->getId();
        $data['s^name'] = $this->getStringReplacedText($product->getName());
        $data['s^brand'] = '';
        $data['s^variant'] = '';
        $categoryIds = $product->getCategoryIds();
        $data['s^category'] = $this->getCategoryName($categoryIds[0]);
        $data['f^price'] = $product->getPrice();
        $data['i^prqt'] = $type . $qty;
        $data['s^currency'] = $this->getStoreCurrency();
        $json = json_encode($data);
        return $json;
    }

    /* To get product id from item id */

    public function getProductFromItem($id)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $itemData = $_objectManager->create('Magento\Quote\Model\Quote\Item')->load($id);
        $product = $itemData->getData();
        return $product['product_id'];
    }

    /* To get quote item qty */

    public function getQuoteItemQty($id)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $itemData = $_objectManager->create('Magento\Quote\Model\Quote\Item')->load($id);
        $qty = (int) $itemData->getQty();
        return $qty;
    }

    /* Get store currency */

    public function getStoreCurrency()
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }

    /* To check if customer logged in */

    public function isCustomerLoggedIn()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn()) {
            return $customerSession->getCustomer();
        }
        return '';
    }

    /* To fetch customer's identity field */

    public function getCustomerIdentity()
    {
        $identity = ''; // The blank value is not considered 
        $customer = $this->isCustomerLoggedIn();
        if ($customer) {
            $key = $this->getSmartechCustomerIdentityKey();
            $identity = $customer->getData($key);
        }
        return $identity;
    }

    /* get Customer identity key */

    public function getSmartechCustomerIdentityKey()
    {
        $key = 'email';
        if ($this->getRegisterionId() != '') {
            if (strpos($this->getRegisterionId(), 'mobile') !== false) {
                $key = $this->getRegisterionId();
            } elseif (strpos($this->getRegisterionId(), 'id') !== false) {
                $key = 'entity_id';
            }
        }
        return $key;
    }

    /* get String replaced text */
    /* issue arrised because % symbole was throwing error in sprintf() */
    public function getStringReplacedText($string)
    {
        return str_replace("%", "%%", $string);
    }


}
