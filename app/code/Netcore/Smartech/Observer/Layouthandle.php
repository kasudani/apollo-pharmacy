<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Layouthandle implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * @var
     */
    protected $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    protected $_queryFactory;
    protected $_request;
    protected $_catalogSession;
    protected $_customerSession;
    protected $_checkoutSession;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Search\Model\QueryFactory $queryFactory,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->_queryFactory = $queryFactory;
        $this->_catalogSession = $catalogSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_sessionManager = $sessionManager;
        $this->helper = $helper;
        $this->config = $config;
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /* Event handled here for delete cart item & add cart item ajax call */

    public function execute(EventObserver $observer)
    {

        if ($this->_request->getFullActionName() == 'customer_section_load') {
            $prod = $this->getCatalogSession()->getSmacProd();
            $getCookie = $this->_cookieManager->getCookie("Addcartflag");
            if ($prod && $getCookie && $getCookie == 'addcartsuccess') {
                $json = $this->helper->getCartJson($prod);
                // Handled for ajax add to cart success thorugh custom js 
                if ($getCookie === "addcartsuccess") {
                    $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()
                        ->setDuration(259200)
                        ->setPath($this->_sessionManager->getCookiePath())
                        ->setDomain($this->_sessionManager->getCookieDomain())
                        ->setHttpOnly(false);

                    $this->_cookieManager->setPublicCookie("Addcartflag", 'addcartajax', $publicCookieMetadata
                    );

                    $this->_cookieManager->setPublicCookie("SmCartJson", $json, $publicCookieMetadata
                    );
                    $this->getCatalogSession()->unsSmacProd();
                }
            }
        }
    }

}