<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Pageviewdispatch implements ObserverInterface
{    

	/**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;
     /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $_response;
    protected $_request;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Framework\App\ResponseFactory $_response,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\UrlInterface $urlInterface,
        Config $config
    )
    {
        $this->_response = $_response;
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->config = $config;
        $this->_urlInterface = $urlInterface;
    }

    public function execute(EventObserver $observer)
    {
        
    }

}