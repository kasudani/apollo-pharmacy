<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Registersuccessdispatch implements ObserverInterface
{    

    /* Netcore Smartech Api */
    CONST API_KEY = '43d4b598b63becd8952b2e2ce1978344';
    CONST TYPE = 'contact';
    CONST ACTIVITY = 'addsync';
    CONST LIST_ID = '2'; // It is specific data container for new registration / user..

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;

    protected $_request;

    protected $_curl;

    protected $_customerModel;

    protected $_session;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Customer\Model\Session $session,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->config = $config;
        $this->_curl = $curl;
        $this->_customerModel = $customerModel;
        $this->_session = $session;
    }

    public function execute(EventObserver $observer)
    {
            
        /* Customer Information */    
        $customerInfo = $observer->getEvent()->getCustomer();
        $email = $customerInfo->getEmail();        

        /** 
         *  Original Code 
         *  =============
         */

        /*$email = $observer->getEvent()->getCustomer()->getEmail();

        if ($email) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
            $customerSession->setEmail($email);
        }*/

        /** 
         *  Testing Code 
         *  =============
         */

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/customer_registration_observer.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        

        /* logger started */
        $logger->info(' ');
        $logger->info('customer_registration_observer_started');
        $logger->info('==========================');
        
        if ($email) {

            $customerId = $customerInfo->getId();
            $firstName = $customerInfo->getFirstName();
            $lastName = $customerInfo->getLastName();

            # load customer by id to get the mobile number
            $customerData = $this->_customerModel->load($customerId);
            $mobileNumber = $customerData->getMobileNumber();

            /* Print log */
            $logger->info('customerId =>'.$customerId);
                    
            // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            // $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
            $this->_session->setEmail($email);

            
            /* Data params */
            // CUST_ID, FIRSTNAME, LASTNAME, EMAIL, MOBILE

            $DATA = '{"CUST_ID":"'.$customerId.'","FNAME": "'.$firstName.'", "LNAME": "'.$lastName.'","EMAIL": "'.$email.'", "MOBILE": "'.$mobileNumber.'"}';
            
            /* API URL */
            $api = 'http://api.netcoresmartech.com/apiv2?apikey='.self::API_KEY.'&type='.self::TYPE.'&activity='.self::ACTIVITY.'&data='.$DATA.'&listid='.self::LIST_ID.' ';
            
            /* Form the CURL request */
            try 
            {
                $curl_url = curl_init($api);

                curl_setopt($curl_url, CURLOPT_POST, TRUE);
                curl_setopt($curl_url, CURLOPT_RETURNTRANSFER, true);

                $curl_response1 = curl_exec($curl_url);

                /* Print log response */
                $logger->info($curl_response1); 
                $logger->info($DATA);
                $logger->info($api); 

                
            } 
            catch (\Exception $e) 
            {
                $logger->info($e->getMessage());
            }

            /* 
            $url = "http://api.netcoresmartech.com/apiv2?";
            //if the method is get
            $this->_curl->get($url);
            //if the method is post
            $this->_curl->post($url, $params);
            //response will contain the output in form of JSON string
            $response = $this->_curl->getBody();

            // $responseArr = $response->__toArray();
            // $em  =  array();
            // $this->_logger->addDebug($response,$em);

            $logger->info($response);
            */

        }

        /* logger ended */
        $logger->info('==========================');
        $logger->info('customer_registration_observer_ended');

        return $this;
    }

}