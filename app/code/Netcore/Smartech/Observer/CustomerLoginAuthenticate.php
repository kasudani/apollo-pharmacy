<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerLoginAuthenticate implements ObserverInterface
{
	/* Netcore Smartech Api */
    CONST API_KEY = '43d4b598b63becd8952b2e2ce1978344';
    CONST TYPE = 'contact';
    CONST ACTIVITY = 'addsync';
    CONST LIST_ID = '3'; // It is specific data container for new registration / user..

    protected $_curl;

    protected $_customerModel;

    protected $_session;

    
    public function __construct( 
    	\Magento\Customer\Model\Session $session,
    	\Magento\Customer\Model\Customer $customerModel,       
        \Magento\Framework\HTTP\Client\Curl $curl
    )
    {
        $this->_curl = $curl;
        $this->_customerModel = $customerModel;
        $this->_session = $session;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // $customer = $observer->getEvent()->getCustomer();

        // echo $customerInfo->getEmail();
        
        //return $this;
       
    }
}