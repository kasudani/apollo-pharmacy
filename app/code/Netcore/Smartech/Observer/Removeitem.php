<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

use \Magento\Checkout\Model\Session as CheckoutSession;

class Removeitem implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;

    /**
     * @var
     */
    protected $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $_cookieMetadataFactory;

    protected $_sessionManager;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;


    /**
     * The affiliate cookie name
     */
    const COOKIE_NAME = "Removecartflag";

    /** @var CheckoutSession */
    protected $_checkoutSession;

    protected $_request;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_registry = $registry;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->config = $config;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_sessionManager = $sessionManager;
        $this->_checkoutSession = $checkoutSession;
        $this->_catalogSession = $catalogSession;
        $this->helper = $helper;
    }

    public function execute(EventObserver $observer)
    {

        $item = $observer->getEvent()->getQuoteItem();
        $qty = $item->getQty();
        $sku = $item->getSku();
        $product = $this->helper->getProductDataFromSku($sku);
        if ($product->getData() != '') {
            $this->_catalogSession->setDeleteProd($product->getId());

            // Handled for ajax add to cart success thorugh custom js 
            $cartAction = 'removetItemSidebar';
            if ($cartAction) {
                $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()
                    ->setDuration(259200)
                    ->setPath($this->_sessionManager->getCookiePath())
                    ->setDomain($this->_sessionManager->getCookieDomain())
                    ->setHttpOnly(false);

                $json = $this->helper->getCartJson($product->getId(), '-', $qty);

                $this->_cookieManager->setPublicCookie("SmRemoveCartJson", $json, $publicCookieMetadata
                );

                $this->_cookieManager->setPublicCookie(self::COOKIE_NAME, $cartAction, $publicCookieMetadata
                );
            }
        }
    }

}