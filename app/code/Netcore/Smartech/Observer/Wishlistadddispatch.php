<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Wishlistadddispatch implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    protected $_queryFactory;
    protected $_request;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Search\Model\QueryFactory $queryFactory,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_eventManager = $eventManager;
        $this->_queryFactory = $queryFactory;
        $this->config = $config;
    }

    public function execute(EventObserver $observer)
    {
        $product = '';
        $product = $observer->getEvent()->getItem()->getProductId();
        if ($product) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $catalogSession = $objectManager->get('\Magento\Catalog\Model\Session');
            $catalogSession->setSmWAddProd($product);
        }
    }

}