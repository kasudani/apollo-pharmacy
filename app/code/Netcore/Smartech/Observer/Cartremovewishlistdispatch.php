<?php

namespace Netcore\Smartech\Observer;

use Magento\Framework\View\Page\Config;

use Magento\Framework\Event\Observer as EventObserver;

use Magento\Framework\Event\ObserverInterface;

class Cartremovewishlistdispatch implements ObserverInterface
{    

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    protected $_eventManager;
    protected $_queryFactory;
    protected $_request;
    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Search\Model\QueryFactory $queryFactory,
        Config $config
    )
    {
        $this->_layout = $layout;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_catalogSession = $catalogSession;
        $this->_eventManager = $eventManager;
        $this->_queryFactory = $queryFactory;
        $this->config = $config;
    }

    /* Remove item from cart and move to wishlist */

    public function execute(EventObserver $observer)
    {
        $productId = '';
        $item = $this->_request->getParam('item');
        if ($item) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $quoteItem = $objectManager->create(\Magento\Quote\Model\Quote\Item::class);
            $itemData = $quoteItem->load($item);
            $productId = $itemData->getProductId();
            $qty = $itemData->getQty();
            if ($productId) {
                $this->_catalogSession->setSmWAddQty($qty);
                $this->_catalogSession->setCartRemove('remove');
                $this->_catalogSession->setDeleteProd($productId);
                $this->_catalogSession->setSmWAddProd($productId);
                $this->_catalogSession->setCartRemoveQty($qty);
            }
        }
    }

}
