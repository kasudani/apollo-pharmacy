<?php
namespace Netcore\Smartech\Block;
 
class Register extends \Magento\Framework\View\Element\Template
{
    protected $_urlInterface;
    protected $_catalogSession;
    protected $_customerSession;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Title $pageTitle,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_request = $request;
        $this->_pageTitle = $pageTitle;
        $this->_urlInterface = $urlInterface;
        $this->_catalogSession = $catalogSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->addData(array('cache_lifetime' => false));
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    public function executeRegisterJs()
    {
        $str = '';
        $email = $this->getCustomerSession()->getEmail();
        if ($email) {
            $json = $this->helper->getRegisterJson($email);
            $str = $this->helper->getContactTrack($json);
            $this->getCustomerSession()->unsEmail();
            return sprintf($str);
        } else {
            return '';
        }
    }

}

