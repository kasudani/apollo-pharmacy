<?php
namespace Netcore\Smartech\Block;
 
class Addtocartdispatch extends \Magento\Framework\View\Element\Template
{
    protected $_urlInterface;
    protected $_catalogSession;
    protected $_customerSession;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Title $pageTitle,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_request = $request;
        $this->_pageTitle = $pageTitle;
        $this->_urlInterface = $urlInterface;
        $this->_catalogSession = $catalogSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->addData(array('cache_lifetime' => false));
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    public function executeAddToCartJs()
    {
        $prod = $getCookie = $json = '';
        $qty = 1;

        $prod = $this->getCatalogSession()->getSmacProd();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cookieManager = $objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
        if ($this->getCatalogSession()->getSmacQty() > 1) {
            $qty = $this->getCatalogSession()->getSmacQty();
        }
        $getCookie = $cookieManager->getCookie("Addcartflag");
        if ($prod) {
            $json = $this->helper->getCartJson($prod, '', $qty);
            // Call handled for add to cart page redirect success event
            if ($getCookie == 'addcartsuccess') {
                $this->getCatalogSession()->unsSmacProd();
                $this->getCatalogSession()->unsSmacQty();
                $cookieManager->deleteCookie('SmCartJson');
                $cookieManager->deleteCookie('Addcartflag');
                $str = $this->helper->getScriptAddToCart($json);
                return sprintf($str);
            }
        }
    }

}

