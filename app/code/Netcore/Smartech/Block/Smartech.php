<?php

namespace Netcore\Smartech\Block;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;

class Smartech extends \Magento\Framework\View\Element\Template
{

    protected $_urlInterface;

    /**
     * Name of cookie that holds private content version
     */
    const BPN_SENDER = '';
    const BPN_API_KEY = '';
    const SM_WEB_ID = '';
    const SM_SITE_ID = '';

    /**
     * CookieManager
     *
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Title $pageTitle,
        \Magento\Framework\UrlInterface $urlInterface,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Filesystem\Io\File $file,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_request = $request;
        $this->_pageTitle = $pageTitle;
        $this->_urlInterface = $urlInterface;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->_dir = $dir->getRoot();
        $this->_file = $file;
        $this->addData(array('cache_lifetime' => false));
        parent::__construct($context, $data);
    }

    /**
     * Get form key cookie
     *
     * @return string
     */
    public function get()
    {
        return $this->cookieManager->getCookie(self::COOKIE_NAME);
    }

    /**
     * @param string $value
     * @param int $duration
     * @return void
     */
    public function set($cookieName, $value, $duration = 86400)
    {
        $metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration($duration)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie($cookieName, $value, $metadata);
    }

    /* General smartech function cal */

    public function getSmartechTxt()
    {
        return 'Smartech cal!';
    }

    /*
      @description - fetch smartech track details for page browse event
     */

    public function getTrackDetails()
    {
        $data['is_enable'] = $this->helper->getWebTrackEnable();
        $data['webId'] = $this->helper->getWebId();
        $data['siteId'] = $this->helper->getSiteId();
        $data['title'] = $this->_pageTitle->getShort();
        $data['action'] = $this->_request->getFullActionName();
        $data['url'] = $this->_urlInterface->getCurrentUrl();
        if ($this->helper->getBpnTrackEnable() && $this->helper->getWebTrackEnable()) {
            $this->writeFile('sw.js', $this->_dir);
        }
        return $data;
    }

    /* BPN data fetch code from backend */

    public function addBpnPushJs()
    {
        $track = $this->getTrackDetails();
        if ($this->helper->getBpnTrackEnable()) {
            $data['bpn_sender_id'] = $this->helper->getBpnSenderId();
            $data['bpn_api_key'] = $this->helper->getBpnApiKey();
            return $data;
        }
    }

    /*
      @description - Handle created to generate file
     */

    public function writeFile($fileName, $dir)
    {
        if ($this->helper->getBpnTrackEnable() && $this->helper->getWebTrackEnable()) {
            $content = $this->geterateBpnScriptFile();
            $filePath = "/" . $fileName;
            if (file_exists($dir . '/' . $fileName)) {
                unlink($dir . $filePath);
            }
            $ioAdapter = $this->_file;
            $ioAdapter->open(array('path' => $dir));
            $ioAdapter->write($fileName, $content, 0644);
            return;
        }
    }

    /*
      @description - function added to get generate the push notification script in the root
     */

    function geterateBpnScriptFile()
    {
        $bpnData['bpn_apiKey'] = $this->helper->getBpnApiKey();
        $bpnData['bpn_sender_id'] = $this->helper->getBpnSenderId();
        $bpnData['sm_web_id'] = $this->helper->getWebId();
        $bpnData['sm_site_id'] = $this->helper->getSiteId();
        $bpnData['includeScript'] = "importScripts('http://tw.netcore.co.in/sw.js');";
        $script = "var config = {
            apiKey: '" . $bpnData['bpn_apiKey'] . "',
            messagingSenderId: '" . $bpnData['bpn_sender_id'] . "',
            user_key: '" . $bpnData['sm_web_id'] . "',
            siteid: '" . $bpnData['sm_site_id'] . "',
        };" . PHP_EOL .
            $bpnData['includeScript'];
        return $script;
    }

}
