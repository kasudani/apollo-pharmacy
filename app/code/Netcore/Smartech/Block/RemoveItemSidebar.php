<?php
namespace Netcore\Smartech\Block;
 
class RemoveItemSidebar extends \Magento\Framework\View\Element\Template
{
    protected $_urlInterface;
    protected $_catalogSession;
    protected $_customerSession;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Netcore\Smartech\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Page\Title $pageTitle,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_request = $request;
        $this->_pageTitle = $pageTitle;
        $this->_urlInterface = $urlInterface;
        $this->_catalogSession = $catalogSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->addData(array('cache_lifetime' => false));
        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCatalogSession()
    {
        return $this->_catalogSession;
    }

    public function getCustomerSession()
    {
        return $this->_customerSession;
    }

    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    public function executeRemoveToCartJs()
    {
        // Call made to handle remove item from cart page with page load event
        $removeFlag = $this->getCatalogSession()->getCartRemove();
        if ($removeFlag == 'remove') {
            $this->getCatalogSession()->unsCartRemove();
            $qty = $this->getCatalogSession()->getCartRemoveQty();
            $productId = $this->getCatalogSession()->getDeleteProd();
            $json = $this->helper->getCartJson($productId, '-', $qty);
            $this->getCatalogSession()->unsCartQuoteItem();
            $this->getCatalogSession()->unsDeleteProd();
            $this->getCatalogSession()->unsCartRemoveQty();
            $str = $this->helper->getScriptRemoveFromCart($json);
            return sprintf($str);
        }
    }

}

