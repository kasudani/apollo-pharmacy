<?php

namespace Netcore\Smartech\Model;

class QuoteItems
{
    protected $_quoteRepository;

    public function __construct(\Magento\Quote\Model\QuoteRepository $quoteRepository)
    {
        $this->_quoteRepository = $quoteRepository;
    }

    public function getQuoteItems($quoteId)
    {
        try {
            $quote = $this->_quoteRepository->get($quoteId);
            $items = $quote->getAllItems();
            return $items;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return [];
        }
    }

}
