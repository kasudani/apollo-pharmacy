<?php
/**
 * Copyright © 2016 Jake Sharp (http://www.jakesharp.co/) All rights reserved.
 */

namespace JakeSharp\Productslider\Block\Slider;


class Items extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * Max number of products in slider
     */
    const MAX_PRODUCTS_COUNT = 20;

    /**
     * Products collection factory
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productsCollectionFactory;

    /**
     * Product reports collection factory
     *
     * @var \Magento\Reports\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_reportsCollectionFactory;

    /**
     * Product slider factory
     *
     * @var \JakeSharp\Productslider\Model\ProductsliderFactory
     */
    protected $_sliderFactory;

    /**
     * Product slider id
     *
     * @var int
     */
    protected $_sliderId;

    /**
     * Product slider model
     *
     * @var \JakeSharp\Productslider\Model\Productslider
     */
    protected $_slider;

    /**
     * Events type factory
     *
     * @var \Magento\Reports\Model\Event\TypeFactory
     */
    protected $_eventTypeFactory;

    /**
     * Products visibility
     *
     * @var \Magento\Reports\Model\Event\TypeFactory
     */
    protected $_catalogProductVisibility;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_dateTime;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Product slider template
     */
    protected $_template = 'JakeSharp_Productslider::slider/items.phtml';

    /**
     * @var
     */
    protected $_productsNumber;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productsCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportsCollectionFactory
     * @param \JakeSharp\Productslider\Model\ProductsliderFactory $productsliderFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Reports\Model\Event\TypeFactory $eventTypeFactory
     * @param array $data
     */
    public function __construct
    (
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productsCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportsCollectionFactory,
        \JakeSharp\Productslider\Model\ProductsliderFactory $productsliderFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Reports\Model\Event\TypeFactory $eventTypeFactory,
        array $data = []
    ){

        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$_SERVER['HTTP_USER_AGENT'])||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($_SERVER['HTTP_USER_AGENT'],0,4))) {
           
           $this->_template = 'JakeSharp_Productslider::slider/mobile-items.phtml';
        }

        $this->_productCollectionFactory = $productsCollectionFactory;
        $this->_reportsCollectionFactory = $reportsCollectionFactory;
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->_sliderFactory = $productsliderFactory;
        $this->_dateTime = $dateTime;
        $this->_eventTypeFactory = $eventTypeFactory;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context,$data);
    }

    /**
     * @param $type
     * @return Collection|\Magento\Catalog\Model\ResourceModel\Product\Collection|string
     */
    public function getSliderProducts($type)
    {
        $collection = "";
        switch($type){
            case 'new':
                $collection =  $this->_getNewProducts($this->_productCollectionFactory->create());
                break;
            case 'bestsellers':
                $collection = $this->_getBestsellersProducts($this->_productCollectionFactory->create());
                break;
            case 'mostviewed':
                $collection =  $this->_getMostViewedProducts($this->_productCollectionFactory->create());
                break;
            case 'onsale':
                $collection =  $this->_getOnSaleProducts($this->_productCollectionFactory->create());
                break;
            case 'featured':
                $collection =  $this->_getSliderFeaturedProducts($this->_productCollectionFactory->create());
                break;
            case 'autorelated':
                $collection =  $this->_getAutoRelatedProducts($this->_productCollectionFactory->create());
                break;
        }

        return $collection;
    }

    /**
     * Get additional-featured slider products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getSliderFeaturedProducts($collection)
    {
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()
                    ->join(['slider_products' => $collection->getTable('js_productslider_product')],
                            'e.entity_id = slider_products.product_id AND slider_products.slider_id = '.$this->getSliderId(),
                            ['position'])
                    ->order('slider_products.position');
        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);

        $this->_productsNumber = $this->getProductsCount() - $collection->count();

        \Magento\Framework\App\ObjectManager::getInstance()
             ->get('Psr\Log\LoggerInterface')
             ->debug("this->_productsNumber = ".$this->_productsNumber);

        return $collection;
    }

    /**
     * Get product slider items based on type
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getNewProducts($collection)
    {
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToFilter(
                'news_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left')
            ->addAttributeToFilter(
                'news_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left')
            ->addAttributeToSort(
               'news_from_date',
               'desc')
            ->addStoreFilter($this->getStoreId())
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);

        return $collection;
    }

    /**
     * Get most viewed products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return Collection
     */
    protected function _getMostViewedProducts($collection)
    {
        $eventTypes = $this->_eventTypeFactory->create()->getCollection();
        $reportCollection = $this->_reportsCollectionFactory->create();

        // Getting event type id for catalog_product_view event
        foreach ($eventTypes as $eventType) {
            if ($eventType->getEventName() == 'catalog_product_view') {
                $productViewEvent = (int)$eventType->getId();
                break;
            }
        }

        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()->reset()->from(
                    ['report_table_views' => $reportCollection->getTable('report_event')],
                    ['views' => 'COUNT(report_table_views.event_id)']
                )->join(
                    ['e' => $reportCollection->getProductEntityTableName()],
                    $reportCollection->getConnection()->quoteInto(
                        'e.entity_id = report_table_views.object_id',
                        $reportCollection->getProductAttributeSetId()
                    )
                )->where(
                    'report_table_views.event_type_id = ?',
                    $productViewEvent
                )->group(
                    'e.entity_id'
                )->order(
                    'views DESC'
                )->having(
                    'COUNT(report_table_views.event_id) > ?',
                    0
                );

        $collection->addStoreFilter($this->getStoreId())
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
//            ->addViewsCount()

        return $collection;
    }

    /**
     * Get on sale slider products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getOnSaleProducts($collection)
    {
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToFilter(
                'special_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left')
            ->addAttributeToFilter(
                'special_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left')
            ->addAttributeToSort(
                'news_from_date',
                'desc')
            ->addStoreFilter($this->getStoreId())
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);

        return $collection;
    }

    /**
     * Get best selling products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getBestsellersProducts($collection)
    {
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()
                    ->join(['bestsellers' => $collection->getTable('sales_bestsellers_aggregated_yearly')],
                                'e.entity_id = bestsellers.product_id AND bestsellers.store_id = '.$this->getStoreId(),
                                ['qty_ordered','rating_pos'])
                    ->order('rating_pos');
        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);

        return $collection;
    }

    /**
     * @param $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getAutoRelatedProducts($collection)
    {
        $product = $this->getProduct();

        if(!$product){
            return;
        }

        $categories = $this->getProduct()->getCategoryIds();

        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->addCategoriesFilter(['in' => $categories]);

        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);
        \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Psr\Log\LoggerInterface')
            ->debug($this->getProductsCount());

        $collection->addAttributeToFilter('entity_id', array('neq' => $product->getId()));
        $collection->getSelect()->order('rand()');

        return $collection;
    }

    /**
     * Get slider products including additional products
     *
     * @return array
     */
    public function getSliderProductsCollection()
    {
        $collection = [];
        $type = $this->_slider->getType();

        $featuredProducts = $this->getSliderProducts("featured");
        if(count($featuredProducts)>0){
            $collection['featured'] = $featuredProducts;
        }

        if($type !== "featured"){
            if($this->_productsNumber > 0){
                $sliderProducts = $this->getSliderProducts($type);
                if(count($sliderProducts)>0){
                    $collection['products'] = $sliderProducts;
                }
            }
        }

        return $collection;
    }

    /**
     * Get start of day date
     *
     * @return string
     */
    public function getStartOfDayDate()
    {
        return $this->_localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
    }

    /**
     * Get end of day date
     *
     * @return string
     */
    public function getEndOfDayDate()
    {
        return $this->_localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');
    }

    /**
     * Set slider model
     *
     * @param \JakeSharp\Productslider\Model\Productslider $slider
     *
     * @return this
     */
    public function setSlider($slider)
    {
        $this->_slider = $slider;
        return $this;
    }

    /**
     * Get slider id
     *
     * @return int
     */
    public function getSliderId()
    {
        return $this->_slider->getId();
    }

    /**
     * Get slider
     *
     * @return \JakeSharp\Productslider\Model\Productslider
     */
    public function getSlider()
    {
        return $this->_slider;
    }

    /**
     * @param int
     *
     * @return this
     */
    public function setSliderId($sliderId)
    {
        $this->_sliderId = $sliderId;
        $slider = $this->_sliderFactory->create()->load($sliderId);

        if($slider->getId()){
            $this->setSlider($slider);
            $this->setTemplate($this->_template);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSliderDisplayId()
    {
        return $this->_dateTime->timestamp().$this->getSliderId();
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return int
     */
    public function getProductsCount()
    {
        $items = self::MAX_PRODUCTS_COUNT;

        /**
        * Total number of products in slider must be equal with getProductsNumber
        * If not configured in slider settings then default MAX_PRODUCTS_COUNT is used
        * Additional featured products plus base slider type products equals total slider products
        */
        if(!$this->_productsNumber){
            if($this->_slider->getProductsNumber() > 0 && $this->_slider->getProductsNumber() <= self::MAX_PRODUCTS_COUNT ){
                $items = $this->_slider->getProductsNumber();
            }
        } else {
            $items = $this->_productsNumber;
        }

        return $items;
    }
}