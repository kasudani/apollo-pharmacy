<?php
namespace Emipro\Paymentservicecharge\Plugin;
class UpdateFeeForOrder
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quote;
    protected $logger;
    protected $_checkoutSession;
    protected $_registry;
    protected $_request;
    const AMOUNT_Payment = 'payment_fee';
    const AMOUNT_SUBTOTAL = 'subtotal';
    
    public function __construct(
        \Magento\Quote\Model\Quote $quote,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->quote = $quote;
        $this->logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_registry = $registry;
        $this->_request = $request;
    }
    /**
     * Get shipping, tax, subtotal and discount amounts all together
     *
     * @return array
     */
    public function afterGetAmounts($cart,$result)
    {
        $total = $result;
        $quote = $this->_checkoutSession->getQuote();
        $paymentMethod = $quote->getPayment()->getMethod();
        $paypalMehodList = ['payflowpro','payflow_link','payflow_advanced','braintree_paypal','paypal_express_bml','payflow_express_bml','payflow_express','paypal_express'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
        if($pointsSession->getCalcpoints()){
            if(in_array($paymentMethod,$paypalMehodList)){
                $finalpoints = $pointsSession->getCalcpoints();
                $total[self::AMOUNT_SUBTOTAL] = $total[self::AMOUNT_SUBTOTAL] + $finalpoints;
            }
        }
        return  $total;
    }
    /**
     * Get shipping, tax, subtotal and discount amounts all together
     *
     * @return array
     */
    public function beforeGetAllItems($cart)
    {   
        $paypalTest = $this->_registry->registry('is_paypal_items')? $this->_registry->registry('is_paypal_items') : 0;
        $quote = $this->_checkoutSession->getQuote();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
        $paymentMethod = $quote->getPayment()->getMethod();
        
        $paypalMehodList = ['payflowpro','payflow_link','payflow_advanced','braintree_paypal','paypal_express_bml','payflow_express_bml','payflow_express','paypal_express'];
        if($pointsSession->getCalcpoints()){
            if($paypalTest < 3 && in_array($paymentMethod,$paypalMehodList)){
                if(method_exists($cart , 'addCustomItem' ))
                {   
                    if(!$pointsSession->getCriditcustomitem() && !$pointsSession->getCriditcustomitempoint()){
                        $cart->addCustomItem($pointsSession->getCalcLable(), 1 ,$pointsSession->getCalcpoints());
                        $pointsSession->setCriditcustomitem(true);
                        $pointsSession->setCriditcustomitempoint($pointsSession->getCalcpoints());
                    }elseif($pointsSession->getCriditcustomitem() && !$pointsSession->getCriditcustomitempoint()){
                        $cart->addCustomItem($pointsSession->getCalcLable(), 1 ,$pointsSession->getCalcpoints());
                        $pointsSession->setCriditcustomitem(true);
                        $pointsSession->setCriditcustomitempoint($pointsSession->getCalcpoints());
                    }elseif(!$pointsSession->getCriditcustomitem() && $pointsSession->getCriditcustomitempoint()){
                        $cart->addCustomItem($pointsSession->getCalcLable(), 1 ,$pointsSession->getCalcpoints());
                        $pointsSession->setCriditcustomitem(true);
                        $pointsSession->setCriditcustomitempoint($pointsSession->getCalcpoints());
                    }elseif($pointsSession->getCriditcustomitem() && $pointsSession->getCriditcustomitempoint()){
                        //$cart->addCustomItem("Credit Points Discount", 1 ,0);
                        $pointsSession->setCriditcustomitem(0);
                        $pointsSession->setCriditcustomitempoint(0);
                    }
                    $reg = $this->_registry->registry('is_paypal_items');
                    $current = $reg + 1 ;
                    $this->_registry->unregister('is_paypal_items');
                    $this->_registry->register('is_paypal_items', $current);
                }
            }
        }

    }
}