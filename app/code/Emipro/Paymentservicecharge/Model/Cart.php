<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Emipro\Paymentservicecharge\Model;

/**
 * PayPal-specific model for shopping cart items and totals
 * The main idea is to accommodate all possible totals into PayPal-compatible 4 totals and line items
 */
class Cart extends \Magento\Paypal\Model\Cart
{
    protected function _validate()
    {
        $areItemsValid = false;
        $this->_areAmountsValid = false;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
        $customerSession=$objectManager->get('Magento\Customer\Model\Session');
        $logger=$objectManager->get('Psr\Log\LoggerInterface');
        $referenceAmount = $this->_salesModel->getDataUsingMethod('base_grand_total');
        $itemsSubtotal = 0;
        foreach ($this->getAllItems() as $i) {
            $itemsSubtotal = $itemsSubtotal + $i->getQty() * $i->getAmount();
        }

        $sum = $itemsSubtotal + $this->getTax();

        if (empty($this->_transferFlags[self::AMOUNT_SHIPPING])) {
            $sum += $this->getShipping();
        }

        if (empty($this->_transferFlags[self::AMOUNT_DISCOUNT])) {
            $sum -= $this->getDiscount();
            // PayPal requires to have discount less than items subtotal
            $this->_areAmountsValid = round($this->getDiscount(), 4) < round($itemsSubtotal, 4);
        } else {
            $this->_areAmountsValid = $itemsSubtotal > 0.00001;
        }

        /**
         * numbers are intentionally converted to strings because of possible comparison error
         * see http://php.net/float
         */
        // match sum of all the items and totals to the reference amount
        if (sprintf('%.4F', $sum) != sprintf('%.4F', $referenceAmount)) {
            //if($customerSession->isLoggedIn()) 
            //{
                //$areItemsValid = true;
                if($pointsSession->getCalcpoints()){
                $referenceAmount = $this->_salesModel->getDataUsingMethod('base_grand_total')-$pointsSession->getCalcpoints();
                }else{
                    $referenceAmount = $this->_salesModel->getDataUsingMethod('base_grand_total');
                }
            //}else{
            //   $referenceAmount = $this->_salesModel->getDataUsingMethod('base_grand_total');
            //}
        }
        if (sprintf('%.4F', $sum) == sprintf('%.4F', $referenceAmount)) {
            $areItemsValid = true;
        }
        $areItemsValid = $areItemsValid && $this->_areAmountsValid;

        if (!$areItemsValid) {
            $this->_salesModelItems = [];
            $this->_customItems = [];
        }
    }
}
