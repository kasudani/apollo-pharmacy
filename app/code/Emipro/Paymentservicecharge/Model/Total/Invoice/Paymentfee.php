<?php
namespace Emipro\Paymentservicecharge\Model\Total\Invoice;

class Paymentfee extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
	protected $_code="paymentfee";
	protected $_service_charge;
	
	public function collect(\Magento\Sales\Model\Order\Invoice $invoice        
    ) {
    	
        	parent::collect($invoice);
			$order=$invoice->getOrder();
		
			$invoice->setPaychargeFee($order->getPaychargeFee());
			$invoice->setPaychargeBaseFee($order->getPaychargeBaseFee());
			$invoice->setPaychargeFeeName($order->getPaychargeFeeName());

			$invoice->setGrandTotal($invoice->getGrandTotal() + $order->getPaychargeFee());
			$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $order->getPaychargeBaseFee());

        return $this;
    } 

}
