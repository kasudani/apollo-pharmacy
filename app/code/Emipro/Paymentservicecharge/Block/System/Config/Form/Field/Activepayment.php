<?php
/* 
* ////////////////////////////////////////////////////////////////////////////////////// 
* 
* @Author Emipro Technologies Private Limited 
* @Category Emipro 
* @Package  Emipro_Paymentservicecharge 
* @License http://shop.emiprotechnologies.com/license-agreement/ 
* 
* ////////////////////////////////////////////////////////////////////////////////////// 
*/
namespace Emipro\Paymentservicecharge\Block\System\Config\Form\Field;


class Activepayment extends \Magento\Framework\View\Element\Html\Select
{
   private $_activepayment;
    protected $_addPaymentAllOption = true;
    public function _getActivePaymentMethods()
	{
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$methods = array();
        $payments = $objectManager->create('Magento\Payment\Model\Config')->getActiveMethods();
        $scopeConfig=$objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
         
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		foreach ($payments as $paymentCode=>$paymentModel)
	    {
               $paymentTitles =  $scopeConfig->getValue('payment/'.$paymentCode.'/title', $storeScope);
               $methods[$paymentCode] = $paymentTitles;
        }
 		
		 return $methods;
	} 
  
  public function setInputName($value)
    {
        return $this->setName($value);
    }
 
    public function _toHtml()
    {
		if (!$this->getOptions()) {
            if ($this->_addPaymentAllOption) 
            {
                $this->addOption(\Magento\Customer\Model\Group::CUST_GROUP_ALL, __('ALL METHODS'));
            }
            foreach ($this->_getActivePaymentMethods() as $paymentId => $paymentTitles)
            {
				 $this->addOption($paymentId, addslashes($paymentTitles));
            }
        }
        return parent::_toHtml();
    }
}
