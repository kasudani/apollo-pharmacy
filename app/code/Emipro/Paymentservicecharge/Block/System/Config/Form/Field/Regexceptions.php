<?php
namespace Emipro\Paymentservicecharge\Block\System\Config\Form\Field;

class Regexceptions extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray {
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = [];
    protected $_customerGroupRenderer;
    protected $_paymentRenderer;
    protected $_chargeTypeRenderer;
    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = true;
     /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;
    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }
    /**
     * Returns renderer for country element
     *
     * @return \Magento\Braintree\Block\Adminhtml\Form\Field\Countries
     */
    protected function getCustomerGroupRenderer() {
        if (!$this->_customerGroupRenderer) {
        
            $this->_customerGroupRenderer = $this->getLayout()->createBlock(
                    'Magento\CatalogInventory\Block\Adminhtml\Form\Field\Customergroup', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_customerGroupRenderer;
    }

    protected function _getPaymentRenderer() {
        if (!$this->_paymentRenderer) {
            $this->_paymentRenderer = $this->getLayout()->createBlock('Emipro\Paymentservicecharge\Block\System\Config\Form\Field\Activepayment', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_paymentRenderer;
    }

    protected function _getChargeTypeRenderer() {
        if (!$this->_chargeTypeRenderer) {
            $this->_chargeTypeRenderer = $this->getLayout()->createBlock('Emipro\Paymentservicecharge\Block\System\Config\Form\Field\Extrachargetype', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_chargeTypeRenderer;
    }

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->create('Emipro\Paymentservicecharge\Helper\Data')->validatePaymentchargeData();
        $this->addColumn(
                'name', [
            'label' => __('Label'),
            'required' => true,
             'style' => 'width:100px',
                ]
        );

        $this->addColumn(
                'payment_method', [
            'label' => __('Payment Method'),
            'renderer' => $this->_getPaymentRenderer(),
                ]
        );
        $this->addColumn(
                'customer_group', [
            'label' => __('Customer Group'),
            'renderer' => $this->getCustomerGroupRenderer(),
                ]
        );
        $this->addColumn(
                'extra_charge_value', [
            'label' => __('Extra Charge Type'),
            'required' => true,
            'style' => 'width:100px',
                ]
        );

        $this->addColumn(
                'extra_charge_type', [
            'label' => __('Extra Charge Type'),
            'renderer' => $this->_getChargeTypeRenderer(),
                ]
        );
        
        //$this->addColumn('active', array('label' => __('Active')));
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }


    protected function _prepareArrayRow(\Magento\Framework\DataObject $row) {        
		$options = [];

        $customerGroup = $row->getData('customer_group');
        if ($customerGroup) {
            $options['option_' . $this->getCustomerGroupRenderer()->calcOptionHash($customerGroup)] = 'selected="selected"';
        }

        $paymentmethod = $row->getData('payment_method');                
        if ($paymentmethod) {
            $options['option_' . $this->_getPaymentRenderer()->calcOptionHash($paymentmethod)] = 'selected="selected"';  
        }            
        
        $chargetype = $row->getData('extra_charge_type');
        if ($chargetype) {
            $options['option_' . $this->_getChargeTypeRenderer()->calcOptionHash($chargetype)] = 'selected="selected"';
        }
	$row->setData('option_extra_attrs', $options);
               
    }

}
