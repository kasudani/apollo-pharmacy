<?php
/* 
* ////////////////////////////////////////////////////////////////////////////////////// 
* 
* @Author Emipro Technologies Private Limited 
* @Category Emipro 
* @Package  Emipro_Paymentservicecharge 
* @License http://shop.emiprotechnologies.com/license-agreement/ 
* 
* ////////////////////////////////////////////////////////////////////////////////////// 
*/ 
namespace Emipro\Paymentservicecharge\Block\System\Config\Form\Field;


class Extrachargetype extends \Magento\Framework\View\Element\Html\Select
{
   
    private $_chargetype;
	protected $_addChargeAllOption = true;

    public function getExtrachargetype()
	{
		$methods = array();
	
       return array(
            'fixed'=>__('Fixed Charge'),
           'percentage'=>__('Percentage')
        );
		 //return $methods;
 
	} 
  
  public function setInputName($value)
    {
        return $this->setName($value);
    }
 
    public function _toHtml()
    {
		
		if (!$this->getOptions()) {
            /*if ($this->_addChargeAllOption) 
            {
                $this->addOption(Mage_Customer_Model_Group::CUST_GROUP_ALL, Mage::helper('emipro_paymentservicecharge')->__('--Select--'));
            }*/
            foreach ($this->getExtrachargetype() as $key => $Titles)
            {
				 
				 $this->addOption($key, addslashes($Titles));
            }
        }
        return parent::_toHtml();
    }
}
