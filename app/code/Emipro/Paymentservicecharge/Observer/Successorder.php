<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Emipro\Paymentservicecharge\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\ResourceConnection;

class Successorder implements ObserverInterface {

    
 /** @var \Magento\Framework\Logger\Monolog */
    protected $_logger;
    protected $_orderFactory;    
    protected $_checkoutSession;
    
    public function __construct(        
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        ResourceConnection $resource
    ) {
        $this->_logger = $loggerInterface;
        $this->_checkoutSession = $checkoutSession; 
        $this->_orderFactory = $orderFactory;
        $this->_resource = $resource; 
    }
 
    /**
     * This is the method that fires when the event runs.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer )
    {        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $pointsSession=$objectManager->get('Magento\Catalog\Model\Session');
        $pointsSession->setCalcpoints(0);
        $pointsSession->setCriditcustomitem(0);
        $pointsSession->setCriditcustomitempoint(0);
        $pointsSession->setCalcLable('');
    }

}
