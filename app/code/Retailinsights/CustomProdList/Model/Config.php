<?php



namespace Retailinsights\CustomProdList\Model;



class Config extends \Magento\Catalog\Model\Config
{



    /**

     * Retrieve Attributes Used for Sort by as array

     * key = cde, value = name

     *

     * @return array

     */

   

    public function getAttributeUsedForSortByArray()
    {

         

                $options = ['position' => __('Position'),

                            'rating_summary' => __('Rating')];

       

        foreach ($this->getAttributesUsedForSortBy() as $attribute) {

            /* @var $attribute \Magento\Eav\Model\Entity\Attribute\AbstractAttribute */

            $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();

        }

        

return $options;

}

}