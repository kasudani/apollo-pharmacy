<?php
namespace Retailinsights\CouponUse\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
public function upgrade(
	SchemaSetupInterface $setup,
ModuleContextInterface $context)
{
$setup->startSetup();
// Get module table
$tableName = $setup->getTable('customer_custom_coupons');
// Check if the table already exists
if ($setup->getConnection()->isTableExists($tableName) == true) {
// Declare data
$columns = [
'uses_per_cpn' => [
'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
'nullable' => true,
'comment' => 'Uses Per Coupon',
],
'uses_per_cust' => [
'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
'nullable' => true,
'comment' => 'Use per Customer',
],
];
$connection = $setup->getConnection();
foreach ($columns as $name => $definition) {
$connection->addColumn($tableName, $name, $definition);
}
} 
$setup->endSetup();

}
}