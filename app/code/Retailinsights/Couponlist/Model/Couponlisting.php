<?php
namespace Retailinsights\Couponlist\Model;

class Couponlisting extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\Couponlist\Model\ResourceModel\Couponlisting');
    }
}
?>