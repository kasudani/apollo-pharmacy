<?php
namespace Retailinsights\Couponlist\Model\ResourceModel;

class Couponlisting extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_custom_coupons', 'id');
    }
}
?>