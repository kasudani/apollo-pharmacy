<?php

namespace Retailinsights\Couponlist\Block\Index;
use Magento\Framework\ObjectManagerInterface;

class Index extends \Magento\Framework\View\Element\Template {

    protected $_categoryCollectionFactory;
    protected $_categoryHelper;
    protected $objectManager;
    protected $customModelFactory;
    protected $_productCollectionFactory;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
    	\CustomerRicoupons\Couponmodule\Model\CouponmoduleFactory $CouponlistingFactory,
    	 \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource,
        \Magento\Framework\UrlInterface $urlInterface, 
        ObjectManagerInterface $objectManager, 
     array $data = []) {
          
         $this->_connection = $resource->getConnection();
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        $this->customModelFactory = $CouponlistingFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->objectManager = $objectManager;
        parent::__construct($context, $data);

    }
      public function getMediaUrl(){

            $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir;
        }
       public function getBaseUrl()
       {
        return $this->_storeManager->getStore()->getBaseUrl();
       }  
       public function getCategoryCollection()
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
            $collection->addIsActiveFilter();
            $collection->addLevelFilter(2);        
        return $collection;
    }
    public function getCustomerId()
         {
           $om = \Magento\Framework\App\ObjectManager::getInstance();

           $customerSession = $om->get('Magento\Customer\Model\Session');
           if($customerSession->isLoggedIn()) 
           {
            return $customerSession->getCustomer()->getId();  // get Customer Id
           }   
         }
              public function getTableData()
        {
           date_default_timezone_set('Asia/Kolkata');
            $today=date("Y-m-d H:i:s");
            $myTable = $this->_connection->getTableName('customer_custom_coupons');
            $sql     = $this->_connection->select('id','product_skus','uses_per_cust','uses_per_cpn')->from(["tn" => $myTable])
                  ->where('to_date >= ?',$today)
                  ->where('from_date <= ?',$today);      
            $result  = $this->_connection->fetchAll($sql); 
            return $result;
        }
             public function getCouponUsageReportData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('coupons_usage_report');
            $sql     = $this->_connection->select('coupon_usage')->from(["tn" => $myTable])
                  ->where('coupon_id = ?',$coupon_id);      
            $result  = $this->_connection->fetchRow($sql); 
            return $result['coupon_usage'];
        }
             public function getCustomerCouponUsageData($coupon_id)
        {
            $myTable = $this->_connection->getTableName('customer_coupon_usage');
            $sql     = $this->_connection->select('coupon_used')->from(["tn" => $myTable])
                  ->where('coupon_id= ?',$coupon_id)
                  ->where('customer_id= ?',$this->getCustomerId());          
            $result  = $this->_connection->fetchRow($sql); 
            return $result['coupon_used'];
        }
      public function getCustomData()
       {
       	   $categories=$this->getCategoryCollection();
       	    $customcollection=array();
       	      $items=array();
              $data=$this->getTableData();
               foreach($data as $dat)
            {
                 ob_start();
                if($this->getCustomerCouponUsageData($dat['id'])<$dat['uses_per_cust'])
                {              
                 if($this->getCouponUsageReportData($dat['id'])<$dat['uses_per_cpn'])
                 {     
                 $skus=explode(",",$dat['product_skus']);
                 foreach($skus as $sk)
                   {
                     array_push($items,$sk);
                   }      
                }
             }
          }
            $count= 0;
             
             $final=array();
       	   foreach($categories as $category)
       	   {
             $result=array();
       	   	 $prodcollection=$this->_productCollectionFactory->create();
             $prodcollection->addCategoriesFilter(['in' => $category['entity_id']]);
             $prodcollection->addAttributeToFilter('sku', array('in' => $items));
             $prodcollection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
             //$customcollection[$category['name']]=count($prodcollection);
              if(count($prodcollection)>0){
             array_push($result,$category['name'],count($prodcollection),$category['entity_id'],$category->getImageUrl());
             array_push($final,$result);
              $count=$count+1;
                 }       
                       
       	   } 
           $customcollection['count']=$count-1;
           $customcollection['data']=$final;   
       	    return $customcollection;
       }

    

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

}