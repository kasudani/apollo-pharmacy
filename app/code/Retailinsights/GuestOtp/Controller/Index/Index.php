<?php	
namespace Retailinsights\GuestOtp\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	 public $GuestotpFactory;
	 protected $resultJsonFactory;
	 public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Retailinsights\GuestOtp\Model\GuestotpFactory $GuestotpFactory
    ) 
	{
		$this->GuestotpFactory = $GuestotpFactory;
        parent::__construct($context);
    }
    public function execute()
    {	    	        
            $six_digit_random_number = mt_rand(100000, 999999);		  
	        $result= $this->getRequest()->getParams();
			 $mobileregex = "/^(\+){0,1}(91|0){0,1}[0-9]{10}$/"; 
             $error=0; 
             if(strlen($result['mobile'])==10)
             {
             	$result['mobile'] = "0".$result['mobile'];
             }
            if(preg_match($mobileregex, $result['mobile']) != 1)
			{
			     $error=1;	
				  echo 0;
			}
			 if($error!=1)
			 {
			  			  
			$xml = '<?xml version="1.0" encoding="utf-8"?>
				<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				  <soap:Body>
					<Sndotp xmlns="http://tempuri.org/">
					  <otp>'.$six_digit_random_number.'</otp>
					  <mobile>'.$result["mobile"].'</mobile>
					</Sndotp>
				  </soap:Body>
				</soap:Envelope>';

				$url = "http://124.124.88.106:8083/smsservice.asmx?op=Sndotp";

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

				$headers = array();
				array_push($headers, "Content-Type: text/xml; charset=utf-8");
				array_push($headers, "Accept: text/xml");
				array_push($headers, "Cache-Control: no-cache");
				array_push($headers, "Pragma: no-cache");
				if($xml != null) 
						{
							curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
							array_push($headers, "Content-Length: " . strlen($xml));
						}
				curl_setopt($ch, CURLOPT_USERPWD, "user_name:password"); /* If required */
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				$response = curl_exec($ch);
				$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
	              $model = $this->GuestotpFactory->create();
                  $model->setData('customer_email',$result["email"]);
                  $model->setData('customer_otp',$six_digit_random_number);
				  $model->setData('customer_mobile',"'".$result["mobile"]."'");
				  
				
                  $model->save();
				   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
				$tableName = $resource->getTableName('guest_otp');
						
				$sql = "Update " . $tableName . " Set customer_mobile = '".$result["mobile"]."' where id =".$model->getData("id");
				
                  $connection->query($sql);	
             				  
                   echo 1;
			}				   
    }
}