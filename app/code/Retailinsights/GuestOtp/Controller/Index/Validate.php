<?php

namespace Retailinsights\GuestOtp\Controller\Index;

class Validate extends \Magento\Framework\App\Action\Action
{
	 public $GuestotpFactory;
	 protected $resultJsonFactory;
	  protected $request;
	 public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\Retailinsights\GuestOtp\Model\GuestotpFactory $GuestotpFactory,
		\Magento\Framework\Session\SessionManagerInterface $coreSession
    ) 
		{
			$this->_coreSession = $coreSession; 
			$this->GuestotpFactory = $GuestotpFactory;
			parent::__construct($context);
		}
				public function setOtpValidated($value)
				{
				$this->_coreSession->start();
				$this->_coreSession->setOtpValidated($value);
				}
				 public function getOtpValidated()
				{
				$this->_coreSession->start();
				return $this->_coreSession->getOtpValidated();
				}
				 public function unsOtpValidated()
				{
				$this->_coreSession->start();
				return $this->_coreSession->unsOtpValidated();
				}
					public function execute()
					{	    
					
					  $result= $this->getRequest()->getParams();
						 $email=$result['email'];	
						  
						 $otp=$result['otp'];
						 $mobile=$result['mobile'];  
							$model = $this->GuestotpFactory->create();
							$collection = $model->getCollection();
								 $collection->addFieldToFilter('customer_email',$email);
								 $collection->addFieldToFilter('customer_otp',$otp);
								 $collection->addFieldToFilter('customer_mobile',$mobile);
								
								  if(!empty($collection->getData()))
								  {
									
										foreach($collection as $col)
										{
										$id=$col->getData('id');
										}
									   $model->load($id,'id');
									 if($model->getData('id')!=='')
									{
									   
									$model->delete();
									 $this->setOtpValidated(1);
									 echo 1;
									}
								   else
								   {	
								  $this->unsOtpValidated();			
								  echo 0;	
								   }	
								 }
								  else
								  {
									  
								  $model->load($email,'customer_email');	
								  $model->delete();	
								  
								  $this->unsOtpValidated();
									 echo 0;  
								  }
								
							 
					}
}