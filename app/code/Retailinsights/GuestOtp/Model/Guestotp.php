<?php
namespace Retailinsights\GuestOtp\Model;

class Guestotp extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\GuestOtp\Model\ResourceModel\Guestotp');
    }
}
?>