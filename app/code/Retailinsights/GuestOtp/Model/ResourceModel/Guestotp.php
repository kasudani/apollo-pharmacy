<?php
namespace Retailinsights\GuestOtp\Model\ResourceModel;

class Guestotp extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('guest_otp', 'id');
    }
}
?>