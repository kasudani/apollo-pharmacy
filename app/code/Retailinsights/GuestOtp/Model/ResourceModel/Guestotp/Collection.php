<?php

namespace Retailinsights\GuestOtp\Model\ResourceModel\Guestotp;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\GuestOtp\Model\Guestotp', 'Retailinsights\GuestOtp\Model\ResourceModel\Guestotp');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>