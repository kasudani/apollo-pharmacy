<?php

 

namespace Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused\Edit\Tab\Renderer;

 

use Magento\Framework\DataObject;

 

class CouponName extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    /**

     * @var \Magento\Catalog\Model\CategoryFactory

     */

    protected $categoryFactory;

    /**

     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory

     */

    public function __construct(

        \Magento\Catalog\Model\CategoryFactory $categoryFactory,

        \CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule $resource

    ) {

        $this->categoryFactory = $categoryFactory;



         $this->_connection = $resource->getConnection();

    }

        public function getCouponCode($coupon_id)
        {

            $myTable = $this->_connection->getTableName('customer_custom_coupons');

            $sql     = $this->_connection->select('coupon_code')->from(["tn" => $myTable])

                  ->where('id = ?',$coupon_id);      

            $result  = $this->_connection->fetchRow($sql); 

            return $result['coupon_code'];

        }

 

    /**

     * get category name

     * @param  DataObject $row

     * @return string

     */

    public function render(DataObject $row)
    {

        return $this->getCouponCode($row->getData('coupon_id'));

    }

}