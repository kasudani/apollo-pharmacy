<?php
namespace Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('customercouponused_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Customercouponused Information'));
    }
}