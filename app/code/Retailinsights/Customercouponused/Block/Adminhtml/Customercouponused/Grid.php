<?php
namespace Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Retailinsights\Customercouponused\Model\customercouponusedFactory
     */
    protected $_customercouponusedFactory;

    /**
     * @var \Retailinsights\Customercouponused\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Retailinsights\Customercouponused\Model\customercouponusedFactory $customercouponusedFactory
     * @param \Retailinsights\Customercouponused\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Retailinsights\Customercouponused\Model\CustomercouponusedFactory $CustomercouponusedFactory,
        \Retailinsights\Customercouponused\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_customercouponusedFactory = $CustomercouponusedFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_customercouponusedFactory->create()->getCollection();
        $this->setCollection($collection);
    
        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'customer_id',
					[
						'header' => __('customer Id'),
						'index' => 'customer_id',
					]
				);
                $this->addColumn(
                    'customer_name',
                    [
                        'header' => __('customer Name'),
                        'index' => 'customer_name',
                        'renderer'  => 'Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused\Edit\Tab\Renderer\CustomerName'
                    ]
                );
				
				$this->addColumn(
					'coupon_id',
					[
						'header' => __('Coupon Id'),
						'index' => 'coupon_id',
					]
				);
                $this->addColumn(
                    'coupon_code',
                    [
                        'header' => __('Coupon Code'),
                        'index' => 'coupon_code',
                         'renderer'  => 'Retailinsights\Customercouponused\Block\Adminhtml\Customercouponused\Edit\Tab\Renderer\CouponName'
                    ]
                );
				
				$this->addColumn(
					'coupon_used',
					[
						'header' => __('Coupon Used'),
						'index' => 'coupon_used',
					]
				);
				


		

		
		   $this->addExportType($this->getUrl('customercouponused/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Retailinsights_Customercouponused::customercouponused/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('customercouponused');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('customercouponused/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('customercouponused/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('customercouponused/*/index', ['_current' => true]);
    }

    /**
     * @param \Retailinsights\Customercouponused\Model\customercouponused|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		return '#';
    }

	

}