<?php
namespace Retailinsights\Customercouponused\Model\ResourceModel;

class Customercouponused extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_coupon_usage', 'id');
    }
}
?>