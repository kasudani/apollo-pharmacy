<?php

namespace Retailinsights\Customercouponused\Model\ResourceModel\Customercouponused;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\Customercouponused\Model\Customercouponused', 'Retailinsights\Customercouponused\Model\ResourceModel\Customercouponused');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>