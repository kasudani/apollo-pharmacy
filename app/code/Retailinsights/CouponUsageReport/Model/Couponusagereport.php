<?php
namespace Retailinsights\CouponUsageReport\Model;

class Couponusagereport extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\CouponUsageReport\Model\ResourceModel\Couponusagereport');
    }
}
?>