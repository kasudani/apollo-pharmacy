<?php
namespace Retailinsights\CouponUsageReport\Model\ResourceModel;

class Couponusagereport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('coupons_usage_report', 'id');
    }
}
?>