<?php

namespace Retailinsights\CouponUsageReport\Model\ResourceModel\Couponusagereport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\CouponUsageReport\Model\Couponusagereport', 'Retailinsights\CouponUsageReport\Model\ResourceModel\Couponusagereport');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>