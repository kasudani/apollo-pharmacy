<?php
namespace Retailinsights\CouponUsageGrid\Model;

class Couponusagegrid extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\CouponUsageGrid\Model\ResourceModel\Couponusagegrid');
    }
}
?>