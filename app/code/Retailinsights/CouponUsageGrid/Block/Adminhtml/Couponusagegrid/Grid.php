<?php
namespace Retailinsights\CouponUsageGrid\Block\Adminhtml\Couponusagegrid;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Retailinsights\CouponUsageGrid\Model\couponusagegridFactory
     */
    protected $_couponusagegridFactory;

    /**
     * @var \Retailinsights\CouponUsageGrid\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Retailinsights\CouponUsageGrid\Model\couponusagegridFactory $couponusagegridFactory
     * @param \Retailinsights\CouponUsageGrid\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Retailinsights\CouponUsageGrid\Model\CouponusagegridFactory $CouponusagegridFactory,
        \Retailinsights\CouponUsageGrid\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_couponusagegridFactory = $CouponusagegridFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_couponusagegridFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'coupon_id',
					[
						'header' => __('Coupon Id'),
						'index' => 'coupon_id',
					]
				);
                $this->addColumn(
                    'coupon_code',
                    [
                        'header' => __('Coupon Code'),
                        'index' => 'coupon_code',
                        'renderer'  => 'Retailinsights\CouponUsageGrid\Block\Adminhtml\Couponusagegrid\Edit\Tab\Renderer\CouponCode'
                    ]
                );
				
				$this->addColumn(
					'coupon_usage',
					[
						'header' => __('Coupon Usage'),
						'index' => 'coupon_usage',
					]
				);
				


		
        //$this->addColumn(
            //'edit',
            //[
                //'header' => __('Edit'),
                //'type' => 'action',
                //'getter' => 'getId',
                //'actions' => [
                    //[
                        //'caption' => __('Edit'),
                        //'url' => [
                            //'base' => '*/*/edit'
                        //],
                        //'field' => 'id'
                    //]
                //],
                //'filter' => false,
                //'sortable' => false,
                //'index' => 'stores',
                //'header_css_class' => 'col-action',
                //'column_css_class' => 'col-action'
            //]
        //);
		

		
		   $this->addExportType($this->getUrl('couponusagegrid/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('couponusagegrid/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        //$this->getMassactionBlock()->setTemplate('Retailinsights_CouponUsageGrid::couponusagegrid/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('couponusagegrid');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('couponusagegrid/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('couponusagegrid/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('couponusagegrid/*/index', ['_current' => true]);
    }

    /**
     * @param \Retailinsights\CouponUsageGrid\Model\couponusagegrid|\Magento\Framework\Object $row
     * @return string
     */
    

	

}