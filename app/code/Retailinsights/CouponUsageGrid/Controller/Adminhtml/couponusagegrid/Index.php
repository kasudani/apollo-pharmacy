<?php

namespace Retailinsights\CouponUsageGrid\Controller\Adminhtml\couponusagegrid;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPagee;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Retailinsights_CouponUsageGrid::couponusagegrid');
        $resultPage->addBreadcrumb(__('Retailinsights'), __('Retailinsights'));
        $resultPage->addBreadcrumb(__('Manage item'), __('Coupon Usage Report'));
        $resultPage->getConfig()->getTitle()->prepend(__('Coupon Usage Report'));

        return $resultPage;
    }
}
?>