<?php

namespace Retailinsights\OrderCoupons\Model\ResourceModel\Ordercoupons;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retailinsights\OrderCoupons\Model\Ordercoupons', 'Retailinsights\OrderCoupons\Model\ResourceModel\Ordercoupons');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>