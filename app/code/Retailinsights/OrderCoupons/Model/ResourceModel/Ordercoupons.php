<?php
namespace Retailinsights\OrderCoupons\Model\ResourceModel;

class Ordercoupons extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('coupon_order_details', 'id');
    }
}
?>