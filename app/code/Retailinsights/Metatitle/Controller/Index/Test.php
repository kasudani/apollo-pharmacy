<?php
namespace Retailinsights\Metatitle\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		  
	 $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
            $categoryCollection = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
            $categories = $categoryCollection->create();
            $categories->addAttributeToSelect('*');
            foreach ($categories as $category) {
              $category->setmeta_title($category->getName()." Products,Buy ".$category->getName()." - Apollo Pharmacy.");
              $category->setmeta_description('Find best deals & offers on '.$category->getName().' products online at Apollo Pharmacy. Avail COD, Home Delivery & Store Pickup available across India on wide range of '.$category->getName().' Products.');
              $category->save();
            } 
            echo "sucess"; 
    }
}