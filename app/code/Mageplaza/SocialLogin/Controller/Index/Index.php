<?php 

namespace Mageplaza\SocialLogin\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    protected $entityFactory;

    protected $_objectManager;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
         \Mageplaza\SocialLogin\Model\Entity $entityFactory
    )
    {
        $this->entityFactory = $entityFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

	    $mobile = $this->getRequest()->getPost('mobile');
	    $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value', $mobile);
        $model->addFieldToFilter('attribute_id', '155');
        // print_r($model->getData());
        // die();
		if(empty($model->getData())){
		 echo "no";
		}else{
		echo "yes";
		}
    }
}
?>