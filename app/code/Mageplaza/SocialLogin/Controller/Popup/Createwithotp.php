<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_SocialLogin
 * @copyright   Copyright (c) 2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\SocialLogin\Controller\Popup;

use Magento\Captcha\Helper\Data as CaptchaData;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Controller\Account\CreatePost;
use Magento\Customer\Helper\Address;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Metadata\FormFactory;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\UrlFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\SocialLogin\Helper\Data;

/**
 * Class Create
 *
 * @package Mageplaza\SocialLogin\Controller\Popup
 */
class Createwithotp extends \Magento\Framework\App\Action\Action
{
    /**
     * @type \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @type \Magento\Captcha\Helper\Data
     */
    protected $captchaHelper;

    /**
     * @type \Mageplaza\SocialLogin\Helper\Data
     */
    protected $socialHelper;

    /**
     * @type
     */
    private $cookieMetadataManager;

    /**
     * @type
     */
    private $_customerSession;
    private $storeManager;

    private $cookieMetadataFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagement
     * @param \Magento\Customer\Helper\Address $addressHelper
     * @param \Magento\Framework\UrlFactory $urlFactory
     * @param \Magento\Customer\Model\Metadata\FormFactory $formFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param \Magento\Customer\Api\Data\RegionInterfaceFactory $regionDataFactory
     * @param \Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory
     * @param \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerDataFactory
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Customer\Model\Registration $registration
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Customer\Model\CustomerExtractor $customerExtractor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Magento\Customer\Model\Account\Redirect $accountRedirect
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Captcha\Helper\Data $captchaHelper
     * @param \Mageplaza\SocialLogin\Helper\Data $socialHelper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManagement,
        Address $addressHelper,
        UrlFactory $urlFactory,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerInterfaceFactory $customerDataFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        DataObjectHelper $dataObjectHelper,
        AccountRedirect $accountRedirect,
        JsonFactory $resultJsonFactory,
        CaptchaData $captchaHelper,
        Data $socialHelper
    )
    {

        $this->storeManager = $storeManager;
        $this->_customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->captchaHelper = $captchaHelper;
        $this->socialHelper = $socialHelper;

        parent::__construct($context);
    }

    /**
     * Create customer account action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        $result = array(
            'success' => false,
            'message' => array()
        );
/*
        if (!$this->checkCaptcha()) {
            $result['message'] = __('Incorrect CAPTCHA.');
            return $resultJson->setData($result);
        }

        if ($this->session->isLoggedIn() || !$this->registration->isAllowed()) {
            $result['redirect'] = $this->urlModel->getUrl('customer/account');

            return $resultJson->setData($result);
        }

        if (!$this->getRequest()->isPost()) {
            $result['message'] = __('Data error. Please try again.');

            return $resultJson->setData($result);
        }*/

        //$this->session->regenerateId();

        //try {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerFactory = $objectManager->create('Magento\Customer\Model\CustomerFactory');
            $customerm = $objectManager->create('Magento\Customer\Model\Customer');

            $user_name       = $this->getRequest()->getPost('user_name');
            $user_email       = $this->getRequest()->getPost('user_email');
            $user_number       = $this->getRequest()->getPost('user_number');
            $user_password       = $this->getRequest()->getPost('user_password');
            
            if($user_number != $_SESSION['verified_mobile']){
                echo "invalid";
            }
            $user_name = explode(' ', $user_name);
            $first_name = 'Guest';
            $last_name = '';
            
            if(isset($user_name[0]) && $user_name[0]){
                $first_name = $user_name[0];
            }

            if(isset($user_name[1]) && $user_name[1]){
                $last_name = $user_name[1];
            }

            // Get Website ID
            $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

            // Instantiate object (this is the most important part)
            $customer   = $customerFactory->create();
            $customer->setWebsiteId($websiteId);

            // Preparing data for new customer
            $customer->setEmail($user_email); 
            $customer->setFirstname($first_name);
            if($last_name!='')
             $customer->setLastname($last_name);
            $customer->setPassword($user_password);

            // Save data
            $customer->save();
            $customer->sendNewAccountEmail();

            $customerData = $customer->getDataModel();
            $customerData->setCustomAttribute('mobile_number', $user_number);
            $customerData->setCustomAttribute('billing_telephone', $user_number);
            $customer->updateData($customerData);
            $customer->save();

            //echo $customer->getEntityId();
           /* $customers = $customerm->load($customer->getEntityId());

            $customerData = $customers->getDataModel();
            $customerData->setCustomAttribute('mobile_number',$user_number);
            $customerData->setCustomAttribute('billing_telephone',$user_number);
            $customers->updateData($customerData);

            $customerResource = $customerFactory->create();
            $customerResource->saveAttribute($customers, 'mobile_number');
            $customerResource->saveAttribute($customers, 'billing_telephone');
*/
            $this->_customerSession->loginById($customer->getEntityId());

            $_SESSION['verified_mobile'] = '';

            $result["success"] = true; 

        /*} catch (StateException $e) {
            $url = $this->urlModel->getUrl('customer/account/forgotpassword');
            // @codingStandardsIgnoreStart
            $result['message'][] = __(
                'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.',
                $url
            );
        } catch (InputException $e) {
            $result['message'][] = $this->escaper->escapeHtml($e->getMessage());
            foreach ($e->getErrors() as $error) {
                $result['message'][] = $this->escaper->escapeHtml($error->getMessage());
            }
        } catch (LocalizedException $e) {
            $result['message'][] = $this->escaper->escapeHtml($e->getMessage());
        } catch (\Exception $e) {
            $result['message'][] = __('We can\'t save the customer.');
        }*/

        

        return $resultJson->setData($result);
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    protected function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(
                PhpCookieManager::class
            );
        }

        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    protected function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(
                CookieMetadataFactory::class
            );
        }

        return $this->cookieMetadataFactory;
    }

    /**
     * Make sure that password and password confirmation matched
     *
     * @param string $password
     * @param string $confirmation
     * @return boolean
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        return $password == $confirmation;
    }
}
