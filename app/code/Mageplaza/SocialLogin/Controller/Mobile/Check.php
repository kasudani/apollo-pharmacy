<?php 

namespace Mageplaza\SocialLogin\Controller\Mobile;
 
 
class Check extends \Magento\Framework\App\Action\Action {

    protected $entityFactory;

    protected $otpFactory;

    protected $_objectManager;

    protected $_helper;

    protected $attemptFactory;

    /**
     * Constructor
     * 
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    	\Mageplaza\SocialLogin\Helper\Attempt $helper,
        \Magento\Framework\App\Action\Context $context,
        \Mageplaza\SocialLogin\Model\Attempt $attemptFactory,
        \Mageplaza\SocialLogin\Model\Entity $entityFactory,
        \Mageplaza\SocialLogin\Model\Otp $otpFactory,
        \Magento\Framework\Stdlib\DateTime $dateTime
    )
    {
        $this->entityFactory = $entityFactory;
        $this->dateTime = $dateTime;
        $this->otpFactory = $otpFactory;
        $this->helper = $helper;
        $this->attemptFactory = $attemptFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     * 
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
	    $mobile = $this->getRequest()->getPost('mobile');
	    $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value', $mobile);
        $model->addFieldToFilter('attribute_id', '155');
		if(empty($model->getData())){
			echo 'not';
		}else{
			echo 'present';
        }
		
    }
}
?>