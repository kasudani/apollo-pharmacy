<?php

namespace Mageplaza\SocialLogin\Helper;

use Magento\Framework\App\RequestInterface;
use Mageplaza\SocialLogin\Helper\Data as HelperData;
use \Magento\Framework\App\Helper\AbstractHelper;

class Attempt extends AbstractHelper
{

    protected $_directoryList;

    public function __construct(
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Mageplaza\SocialLogin\Model\Attempt $attemptFactory,
        \Mageplaza\SocialLogin\Model\Entity $entityFactory
    ) {
        $this->entityFactory = $entityFactory;
        $this->attemptFactory = $attemptFactory;
        $this->_directoryList = $directoryList;
    }
    

    public function getId($cust_mobile)
    {
        $ent_id = $failures_num = "";
        $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value',$cust_mobile);
        foreach($model->getData() as $rtu)
            {
                $ent_id = $rtu['entity_id']; 
            }
        return $ent_id;
    }
    
    public function getCustomer($cust_mobile)
    {
        $ent_id = $failures_num = "";
        $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value',$cust_mobile);
        foreach($model->getData() as $rtu)
            {
                $ent_id = $rtu['entity_id']; 
            }
        $model1 = $this->attemptFactory->getCollection();
        $model1->addFieldToFilter('entity_id',$ent_id);
        foreach($model1->getData() as $rtu1)
            {
                $failures_num = $rtu1['failures_num']; 
            }
        return $failures_num;
    }

    public function getExpire($cust_mobile)
    {
        $ent_id = $expire_time = "";
        $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value',$cust_mobile);
        foreach($model->getData() as $rtu)
            {
                $ent_id = $rtu['entity_id']; 
            }
        $model1 = $this->attemptFactory->getCollection();
        $model1->addFieldToFilter('entity_id',$ent_id);
        foreach($model1->getData() as $rtu1)
            {
                $expire_time = $rtu1['lock_expires']; 
            }
        return $expire_time;
    }

    public function getNum($cust_mobile)
    {
        $ent_id = $failuresnum = "";
        $model = $this->entityFactory->getCollection();
        $model->addFieldToFilter('value',$cust_mobile);
        foreach($model->getData() as $rtu)
            {
                $ent_id = $rtu['entity_id']; 
            }
        $model1 = $this->attemptFactory->getCollection();
        $model1->addFieldToFilter('entity_id',$ent_id);
        foreach($model1->getData() as $rtu1)
            {
                $failuresnum = $rtu1['tmp_count']; 
            }
        return $failuresnum;
    }

}
