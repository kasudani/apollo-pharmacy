define(['jquery','Magento_Customer/js/customer-data'], function ($, customerData) {
	var seconds = 0;
	var home_url = $("#home_url").val();
	var refered_url = $("#refered_url").val();
	var mobile_url = $("#mobile_url").val();
	var check_url = $("#check_url").val();
	var verify_url = $("#verify_url").val();
	var my_otp_url = $("#my_otp_url").val();
	var my_verify_url = $("#my_verify_url").val();
	var forgot_url = $("#forgot_url").val();
	var send_otp_url = $("#send_otp_url").val();
	var register_otp_url = $("#register_otp_url").val();
	var register_otp_skip_url = $("#register_otp_skip_url").val();
	var login_url = home_url+ 'customer/ajax/login/';
	var set_location_url = home_url+ 'ordermedicine/medicine/setuserlocation/';
	
	var st = setTimeout( function(){ }  , 1 );
	var timer = function(){
		clearTimeout(st);
		st = setTimeout( function(){ 
		seconds--;
		$(".my_resend").text("Resend code in "+seconds);
		if(seconds<1){
			$(".my_resend").text("Resend");
		}else{
			timer();
		}
	  }  , 1000 );
	}

	var goback = function(){
		if($("#login-with-otp-form").is(":visible")){
		    $("#login-with-otp-form").hide();
		    $("#social-form-login").show();
		} else if($("#social-form-login").is(":visible")){
			window.location.href = $("#refered_url").val();
		} else if($("#register-with-otp-form").is(":visible")){
			window.location.href = $("#refered_url").val();
		} else if($(".forgot-password").is(":visible")){
			$(".form-login-next").show();
		    $(".login-header-part").show();
		    $(".forgot-header-part").hide();
		    $(".forgot-password").hide();  
		}
	}

	var gobackscreen = function(){
		if($("#login-with-otp-form").is(":visible")){
		    $("#login-with-otp-form").hide();
		    $("#social-form-login").show();
		} else if($(".forgot-password").is(":visible")){
			$(".form-login-next").show();
		    $(".login-header-part").show();
		    $(".forgot-header-part").hide();
		    $(".forgot-password").hide();  
		} else {
			window.location.href = $("#refered_url").val();
		} 
	}

	var gonext = function(){
		if($("#login-with-otp-form").is(":visible")){
		    $("#login-with-otp-form").hide();
		    $("#social-form-login").show();
		} else if($("#check-form-login").is(":visible")){
		    $("#check-form-login").hide();
			$("#social-form-login").show();
		} else if($(".forgot-password").is(":visible")){
			$(".form-login-next").show();
		    $(".login-header-part").show();
		    $(".forgot-header-part").hide();
		    $(".forgot-password").hide();  
		}
	}

	var gotootp = function(number){
		$("#check-form-login").hide();
		$("#login-with-otp-form").show();
		$(".get-otp1").hide();
		$("#get_my_otp").hide();
		$(".otp-sent-status").text("Please enter OTP sent to "+number);
		timer();
		$(".get-otp2").show();
		$(".my_sent").show();
		setTimeout(function(){ $(".my_sent").hide(); }, 5000);	
		$("#my_otp").show();
		$("#my_verify_mobile").show();
	}

	var openRegister = function(number){
		$("#login-with-otp-form").hide();
		$("#register-with-otp-form").show();
		$("#user_number").val(number);
	}

	var loginAjax = function(){
		if($("#email").val() == "" || $("#pass").val() == ""){
		/*if($("#email").val() == ""){
	    	$(".no_username").show();
	    	setTimeout(function(){ $(".no_username").hide(); }, 5000);
	    }
	    if($("#pass").val() == ""){
	    	$(".no_pass").show();
	    	setTimeout(function(){ $(".no_pass").hide(); }, 5000);
	    }*/
	    } else {
		    var username = $("#email").val();
		    var password = $("#pass").val();
	    	var data = {};
		    data["username"] = username;
		    data["password"] = password;
		    $.ajax({
                url: login_url,
                type: 'POST',
                data: JSON.stringify(data)
            }).done(function (response) {
            	if(response.errors){
            		$(".invalid_credentials").text(response.message);
            		$(".invalid_credentials").show();
            	} else {
            		$.ajax({
			                url: set_location_url, 
			                type: 'GET'
			            }).done(function (response) {
			            	goback();	
			            	
			            }).fail(function () {
							goback();
			            });


            		//goback();	
            	}
            }).fail(function () {
				console.log("Error");
            });
	    }
	}

	var registerWithOtp = function(){
		if($("#user_name").val() == "" || $("#user_email").val() == "" || $("#user_password").val() == ""){
			/*if($("#email").val() == ""){
		    	$(".no_username").show();
		    	setTimeout(function(){ $(".no_username").hide(); }, 5000);
		    }
		    if($("#pass").val() == ""){
		    	$(".no_pass").show();
		    	setTimeout(function(){ $(".no_pass").hide(); }, 5000);
		    }*/

		    } else {
			var user_name = $("#user_name").val();
			var user_email = $("#user_email").val();
			var user_number = $("#user_number").val();
			var user_password = $("#user_password").val();
			var data = {};
		    data["user_name"] = user_name;
		    data["user_number"] = user_number;
		    data["user_email"] = user_email;
		    data["user_password"] = user_password;
		    $.ajax({
                url: register_otp_url,
                type: 'POST',
                data: data
            }).done(function (response) {
            	if(response=="invalid"){
            		$(".invalid_request").show();
            		setTimeout(function(){ $(".invalid_request").hide(); }, 5000);
            	} else if(response.success){
            		goback();	
            	}
            }).fail(function () {
				console.log("Error");
            });
		}
	}

	var registerSkipWithOtp = function(){
	    var user_number = $("#user_number").val();
	    var data = {};
	    data["user_number"] = user_number;
	    $.ajax({
            url: register_otp_skip_url,
            type: 'POST',
            data: data
        }).done(function (response) {
        	if(response=="invalid"){
        		$(".invalid_request").show();
        		setTimeout(function(){ $(".invalid_request").hide(); }, 5000);
        	} else if(response.success){
        		goback();	
        	}
        }).fail(function () {
			console.log("Error");
        });
	}

	var sendToCreate = function(number){
		var data = {};
		data["mobile"] = number;
	    $.ajax({
            url: send_otp_url,
            type: 'POST',
            data: data
        }).done(function (response) {
        	if(response=="sent"){
        		$("#mobile_no").val(number);
        		gotootp(number);
        	} else {
        		$(".invalid_number").show();
        	}
        }).fail(function () {
			console.log("Error");
        });
	}

	var checkNumber = function(){
		if($("#number").val() == ""){
			$(".jq_valid_number").show();
			setTimeout(function() { $(".jq_valid_number").hide(); }, 5000);
	    } else {
	    	var number = $("#number").val();
	    	var data = {};
	    	data["mobile"] = number;
		    $.ajax({
                url: check_url,
                type: 'POST',
                data: data
            }).done(function (response) {
            	if(response.errors || response == 'not'){
            		var filter = /^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$/; 
            		if (filter.test(number)) {
		              if(number.length==10){
		                   sendToCreate(number);
		              } else {
						$(".invalid_number").text(response.message);
						$(".invalid_number").show();
						setTimeout(function() { $(".invalid_number").hide(); }, 5000);
		              }
		            } else {
					  $(".invalid_number").text(response.message);
					  $(".invalid_number").show();
					  setTimeout(function() { $(".invalid_number").hide(); }, 5000);
		            }
            	} else if(response == 'present'){
            		$("#email").val(number);
            		gonext();	
            	}
            }).fail(function () {
				console.log("Error");
            });
	    }
	}

	var loginForgotPassword = function(){
		if($("#email_address_forgot").val() == ""){
		/*if($("#email").val() == ""){
	    	$(".no_username").show();
	    	setTimeout(function(){ $(".no_username").hide(); }, 5000);
	    }
	    if($("#pass").val() == ""){
	    	$(".no_pass").show();
	    	setTimeout(function(){ $(".no_pass").hide(); }, 5000);
	    }*/

	    } else {
		    var username = $("#email_address_forgot").val();
		    var data = {};
		    data["email"] = username;
		    $.ajax({
                url: forgot_url,
                type: 'POST',
                data: data
            }).done(function (response) {
	            if(response.success){
	        		$(".forgot-success-message div").text(response.message);
	        		$(".forgot-success-message").show();
	        	} else {
	        		goback();	
	        	}
            }).fail(function () {
				console.log("Error");
            });
		}
	}

	var sendOtp = function(){
		if($("#mobile_no").val() == ""){
		    	$(".my_no_number").show();
		    	setTimeout(function(){ $(".my_no_number").hide(); }, 5000);
		    } else {
		    var mobile_no = $("#mobile_no").val();
		    $('.my_resend').addClass('disabled');
		    var data = {};
		    data["mobile"] = mobile_no;
		    $.ajax({
                url: mobile_url,
                type: 'POST',
                data: data
            }).done(function (response) {
            	if(response == 'not'){
            		$(".my_no_number").show();
		    		setTimeout(function(){ $(".my_no_number").hide(); }, 5000);	
            	} else {
	        		$(".get-otp1").hide();
	        		$("#get_my_otp").hide();
	        		$(".otp-sent-status").text("Please enter OTP sent to "+mobile_no);
	        		timer();
	        		$(".get-otp2").show();
	        		$(".my_sent").show();
	        		setTimeout(function(){ $(".my_sent").hide(); }, 5000);	
	        		$("#my_otp").show();
	        		$("#my_verify_mobile").show();
            	}
            	setTimeout(function(){ $('.my_resend').removeClass('disabled'); }, 1000);
            }).fail(function () {
				console.log("Error");
				$('.my_resend').removeClass('disabled');
            });
		}
	}

	var verifyOtp = function(){
		if($("#my_check_otp").val() == ""){
	    	$(".my_not_match").show();
	    	setTimeout(function(){ $(".my_not_match").hide(); }, 5000);
		} else {
		    var otp = $("#my_check_otp").val();
		    var mobile_no = $("#mobile_no").val();
		    var data = {};
		    data["otp"] = otp;
		    data["mobile"] = mobile_no;
		    $.ajax({
                url: verify_url,
                type: 'POST',
                data: data
            }).done(function (response) {
            	if(response == 'not matched'){
            		$(".my_not_match").show();
		    		setTimeout(function(){ $(".my_not_match").hide(); }, 5000);
            	} else if(response == 'verified_as_new'){
            		openRegister(mobile_no);
            	} else if(response == 'blocked'){
            		$(".my_no_blocked").show();
		    		setTimeout(function(){ $(".my_no_blocked").hide(); }, 5000);
            	} else {
		    		window.location.href = $("#refered_url").val();
            	}
            }).fail(function () {
				console.log("Error");
            });
		}
	}

    $(document).ready(function($) {
    	
    	$('.mobile-login-back').on('click', function(){
    		gobackscreen();
    	});

        $("#login-with-otp").on("click", function(){
        	var numb = $('#email').val();
        	numb = numb.replace(/[^0-9]/g,'');
	        if (numb.length == 10) {
	        	$("#social-form-login").hide();
	        	$("#my_check_otp").val('');
	        	sendToCreate(numb);
	        } else {
	        	$("#social-form-login").hide();
			    $("#login-with-otp-form").show();
	        }
		});

		$(".mobile-remind").on("click", function(){
		    $(".form-login-next").hide();
		    $(".login-header-part").hide();
		    $(".forgot-header-part").show();
		    $(".forgot-password").show();  
		});

		/* attach a submit handler to the form */
	    $("#check-form-login").submit(function(event) {
			/* stop form from submitting normally */
			event.preventDefault();
			checkNumber();
		});

		/* attach a submit handler to the form */
	    $("#social-form-login").submit(function(event) {
			/* stop form from submitting normally */
			event.preventDefault();
			loginAjax();
		});

	    $("#register-with-otp-form").submit(function(event) {
			/* stop form from submitting normally */
			event.preventDefault();
			registerWithOtp();
		});

	    $(".skip").on('click', function(event) {
	      /* stop form from submitting normally */
	      event.preventDefault();
	      registerSkipWithOtp();
		});

	    $("#social-form-password-forget").submit(function(event) {
	      /* stop form from submitting normally */
	      event.preventDefault();
	      loginForgotPassword();
		});

		$(".edit").on("click", function(){
			$(".get-otp2").hide();
			$("#my_otp").hide();
			$("#my_verify_mobile").hide();
		   	$(".get-otp1").show();
			$("#get_my_otp").show();
		});

		$("#get_my_otp, .my_resend").on("click", function(event){
			event.preventDefault();
			if($(".my_resend").text()=="Resend" && !$(".my_resend").hasClass('disabled')){
			seconds = 30;
				sendOtp();
			}
		});

		$("#my_verify_mobile").on("click", function(){
			verifyOtp();
		});
    });
});