<?php

namespace Mageplaza\SocialLogin\Model;

use Magento\Framework\Model\AbstractModel;

class Attempt extends AbstractModel
{

    protected function _construct()
    {
        $this->_init('Mageplaza\SocialLogin\Model\ResourceModel\Attempt');
    }

}
