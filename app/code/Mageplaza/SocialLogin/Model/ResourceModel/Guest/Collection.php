<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel\Guest;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection

{

    protected function _construct()

    {

        $this->_init('Mageplaza\SocialLogin\Model\Guest', 'Mageplaza\SocialLogin\Model\ResourceModel\Guest');

    }

}

?>