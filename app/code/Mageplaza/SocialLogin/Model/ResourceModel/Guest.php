<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Guest extends AbstractDb

{

    protected function _construct()

    {

        $this->_init('guest_otp', 'id');

    }

}

?>