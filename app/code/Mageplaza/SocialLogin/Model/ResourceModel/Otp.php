<?php

namespace Mageplaza\SocialLogin\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Otp extends AbstractDb

{

    protected function _construct()

    {

        $this->_init('otp_verification', 'id');

    }

}

?>