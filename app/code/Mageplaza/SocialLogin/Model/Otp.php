<?php

namespace Mageplaza\SocialLogin\Model;

use Magento\Framework\Model\AbstractModel;

class Otp extends AbstractModel
{

    protected function _construct()
    {
        $this->_init('Mageplaza\SocialLogin\Model\ResourceModel\Otp');
    }

}
