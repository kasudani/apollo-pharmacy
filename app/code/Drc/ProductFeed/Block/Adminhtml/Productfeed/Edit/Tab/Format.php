<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Block\Adminhtml\Productfeed\Edit\Tab;

class Format extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Type options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\Currency
     */
    protected $currencyOptions;

    /**
     * Store options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceCurrency
     */
    protected $formatPriceCurrencyOptions;

    /**
     * Status options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimals
     */
    protected $formatPriceDecimalsOptions;

    /**
     * Execute Mode options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimalsPoint
     */
    protected $formatPriceDecimalsPointOptions;

    /**
     * Cron Execution Time options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceThousandsSeparator
     */
    protected $formatPriceThousandsSeparatorOptions;


    /**
     * Compress options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\Compress
     */


    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Currency $currencyOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceCurrency $formatPriceCurrencyOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimals $formatPriceDecimalsOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimalsPoint $formatPriceDecimalsPointOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceThousandsSeparator $formatPriceThousandsSeparatorOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Compress $compressOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Drc\ProductFeed\Model\Productfeed\Source\Currency $currencyOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceCurrency $formatPriceCurrencyOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimals $formatPriceDecimalsOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\FeedFormatPriceDecimalsPoint $formatPriceDecimalsPointOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceThousandsSeparator $formatPriceThousandsSeparatorOptions,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
    
        $this->currencyOptions    = $currencyOptions;
        $this->formatPriceCurrencyOptions     = $formatPriceCurrencyOptions;
        $this->formatPriceDecimalsOptions    = $formatPriceDecimalsOptions;
        $this->formatPriceDecimalsPointOptions = $formatPriceDecimalsPointOptions;
        $this->formatPriceThousandsSeparatorOptions    = $formatPriceThousandsSeparatorOptions;
        
        parent::__construct($context, $registry, $formFactory, $data);
    }


    protected function _prepareForm()
    {
        
        $model = $this->_coreRegistry->registry('drc_productfeed_productfeed');
    
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('productfeed_');
        $form->setFieldNameSuffix('productfeed');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Format')]);

        if ($model->getId()) {
            $fieldset->addField('productfeed_id', 'hidden', ['name' => 'productfeed_id']);
        }

        
                
             $fieldset->addField(
                 'currency',
                 'select',
                 [
                 'name'  => 'currency',
                 'label' => __('Currency'),
                 'title' => __('Currency'),
                 'required' => true,
                 'values' => array_merge($this->currencyOptions->toOptionArray()),
                 ]
             );
             
             
            $fieldset->addField(
                'format_price_currency_show',
                'select',
                [
                'name'  => 'format_price_currency_show',
                'label' => __('Show Currency Abbreviation'),
                'title' => __('Show Currency Abbreviation'),
                'required' => true,
                'values' => array_merge($this->formatPriceCurrencyOptions->toOptionArray()),
                ]
            );
            
                $fieldset->addField(
                    'format_price_decimals',
                    'select',
                    [
                    'name'  => 'format_price_decimals',
                    'label' => __('Number of decimal points'),
                    'title' => __('Number of decimal points'),
                    'required' => true,
                    'values' => array_merge($this->formatPriceDecimalsOptions->toOptionArray()),
                    ]
                );
           $fieldset->addField(
               'format_price_decimal_point',
               'select',
               [
                'name'  => 'format_price_decimal_point',
                'label' => __('Separator for the decimal point'),
                'title' => __('Separator for the decimal point'),
                'required' => true,
                'values' => array_merge($this->formatPriceDecimalsPointOptions->toOptionArray()),
               ]
           );
            $fieldset->addField(
                'format_price_thousands_separator',
                'select',
                [
                'name'  => 'format_price_thousands_separator',
                'label' => __('Thousands Separator'),
                'title' => __('Thousands Separator'),
                'required' => true,
                'values' => array_merge($this->formatPriceThousandsSeparatorOptions->toOptionArray()),
                ]
            );
    
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Format');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
