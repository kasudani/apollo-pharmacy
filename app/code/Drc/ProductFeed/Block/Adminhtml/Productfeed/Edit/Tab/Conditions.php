<?php

/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Drc\ProductFeed\Block\Adminhtml\Productfeed\Edit\Tab;

class Conditions extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * Type options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\ProductType
     */
    protected $productTypeOptions;
    
    protected $attributeTypeOptions;
     
    protected $productVisibility;
     
    protected $resultPageFactory;

     /**
     * Compress options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\Compress
     */

    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\Productfeed\Source\ProductType $productTypeOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceProductType $formatPriceProductTypeOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimals $formatPriceDecimalsOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimalsPoint $formatPriceDecimalsPointOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceThousandsSeparator $formatPriceThousandsSeparatorOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Compress $compressOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Drc\ProductFeed\Model\Productfeed\Source\ProductType $productTypeOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\AttributeType $attributeTypeOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\ProductVisibility $productVisibility,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        $this->productTypeOptions = $productTypeOptions;
        $this->attributeTypeOptions = $attributeTypeOptions;
        $this->productVisibility = $productVisibility;
        $this->resultPageFactory = $resultPageFactory;
        
        
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('drc_productfeed_productfeed');

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('productfeed_');
        $form->setFieldNameSuffix('productfeed');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Product Type')]);

        if ($model->getId()) {
            $fieldset->addField('productfeed_id', 'hidden', ['name' => 'productfeed_id']);
        }

        $fieldset->addField(
            'product_types[]',
            'checkboxes',
            [
            'name' => 'product_types[]',
            'values' => $this->productTypeOptions->toOptionArray()
                ]
        );

        $fieldset = $form->addFieldset('base_fieldset2', ['legend' => __('Attribute Set')]);

        $fieldset->addField(
            'attribute_set[]',
            'checkboxes',
            [
            'name' => 'attribute_set[]',
            'values' => $this->attributeTypeOptions->toOptionArray()
                ]
        );
        
        $fieldset = $form->addFieldset('base_fieldset3', ['legend' => __('Product Visibility')]);

        $fieldset->addField(
            'product_visibility[]',
            'checkboxes',
            [
            'name' => 'product_visibility[]',
            'values' => $this->productVisibility->toOptionArray()
                ]
        );
        
        


        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Conditions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
