<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Block\Adminhtml\Productfeed\Edit\Tab;

class Ftp extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Type options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\Currency
     */
    protected $formatPriceCurrencyOptions;

   
    /**
     * Compress options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\Compress
     */


    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Currency $currencyOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceCurrency $formatPriceCurrencyOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimals $formatPriceDecimalsOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceDecimalsPoint $formatPriceDecimalsPointOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceThousandsSeparator $formatPriceThousandsSeparatorOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Compress $compressOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Drc\ProductFeed\Model\Productfeed\Source\FormatPriceCurrency $formatPriceCurrencyOptions,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
    
      
         $this->formatPriceCurrencyOptions     = $formatPriceCurrencyOptions;
        parent::__construct($context, $registry, $formFactory, $data);
        
//       p( $this->formatdate);
//       exit();
    }


    protected function _prepareForm()
    {
        
        $model = $this->_coreRegistry->registry('drc_productfeed_productfeed');
    
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('productfeed_');
        $form->setFieldNameSuffix('productfeed');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('FTP / SFTP')]);

        if ($model->getId()) {
            $fieldset->addField('productfeed_id', 'hidden', ['name' => 'productfeed_id']);
        }
        
          $fieldset->addField(
              'ftp_enabled',
              'select',
              [
                'name'  => 'ftp_enabled',
                'label' => __('Enabled'),
                'title' => __('Enabled'),
                'required' => true,
                 'values' => array_merge($this->formatPriceCurrencyOptions->toOptionArray()),
              ]
          );
         $fieldset->addField(
             'host',
             'text',
             [
                'name'  => 'host',
                'label' => __('Host'),
                'title' => __('Host'),
                'required' => false,
                'after_element_html' => '<small>Add port if necessary. e.g. partnerupload.google.com:19321</small>',
             ]
         );
          $fieldset->addField(
              'delivery_type',
              'select',
              [
                'name'  => 'delivery_type',
                'label' => __('Protocol'),
                'title' => __('Protocol'),
                'required' => false,
                'values' => [
                            ['value'=>'ftp','label'=>'FTP'],
                            ['value'=>'sftp','label'=>'SFTP'],
                           
                       ],
              ]
          );
         
           $fieldset->addField(
               'user',
               'text',
               [
                'name'  => 'user',
                'label' => __('User'),
                'title' => __('User'),
                'required' => false,
               ]
           );
            $fieldset->addField(
                'password',
                'text',
                [
                'name'  => 'password',
                'label' => __('Password'),
                'title' => __('Password'),
                'required' => false,
                ]
            );
              $fieldset->addField(
                  'path',
                  'text',
                  [
                  'name'  => 'path',
                  'label' => __('Path'),
                  'title' => __('Path'),
                  'required' => false,
                  ]
              );
               $fieldset->addField(
                   'passivemode',
                   'select',
                   [
                   'name'  => 'passivemode',
                   'label' => __('Passive Mode'),
                   'title' => __('Passive Mode'),
                   'required' => false,
                   'values' => array_merge($this->formatPriceCurrencyOptions->toOptionArray()),
                   ]
               );


    
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('FTP / SFTP');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
