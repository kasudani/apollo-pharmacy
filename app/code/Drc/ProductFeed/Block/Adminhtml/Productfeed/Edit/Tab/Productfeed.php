<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Block\Adminhtml\Productfeed\Edit\Tab;

class Productfeed extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Type options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\FeedType
     */
    protected $feedTypeOptions;

    /**
     * Store options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\StoreId
     */
    protected $storeIdOptions;

    /**
     * Status options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\IsActive
     */
    protected $isActiveOptions;

    /**
     * Execute Mode options
     *
     * @var \Drc\ProductFeed\Model\Productfeed\Source\ExecuteMode
     */
    protected $executeModeOptions;

    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\Productfeed\Source\FeedType $feedTypeOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\StoreId $storeIdOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\IsActive $isActiveOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\ExecuteMode $executeModeOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\CronTime $cronTimeOptions
     * @param \Drc\ProductFeed\Model\Productfeed\Source\Compress $compressOptions
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Drc\ProductFeed\Model\Productfeed\Source\FeedType $feedTypeOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\StoreId $storeIdOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\IsActive $isActiveOptions,
        \Drc\ProductFeed\Model\Productfeed\Source\ExecuteMode $executeModeOptions,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
    
        $this->feedTypeOptions    = $feedTypeOptions;
        $this->storeIdOptions     = $storeIdOptions;
        $this->isActiveOptions    = $isActiveOptions;
        $this->executeModeOptions = $executeModeOptions;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {


        /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
        $productfeed = $this->_coreRegistry->registry('drc_productfeed_productfeed');
       
        
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('productfeed_');
        $form->setFieldNameSuffix('productfeed');
        
        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Product feed information'),
                'class'  => 'fieldset-wide'
            ]
        );
        if ($productfeed->getId()) {
            $fieldset->addField(
                'productfeed_id',
                'hidden',
                ['name' => 'productfeed_id']
            );
        }
        $fieldset->addField(
            'name',
            'text',
            [
                'name'  => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            ]
        );
        $fieldset->addField(
            'filename',
            'text',
            [
                'name'  => 'filename',
                'label' => __('File Name'),
                'title' => __('File Name'),
                'required' => true,
                'after_element_html' => '<small>Make sure this file name is same as file name set in your merchant account feed but without file extension</small>',
            ]
        );
        $fieldset->addField(
            'feed_type',
            'select',
            [
                'name'  => 'feed_type',
                'label' => __('Type'),
                'title' => __('Type'),
                'required' => true,
                'disabled' => 'disabled',
                'values' => array_merge($this->feedTypeOptions->toOptionArray()),
            ]
        );
        $fieldset->addField(
            'store_id',
            'select',
            [
                'name'  => 'store_id',
                'label' => __('Store'),
                'title' => __('Store'),
                'required' => true,
                'values' => array_merge($this->storeIdOptions->toOptionArray()),
            ]
        );
        $fieldset->addField(
            'is_active',
            'select',
            [
                'name'  => 'is_active',
                'label' => __('Status'),
                'title' => __('Status'),
                'required' => true,
                'values' => array_merge($this->isActiveOptions->toOptionArray()),
            ]
        );
        $fieldset->addField(
            'execute_mode',
            'select',
            [
                'name'  => 'execute_mode',
                'label' => __('Execute Mode'),
                'title' => __('Execute Mode'),
                'required' => true,
                'values' => $this->executeModeOptions->toOptionArray(),
                'after_element_html' => '<small>For "Auto" mode, you can set cron settings from Stores > Configuration and click "Settings" under "DRC SYSTEMS - PRODUCT FEED" tab. In case of "Manual" mode, you must click "Save and Generate" to save and upload feed to FTP/SFTP account</small>',
            ]
        );
        
        $fieldset->addField(
            'format',
            'hidden',
            [
                'name'  => 'format',
                'label' => __('format'),
                'title' => __('format'),
            
            ]
        );

        $productfeedData = $this->_session->getData('drc_productfeed_productfeed_data', true);
       
        if ($productfeedData) {
            $productfeed->addData($productfeedData);
        } else {
            if (!$productfeed->getId()) {
                $productfeed->addData($productfeed->getDefaultValues());
            }
        }
        $form->addValues($productfeed->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
}
