<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Adminhtml\Search;

class Productfeed extends \Magento\Framework\DataObject
{
    /**
     * ProductFeed Collection factory
     *
     * @var \Drc\ProductFeed\Model\ResourceModel\Productfeed\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Backend data helper
     *
     * @var \Magento\Backend\Helper\Data
     */
    protected $adminhtmlData;

    /**
     * constructor
     *
     * @param \Drc\ProductFeed\Model\ResourceModel\Productfeed\CollectionFactory $collectionFactory
     * @param \Magento\Backend\Helper\Data $adminhtmlData
     */
    public function __construct(
        \Drc\ProductFeed\Model\ResourceModel\Productfeed\CollectionFactory $collectionFactory,
        \Magento\Backend\Helper\Data $adminhtmlData
    ) {
    
        $this->collectionFactory = $collectionFactory;
        $this->adminhtmlData     = $adminhtmlData;
        parent::__construct();
    }

    /**
     * Load search results
     *
     * @return $this
     */
    public function load()
    {
        $result = [];
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($result);
            return $this;
        }

        $query = $this->getQuery();
        $collection = $this->collectionFactory->create()
            ->addFieldToFilter('name', ['like' => '%'.$query.'%'])
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();

        foreach ($collection as $productfeed) {
            $result[] = [
                'id' => 'drc_productfeed_productfeed/1/' . $productfeed->getId(),
                'type' => __('ProductFeed'),
                'name' => $productfeed->getName(),
                'description' => $productfeed->getName(),
                'form_panel_title' => __(
                    'ProductFeed %1',
                    $productfeed->getName()
                ),
                'url' => $this->adminhtmlData->getUrl('drc_productfeed/productfeed/edit', ['productfeed_id' => $productfeed->getId()]),
            ];
        }

        $this->setResults($result);

        return $this;
    }
}
