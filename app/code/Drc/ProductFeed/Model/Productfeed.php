<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model;

/**
 * @method Productfeed setName($name)
 * @method Productfeed setFilename($filename)
 * @method Productfeed setFeedType($feedType)
 * @method Productfeed setStoreId($storeId)
 * @method Productfeed setIsActive($isActive)
 * @method Productfeed setExecuteMode($executeMode)
 * @method Productfeed setCronTime($cronTime)
 * @method Productfeed setCompress($compress)
 * @method mixed getName()
 * @method mixed getFilename()
 * @method mixed getFeedType()
 * @method mixed getStoreId()
 * @method mixed getIsActive()
 * @method mixed getExecuteMode()
 * @method mixed getCronTime()
 * @method mixed getCompress()
 * @method Productfeed setCreatedAt(\string $createdAt)
 * @method string getCreatedAt()
 * @method Productfeed setUpdatedAt(\string $updatedAt)
 * @method string getUpdatedAt()
 */
class Productfeed extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Cache tag
     *
     * @var string
     */
    const CACHE_TAG = 'drc_productfeed_productfeed';

    /**
     * Cache tag
     *
     * @var string
     */
    protected $_cacheTag = 'drc_productfeed_productfeed';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'drc_productfeed_productfeed';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Drc\ProductFeed\Model\ResourceModel\Productfeed');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * get entity default values
     *
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
