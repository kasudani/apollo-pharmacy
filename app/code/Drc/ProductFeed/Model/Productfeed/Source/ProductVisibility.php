<?php

/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Drc\ProductFeed\Model\Productfeed\Source;

class ProductVisibility implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => 1,
                'label' => __('Not Visible Individually')
            ],
            [
                'value' => 2,
                'label' => __('Catalog')
            ],
            [
                'value' => 3,
                'label' => __('Search')
            ],
            [
                'value' => 4,
                'label' => __('Catalog, Search')
            ],
            
        ];
        return $options;
    }
}
