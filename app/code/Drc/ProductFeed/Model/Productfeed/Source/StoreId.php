<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class StoreId implements \Magento\Framework\Option\ArrayInterface
{
    protected $storeManagerInterface;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface
    ) {
    
        $this->storeManagerInterface = $storeManagerInterface;
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $stores = $this->storeManagerInterface->getStores();
        foreach ($stores as $store) {
            $options[] = ['value' => $store->getId(), 'label' => $store->getName()];
        }
        return $options;
    }
}
