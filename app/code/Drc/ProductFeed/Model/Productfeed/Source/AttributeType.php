<?php

/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Drc\ProductFeed\Model\Productfeed\Source;

class AttributeType implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
      
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collectionProduct = $objectManager->create('\Magento\Catalog\Model\Product\AttributeSet\Options');

        $options = [];
        foreach ($collectionProduct->toOptionArray() as $key => $value) {
            $options[] = ['value' => $value['value'], 'label' => $value['label']];
        }

        return $options;
    }
}
