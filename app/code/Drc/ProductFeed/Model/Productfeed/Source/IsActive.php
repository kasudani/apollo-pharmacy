<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class IsActive implements \Magento\Framework\Option\ArrayInterface
{
    const ACTIVE = 1;
    const INACTIVE = 2;


    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::ACTIVE,
                'label' => __('Active')
            ],
            [
                'value' => self::INACTIVE,
                'label' => __('Inactive')
            ],
        ];
        return $options;
    }
}
