<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Model\Productfeed\Source;

class Compress implements \Magento\Framework\Option\ArrayInterface
{
    const _EMPTY = 1;
    const ZIP = 2;
    const GZ = 3;
    const BZ2 = 4;


    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::_EMPTY,
                'label' => __('')
            ],
            [
                'value' => self::ZIP,
                'label' => __('zip')
            ],
            [
                'value' => self::GZ,
                'label' => __('gz')
            ],
            [
                'value' => self::BZ2,
                'label' => __('bz2')
            ],
        ];
        return $options;
    }
}
