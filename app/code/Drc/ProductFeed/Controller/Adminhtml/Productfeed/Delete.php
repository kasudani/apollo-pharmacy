<?php
/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */
namespace Drc\ProductFeed\Controller\Adminhtml\Productfeed;

class Delete extends \Drc\ProductFeed\Controller\Adminhtml\Productfeed
{
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('productfeed_id');
        if ($id) {
            $name = "";
            try {
                /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
                $productfeed = $this->productfeedFactory->create();
                $productfeed->load($id);
                $name = $productfeed->getName();
                $productfeed->delete();
                $this->messageManager->addSuccess(__('The product feed has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_drc_productfeed_productfeed_on_delete',
                    ['name' => $name, 'status' => 'success']
                );
                $resultRedirect->setPath('drc_productfeed/*/');
                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_drc_productfeed_productfeed_on_delete',
                    ['name' => $name, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath('drc_productfeed/*/edit', ['productfeed_id' => $id]);
                return $resultRedirect;
            }
        }
        // display error message
        $this->messageManager->addError(__('The product feed to delete was not found.'));
        // go to grid
        $resultRedirect->setPath('drc_productfeed/*/');
        return $resultRedirect;
    }
}
