<?php

/**
 * {{Drc}}_{{ProductFeed}} extension
 *                     NOTICE OF LICENSE
 *
 *                     This source file is subject to the MIT License
 *                     that is bundled with this package in the file LICENSE.txt.
 *                     It is also available through the world-wide-web at this URL:
 *                     http://opensource.org/licenses/mit-license.php
 *
 *                     @category  {{Drc}}
 *                     @package   {{Drc}}_{{ProductFeed}}
 *                     @copyright Copyright (c) {{2017}}
 *                     @license   http://opensource.org/licenses/mit-license.php MIT License
 */

namespace Drc\ProductFeed\Controller\Adminhtml\Productfeed;

class Edit extends \Drc\ProductFeed\Controller\Adminhtml\Productfeed
{

    /**
     * Backend session
     *
     * @var \Magento\Backend\Model\Session
     */
    protected $backendSession;

    /**
     * Page factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Result JSON factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * constructor
     *
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Drc\ProductFeed\Model\ProductfeedFactory $productfeedFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->backendSession = $context->getSession();
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($productfeedFactory, $registry, $context);
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Drc_ProductFeed::productfeed');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {


        $id = $this->getRequest()->getParam('productfeed_id');
        /** @var \Drc\ProductFeed\Model\Productfeed $productfeed */
        $productfeed = $this->initProductfeed();
     
        $set =  $this->getRequest()->getParam('set');
        if (isset($set) && trim($set)!='') {
            switch ($set) {
                case 'google':
                    $contentArr = ['header'=>'<?xml version="1.0"?> <rss version="2.0" xmlns:g="http://base.google.com/ns/1.0"> <channel>','item'=>'item','content'=>'<g:id>{attr_sku}</g:id>
<title>{attr_name}</title>
<description>{attr_desc}</description>
<g:product_type>{attr_type}</g:product_type>
<link>{attr_link}</link>
<g:image_link>{attr_image_link}</g:image_link>
<g:condition>{attr_condition}</g:condition>
<g:availability>{attr_availability}</g:availability>
<g:price>{attr_price}</g:price>
<g:brand>{attr_manufacturer}</g:brand>
<g:google_product_category>GOOGLE CATEGORIES (https://support.google.com/merchants/answer/160081)</g:google_product_category>
<g:tax>
 <g:country>US</g:country>
 <g:rate>0</g:rate>
 <g:tax_ship>no</g:tax_ship>
</g:tax>
<g:shipping>
 <g:country>US</g:country>
 <g:price>0 USD</g:price>
</g:shipping>
<g:identifier_exists>{attr_identifier}</g:identifier_exists>','footer'=>'</channel> </rss>'];
                    $productfeed->setData('name', 'Google Feed');
                    $productfeed->setData('filename', 'google_feed');
                    $productfeed->setData('feed_type', 2);
                    $productfeed->setData('is_active', 1);
                    //$productfeed->setData('store_id'
                    $productfeed->setData('execute_mode', 1);
                    $productfeed->setData('content', json_encode($contentArr));
                    $productfeed->save();
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath(
                        'drc_productfeed/*/edit',
                        [
                        'productfeed_id' => $productfeed->getId(),
                        'set' => '',
                        '_current' => true
                        ]
                    );
                    return $resultRedirect;
                    break;
                case 'bing':
                    break;
                case 'shopping':
                    break;
                default:
                    break;
            }
        }
 
        $format = json_decode($productfeed->getData('format'), true);
        $conditions = json_decode($productfeed->getData('conditions'), true);
        $delivery =  json_decode($productfeed->getData('delivery'), true);
        $analytics =  json_decode($productfeed->getData('analytics'), true);
        $content = json_decode($productfeed->getData('content'), true);

//        p($content);
//        exit();
     
        $data1 =
            [
            'productfeed_id' => $productfeed->getData('productfeed_id'),
            'name' => $productfeed->getData('name'),
            'filename' => $productfeed->getData('filename'),
            'feed_type' => $productfeed->getData('feed_type'),
            'store_id' => $productfeed->getData('store_id'),
            'is_active' => $productfeed->getData('is_active'),
            'execute_mode' => $productfeed->getData('execute_mode'),
            'cron_time' => $productfeed->getData('cron_time'),
            'format' => $productfeed->getData('format'),
            'conditions' => $productfeed->getData('conditions'),
            'delivery' => $productfeed->getData('delivery'),
            'analytics' => $productfeed->getData('analytics'),
            'compress' => $productfeed->getData('compress'),
            'created_at' => $productfeed->getData('created_at'),
            'updated_at' => $productfeed->getData('updated_at'),
            'currency' => isset($format['currency']) ? $format['currency'] : "",
            'format_price_currency_show' => isset($format['format_price_currency_show']) ? $format['format_price_currency_show'] : "",
            'format_price_decimals' => isset($format['format_price_decimals']) ? $format['format_price_decimals'] : "",
            'format_price_decimal_point' => isset($format['format_price_decimal_point']) ? $format['format_price_decimal_point'] : "",
            'format_price_thousands_separator' => isset($format['format_price_thousands_separator']) ? $format['format_price_thousands_separator'] : "",
            'format_date' => isset($format['format_date']) ? $format['format_date'] : "",
            'product_types[]'=> !empty($conditions['product_types']) ? explode(",", $conditions['product_types']) :$conditions['product_types'],
            'attribute_set[]'=> !empty($conditions['attribute_set']) ? explode(",", $conditions['attribute_set']) :$conditions['attribute_set'],
            'product_visibility[]'=> !empty($conditions['product_visibility']) ? explode(",", $conditions['product_visibility']) :$conditions['product_visibility'],
            'ftp_enabled' => !empty($delivery['ftp_enabled']) ? $delivery['ftp_enabled'] : 1,
            'host' => !empty($delivery['host']) ? $delivery['host'] : "",
            'delivery_type' => !empty($delivery['delivery_type']) ? $delivery['delivery_type'] : "ftp",
            'user' => !empty($delivery['user']) ? $delivery['user'] : "",
             'password' => !empty($delivery['password']) ? $delivery['password'] : "",
            'path' => !empty($delivery['path']) ? $delivery['path'] : "",
            'passivemode' => !empty($delivery['passivemode']) ? $delivery['passivemode'] : 1,
            'feed_utm_source'=> !empty($analytics['feed_utm_source']) ? $analytics['feed_utm_source'] : "",
            'utm_medium'=> !empty($analytics['utm_medium']) ? $analytics['utm_medium'] : "",
            'utm_term'=> !empty($analytics['utm_term']) ? $analytics['utm_term'] : "",
            'utm_content'=> !empty($analytics['utm_content']) ? $analytics['utm_content'] : "",
            'utm_campaign'=> !empty($analytics['utm_campaign']) ? $analytics['utm_campaign'] : "",
            'header'=> !empty($content['header']) ? $content['header'] : "",
             'item'=> !empty($content['item']) ? $content['item'] : "",
             'content'=> !empty($content['content']) ? $content['content'] : "",
            'footer'=> !empty($content['footer']) ? $content['footer'] : "",
            
              ];


        /** @var \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Drc_ProductFeed::productfeed');
        $resultPage->getConfig()->getTitle()->set(__('ProductFeeds'));

        if ($id) {
            $productfeed->load($id);

            if (!$productfeed->getId()) {
                $this->messageManager->addError(__('This ProductFeed no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath(
                    'drc_productfeed/*/edit',
                    [
                    'productfeed_id' => $productfeed->getId(),
                    '_current' => true
                        ]
                );
                return $resultRedirect;
            }
        }
        $title = $productfeed->getId() ? $productfeed->getName() : __('New ProductFeed');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $data = $this->backendSession->getData('drc_productfeed_productfeed_data', true);
        
        $productfeed->setData($data1);
        if (!empty($data)) {
            $productfeed->setData($data);
        }

        return $resultPage;
    }
}
