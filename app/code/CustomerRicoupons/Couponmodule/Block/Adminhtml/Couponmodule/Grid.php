<?php
namespace CustomerRicoupons\Couponmodule\Block\Adminhtml\Couponmodule;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \CustomerRicoupons\Couponmodule\Model\couponmoduleFactory
     */
    protected $_couponmoduleFactory;

    /**
     * @var \CustomerRicoupons\Couponmodule\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \CustomerRicoupons\Couponmodule\Model\couponmoduleFactory $couponmoduleFactory
     * @param \CustomerRicoupons\Couponmodule\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \CustomerRicoupons\Couponmodule\Model\CouponmoduleFactory $CouponmoduleFactory,
        \CustomerRicoupons\Couponmodule\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_couponmoduleFactory = $CouponmoduleFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_couponmoduleFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );


		
				$this->addColumn(
					'coupon_code',
					[
						'header' => __('Coupon Code'),
						'index' => 'coupon_code',
					]
				);
				
				$this->addColumn(
					'coupon_description',
					[
						'header' => __('Coupon Description'),
						'index' => 'coupon_description',
					]
				);
				
				$this->addColumn(
					'coupon_name',
					[
						'header' => __('Coupon Name'),
						'index' => 'coupon_name',
					]
				);
				
				$this->addColumn(
					'discount_percent',
					[
						'header' => __('Discount Percent'),
						'index' => 'discount_percent',
					]
				);
                $this->addColumn(
                    'from_date',
                    [
                        'header' => __('From Date'),
                        'index' => 'from_date',
                    ]
                );
                $this->addColumn(
                    'to_date',
                    [
                        'header' => __('To Date'),
                        'index' => 'to_date',
                    ]
                );
                $this->addColumn(
                    'product_skus',
                    [
                        'header' => __('Products'),
                        'index' => 'product_skus',
                    ]
                );
		
		   $this->addExportType($this->getUrl('couponmodule/*/exportCsv', ['_current' => true]),__('CSV'));
		   $this->addExportType($this->getUrl('couponmodule/*/exportExcel', ['_current' => true]),__('Excel XML'));

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

	
    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {

        $this->setMassactionIdField('id');
        
        $this->getMassactionBlock()->setFormFieldName('couponmodule');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('couponmodule/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        $statuses = $this->_status->getOptionArray();

        $this->getMassactionBlock()->addItem(
            'status',
            [
                'label' => __('Change status'),
                'url' => $this->getUrl('couponmodule/*/massStatus', ['_current' => true]),
                'additional' => [
                    'visibility' => [
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Status'),
                        'values' => $statuses
                    ]
                ]
            ]
        );


        return $this;
    }
		

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('couponmodule/*/index', ['_current' => true]);
    }

    /**
     * @param \CustomerRicoupons\Couponmodule\Model\couponmodule|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
		
        return $this->getUrl(
            'couponmodule/*/edit',
            ['id' => $row->getId()]
        );
		
    }

	

}