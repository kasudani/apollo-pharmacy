<?php

namespace CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CustomerRicoupons\Couponmodule\Model\Couponmodule', 'CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>