<?php
namespace CustomerRicoupons\Couponmodule\Model;

class Couponmodule extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('CustomerRicoupons\Couponmodule\Model\ResourceModel\Couponmodule');
    }
}
?>