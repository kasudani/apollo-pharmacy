<?php

namespace Ri\Abandonedemail\Model\ResourceModel\Abandonedemail;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ri\Abandonedemail\Model\Abandonedemail', 'Ri\Abandonedemail\Model\ResourceModel\Abandonedemail');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
