<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Checkout\Controller\Index;

class Index extends \Magento\Checkout\Controller\Onepage
{
    /**
     * Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Checkout\Helper\Data $checkoutHelper */
        $checkoutHelper = $this->_objectManager->get('Magento\Checkout\Helper\Data');
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        $deliveryMethod = $checkoutSession->getDeliveryMethod();
        $orderType = $checkoutSession->getItemsTypeInQuote();
        if($orderType == ""){
            $this->getquoteItemsType();
            $orderType = $checkoutSession->getItemsTypeInQuote();
        }

        if($deliveryMethod == "" && $orderType != "Fmcg"){
            $this->messageManager->addError(__('Please select delivery method.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }


        if (!$checkoutHelper->canOnepageCheckout()) {
            $this->messageManager->addError(__('One-page checkout is turned off.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError() || !$quote->validateMinimumAmount()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

		/*
		//$items = $quote->getAllItems();
		//print "<pre>"; print_r($items); exit;	
		//print "i am here"; exit;


		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 

		$itemsCollection = $cart->getQuote()->getItemsCollection();
		$itemsVisible = $cart->getQuote()->getAllVisibleItems();
		$items = $cart->getQuote()->getAllItems();

		$this->test2();
		exit();

		foreach($items as $item) 
		{
			echo 'ID: '.$item->getProductId().'<br />';
			echo 'Name: '.$item->getName().'<br />';
			echo 'Sku: '.$item->getSku().'<br />';
			echo 'Quantity: '.$item->getQty().'<br />';
			echo 'Price: '.$item->getPrice().'<br />';
			echo "<br />";            
		}
		exit;
		//print "<pre>"; print_r($items); exit;
		*/
	

        if (!$this->_customerSession->isLoggedIn() && !$checkoutHelper->isAllowedGuestCheckout($quote)) {
            $this->messageManager->addError(__('Guest checkout is disabled.'));
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        $this->_customerSession->regenerateId();
        $this->_objectManager->get('Magento\Checkout\Model\Session')->setCartWasUpdated(false);
        $this->getOnepage()->initCheckout();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Checkout'));
        return $resultPage;
		
    }

    public function getquoteItemsType(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart1 = $objectManager->get('\Magento\Checkout\Model\Cart'); // retrieve quote items collection
        $itemsCollection = $cart1->getQuote()->getItemsCollection(); // get array of all items what can be display directly
        $cart_products=$itemsCollection->getData();
        $pharma_count=0;
        foreach ($cart_products as $product) {
            $productType = $objectManager->create('Apollo\AssignOrders\Observer\Updateorderinfo')->getOrderTypes($product['product_id'],$objectManager);
            $orderTypes[] = $productType;
            if($productType=="Pharma")
                $pharma_count++;
        }
        $orderTypes = array_unique($orderTypes);
        if(count($orderTypes) > 1){
            $orderType = "Both";
        }else if(isset($orderTypes[0])){
            $orderType = $orderTypes[0];
        }else{
            $orderType = "Pharma";
        }
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $checkoutSession->setItemsTypeInQuote($orderType);
        return true;
    }
	
	
}
