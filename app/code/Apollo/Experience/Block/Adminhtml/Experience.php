<?php
namespace Apollo\Experience\Block\Adminhtml;

class Experience extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Apollo_Experience';
        $this->_controller = 'adminhtml_experience';
        $this->_headerText = __('Experience List');
        $this->_addButtonLabel = __('Add New Experience');
        parent::_construct();
    }
}
