<?php
namespace Apollo\Experience\Block\Adminhtml\Experience;


class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Page\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Apollo\Experience\Model\Experience $experience
     * @param \Apollo\Experience\Model\ResourceModel\Experience\CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Apollo\Experience\Model\Experience $collectionFactory,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('experienceListGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
    }

    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', ['header' => __('ID'), 'index' => 'id']);
        $this->addColumn('order_id', ['header' => __('Order ID'), 'index' => 'order_id']);
        $this->addColumn('customer_name', ['header' => __('Customer Name'), 'index' => 'customer_name']);
        $this->addColumn('customer_email', ['header' => __('Customer Email'), 'index' => 'customer_email']);
        $this->addColumn('experience', ['header' => __('Experience'), 'index' => 'experience']);
        $this->addColumn('delivery_arrive', ['header' => __('Delivery Arrive'), 'index' => 'delivery_arrive']);
        return parent::_prepareColumns();
    }
    /**
     * Row click url
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }
}
