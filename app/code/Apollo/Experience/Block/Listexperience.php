<?php
namespace Apollo\Experience\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Main experience list block
 */
class Listexperience extends \Magento\Framework\View\Element\Template
{
	protected $experienceFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

	public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Apollo\Experience\Model\Experience $experienceFactory,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    array $data = []
	) { 
	    $this->experienceFactory = $experienceFactory;

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
	}

	public function getExperiences(){
		return $this->experienceFactory->getCollection();
	}


   /**
     * Set news collection
     */
    protected  function _construct()
    {
        parent::_construct();
        $collection = $this->experienceFactory->getCollection()
            ->setOrder('id', 'DESC');
        $this->setCollection($collection);
    }
 
   /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
           'Magento\Theme\Block\Html\Pager',
           'apollo.experience.list.pager'
        );
        $pager->setLimit(20)
            ->setShowAmounts(false)
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
 
        return $this;
    }
 
   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

}
