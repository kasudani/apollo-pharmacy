<?php

namespace Apollo\Experience\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Apollo\Experience\Model\ExperienceFactory;
use Magento\Framework\Controller\ResultFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultFactory;
    protected $moduleFactory;

    protected $session;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        ResultFactory $resultFactory,
        ExperienceFactory $moduleFactory
    )
    {
        $this->session = $customerSession;
        $this->moduleFactory = $moduleFactory;
        parent::__construct($context);
        $this->resultFactory = $resultFactory;
    }
    public function execute()
    {   
        $model = $this->moduleFactory->create();
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $data = $this->getRequest()->getPostValue();
        if(!empty($data)){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerSession = $objectManager->create('Magento\Customer\Model\Session');
            if($customerSession->isLoggedIn()) {
                $data['customer_name'] = $customerSession->getCustomer()->getName();
                $data['customer_email'] =  $customerSession->getCustomer()->getEmail();
            } else {
                $data['customer_name'] = "";
                $data['customer_email'] = "";
            }
            $model->setData($data);
            $model->save();
            $resultJson->setData(array('success'=>true,'msg'=>'Experience data saved successfully.'));
            $this->messageManager->addSuccess(__('Experience data saved successfully.'));
        } else {
            $resultJson->setData(array('success'=>false,'msg'=>'Something was wrong.'));
        }
        return $resultJson;
    }

}
