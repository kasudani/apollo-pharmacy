<?php

namespace Apollo\Experience\Controller\Adminhtml;


abstract class Index extends \Magento\Backend\App\Action
{
    
    protected $_coreRegistry = null;

    
    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry)
    {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Apollo_Experience::list')
            ->addBreadcrumb(__('Experience'), __('Experience'));
        return $resultPage;
    }

    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Experience::elements');
    }
}
