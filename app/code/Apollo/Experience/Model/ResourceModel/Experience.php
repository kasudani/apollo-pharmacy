<?php
namespace Apollo\Experience\Model\ResourceModel;
class Experience extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('apollo_experiences','id');
    }
}