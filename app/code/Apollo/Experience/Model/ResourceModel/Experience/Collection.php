<?php
namespace Apollo\Experience\Model\ResourceModel\Experience;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Apollo\Experience\Model\Experience','Apollo\Experience\Model\ResourceModel\Experience');
    }
}