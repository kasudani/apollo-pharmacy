<?php
namespace Apollo\Healthareas\Controller\Adminhtml\Healtharea;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;
use Apollo\Healthareas\Api\Data\HealthareaInterface;
use \Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;;

class Save extends \Magento\Backend\App\Action
{

    protected $uploaderFactory;
    
    protected $adapterFactory;
    
    protected $filesystem;

    public function __construct(
        Context $context,
        UploaderFactory $uploaderFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem
    ) {
        $this->uploaderFactory = $uploaderFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Apollo_Healthareas::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            
            $model = $this->_objectManager->create('Apollo\Healthareas\Model\Healtharea');

            $id = $this->getRequest()->getParam('healtharea_id');
            
            if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                try {
                    $uploaderFactory = $this->uploaderFactory->create(['fileId' => 'image']);
                    $uploaderFactory->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $imageAdapter = $this->adapterFactory->create();
                    $uploaderFactory->addValidateCallback('image',$imageAdapter,'validateUploadFile');
                    $uploaderFactory->setAllowRenameFiles(true);
                    $uploaderFactory->setFilesDispersion(true);
                    $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
                    $destinationPath = $mediaDirectory->getAbsolutePath('healtharea/images');
                    $result = $uploaderFactory->save($destinationPath);
                    if (!$result) {
                        throw new LocalizedException(
                            __('File cannot be saved to path: $1', $destinationPath)
                        );
                    }
                    $imagePath = $result['file'];
                    $data['image'] = $imagePath;
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('*/*/edit');
                }
            }

            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'healtharea_prepare_save',
                ['healtharea' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved this Health Area.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['healtharea_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the health area.'));
            }
            return $resultRedirect->setPath('*/*/');
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['healtharea_id' => $this->getRequest()->getParam('healtharea_id')]);

        }
        return $resultRedirect->setPath('*/*/');
    }
}