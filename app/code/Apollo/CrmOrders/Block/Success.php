<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */

namespace Apollo\CrmOrders\Block;

use Magento\Sales\Model\Order;

class Success  extends \Magento\Framework\View\Element\Template
{
       protected $_checkoutSession;
	protected $x;
	protected $_objectManager;

	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\Order $salesOrderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
   	 ) {
        $this->_checkoutSession = $checkoutSession;
	 //$this->_objetManger = $objectManager;
        parent::__construct($context, $data);
 }

 public function getSomething()
 {
		$order = $this->_checkoutSession->getLastOrderId();
		// echo "|".$order."|";

		$https = ((!empty($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] != 'off')) ? true : false;

		if($https) {
		    $uurl = "https://";
		} else {
		    $uurl = "http://";
		}
	
		 $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
      		 $connection = $this->_resources->getConnection();
     		 $themeTable = $this->_resources->getTableName('sales_order_grid');

		$urlp=$_SERVER['HTTP_HOST'];
		$userData = array("username" => "apolloadm", "password" => "VarshiniG9");
		$ch = curl_init($uurl.$urlp."/index.php/rest/V1/integration/admin/token");
	

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));
		$token = curl_exec($ch);		
 	       $ch2 = curl_init($uurl.$urlp."/index.php/rest/V1/orders/".$order);  // to display particluar order
		curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch2, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
		$resultt = curl_exec($ch2);
		curl_close($ch2);
		$resultt = json_decode($resultt, 1);

		//echo "<pre>";
		//print_r($resultt);

 		$sqlord = "select order_types,delivery_info from sales_order_grid where entity_id='".$order."'";		
		$roword=$connection->fetchrow($sqlord);
		$rowcount=count($roword);
		
//print_r($roword);

$presimg="";
$x="";
$y="";
$z="";
$i=0;
$j=0;
		if($rowcount>=1 && $roword['delivery_info']!="")
		{
			
	    		$bb=explode('"previous_prescriptions";',$roword['delivery_info']);

   			 $regex = '/".*?"/';
   			 $match=array();
			
			
	   		 preg_match_all($regex, $bb[1], $match);

 			
			if(count($match[0])>=1)
			{
		 	   for($i=0;$i<count($match[0]);$i++)
		 	   {
		 	      $sqlimg = 'select uploaded_file from ordermedicine_data where id='.$match[0][$i];
		 	       $rowimg=$connection->fetchrow($sqlimg);

				$presimg .=$uurl.$urlp."/pub/media/medicine_prescription/".$rowimg['uploaded_file'].",";
   		 	   }
			}
		  
		
		}
		else
		{
			$presimg="";
		}
		$i=0;
		if(isset($resultt['items'])){
		foreach($resultt['items'] as $itms)
		{
			$dt=explode(" ",$itms['created_at']);
			$dtf=date_create($dt[0]);
			$date=date_format($dtf,"Y-M-d");

		       $x[$i] =array("ItemID"=>$itms['sku'],"ItemName"=>$itms['name'],"Qty"=>$itms['qty_ordered'],"orddt"=>$date);
			$i++;
		}  }

		$z =array("Totalamount"=>$resultt['payment']['amount_ordered'],"Paymentsource"=>$resultt['payment']['method']);
  
		foreach($resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['street'] as $addr)
		{
			   $y.=$addr." ";
		}

	$cust=array(
	"Mobileno"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['telephone'],
	"Comm_addr"=>$y,
	"Del_addr"=>$y,
	"FirstName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'],
	"LastName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname'],
	"City"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['city'],	
	"Postcode"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['postcode'],
	"Mailid"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['email'],
	"Age"=>"30",	
	"Cardno"=>"00",
	"PatientName"=>$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['firstname'].$resultt['extension_attributes']['shipping_assignments'][0]['shipping']['address']['lastname']
	);

	$tpdetails=array(
	"Orderid"=>$resultt['increment_id'],
	"Shopid"=>"16001",
	"ShippingMethod"=>$resultt['shipping_description'],
	"PaymentMethod"=>$resultt['payment']['method'],	
	"Vendorname"=>"online",
	"DotorName"=>"Apollo",
	"url"=>$presimg,
	"Ordertype"=>$roword['order_types'],
	"Customerdetails"=>$cust,
	"Paymentdetails"=>$z,
	"itemdetails"=>$x
	);

	$arr=array("tpdetails"=>$tpdetails);
 	
	//echo "<pre>";
	//print_r(json_encode($arr));

	

	$curl=curl_init("http://220.225.226.198:51/Onlineorder.svc/PLACE_ORDERS");
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($arr));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($arr))));

	$tokenn = curl_exec($curl);

	$res=json_decode($tokenn, TRUE);

	//print_r($res);

		if($res['ordersResult']['Status']=="Success" || $res['ordersResult']['Status']=="success")
		{
			$ins="insert into crmorders_track (orderid,orderdt,crmstatus,entityid) values('".$resultt['increment_id']."','".date('Y-m-d')."','0','".$order."')";
			$connection->query($ins);

		}
		else
		{
			$ins="insert into crmorders_track (orderid,orderdt,crmstatus,entityid) values('".$resultt['increment_id']."','".date('Y-m-d')."','1','".$order."')";
			$connection->query($ins);
		}
				
	 return '';
    }
}