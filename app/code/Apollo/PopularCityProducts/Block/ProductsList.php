<?php
namespace Apollo\PopularCityProducts\Block;

class ProductsList extends \Magento\Framework\View\Element\Template
{   

    protected $logger;

    protected $objectManager;

    protected $_curl;

    protected $_productCollectionFactory;
    protected $listProductBlock;

    /**
    * @param \Magento\Framework\View\Element\Template\Context $context
    * @param array $data
    */
    
    public function __construct(
        \Psr\Log\LoggerInterface $logger, 
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
    ) {
        $this->logger = $logger;
        $this->_curl = $curl;
        $this->objectManager = $objectManager;
        $this->_productCollectionFactory = $productCollectionFactory;  
        $this->listProductBlock = $listProductBlock;
        parent::__construct($context);
    }

    function getCityName() {
        $visitorIp = $this->getVisitorIp();
        $response = @unserialize( @file_get_contents('http://www.geoplugin.net/php.gp?ip='.$visitorIp ) );
        $cityName = @strtolower($response['geoplugin_city']);
        return $cityName;        
    }

    function getVisitorIp() {       
        $remoteAddress = $this->objectManager->create('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
        return $remoteAddress->getRemoteAddress();
    }

    function getProductBySku(){
        /*get city name*/
        $cityName = $this->getCityName();
        /*read product sku by json file*/
        $file_path = $this->getFileUrl('popular_city_products_list.json');
        $json = @file_get_contents($file_path);
        $jsonData = json_decode($json,true);
        $collection = '';
        if(!empty($jsonData)) {
            $skus = "";
            if(isset($jsonData[$cityName])){
                $skus =  $jsonData[$cityName];
                $collection = $this->_productCollectionFactory->create();
                $collection->addAttributeToSelect('*');
                $collection->addFieldToFilter('sku', array('in' => explode(",", $skus)));
                //$collection->setPageSize(52);
                return $collection;
            }
        }
        return $collection;
    }

    /**
     * get file url by file file name
     * @param $file
     * @return string
     */
    public function getFileUrl($file)
    {
        return $this->getFileBaseUrl(). $file;
    }

    /**
    * get file url
    * @return mixed
    */
    function getFileBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
 

    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }

}