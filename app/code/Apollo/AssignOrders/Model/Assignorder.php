<?php

namespace Apollo\AssignOrders\Model;

use Magento\Framework\Model\AbstractModel;

class AssignOrder extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Apollo\AssignOrders\Model\ResourceModel\AssignOrder');
    }
}