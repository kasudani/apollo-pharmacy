<?php
namespace Apollo\AssignOrders\Model\ResourceModel;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected function _construct()
	{
		$this->_init('Apollo\AssignOrders\Model\Assignorder','Apollo\AssignOrders\Model\ResourceModel\Assignorder');
	}

}

