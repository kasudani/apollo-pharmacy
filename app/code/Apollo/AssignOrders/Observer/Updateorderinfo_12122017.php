<?php

namespace Apollo\AssignOrders\Observer;

class Updateorderinfo implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //$order = $observer->getData('order');
        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/orderinfo.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $orderIds = $observer->getEvent()->getOrderIds();//array(7904,7905,7906);//
        foreach($orderIds as $orderId) {
            $logger->info("Order Id ===>".$orderId);
            $orderCollectionFactory = $objectManager->get('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
            $order = $orderCollectionFactory->create()->addFieldToSelect('*')->addFieldToFilter('entity_id', $orderId)->getFirstItem();

            //echo "<pre>";print_R($order->getData());die;
            $phone = $this->getPhone($order);

            $SkusAndOrdertypes = $this->getSkusAndOrdertypes($order, $objectManager);
            $skus = $SkusAndOrdertypes['skus'];
            $orderTypes = $SkusAndOrdertypes['order_types'];
            $orderInfo['order_types'] = $orderTypes;
            $orderInfo['order_skus'] = $skus;
            $orderInfo['order_mobile'] = $phone;
            //echo $orderId;
            //print_r($orderInfo);die;
            $this->updateOrderInf($orderId, $orderInfo, $objectManager);
        }
        return $this;
    }

    public function getSkusAndOrdertypes($order,$objectManager){
        $skus = array(); $orderTypes = array();
        $orderedItems = $order->getAllVisibleItems();
        foreach ($orderedItems as $orderedItem){
            $skus[] = $orderedItem->getSku();
            $productId = $orderedItem->getProductId();
            $orderTypes[] = $this->getOrderTypes($productId,$objectManager);
        }
        $orderTypes = array_unique($orderTypes);
        if(count($orderTypes) > 1){
            $orderType = "Both";
        }else if(isset($orderTypes[0])){
            $orderType = $orderTypes[0];
        }else{
            $orderType = "Pharma";
        }
        return array('skus' => implode(",",$skus),'order_types' => $orderType);
    }

    public function getPhone($order){
        $billingAddress = $order->getBillingAddress();
        return $billingAddress->getTelephone();
    }

    public function getOrderTypes($productId,$objectManager){
        $productData = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        $categoryFactory = $objectManager->create('Magento\Catalog\Model\CategoryFactory');
        $category_id_ary = $productData->getCategoryIds();
        $category_names_array = array();
        if(count($category_id_ary)){
            foreach($category_id_ary as $category_id){
                $_category = $categoryFactory->create()->load($category_id);
                $category_names_array[] = $_category->getName();
            }
        }
        $pharma = false;
        $fmcg = false;
        $order_type = "";
        $category_names = array_unique($category_names_array);

        if(in_array("PHARMA",$category_names))    {
            $pharma = true; $order_type = "Pharma";
        }
        if(in_array("FMCG",$category_names)) {
            $fmcg = true; $order_type = "Fmcg";
        }

        if($pharma == true && $fmcg == true){
            $order_type = "Both";
        }
        return (!empty($order_type) ? $order_type : "Fmcg");
    }

    public function updateOrderInf($orderId,$orderInfo,$objectManager){
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $deliveryInfo = $checkoutSession->getDeliveryInfo();
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('sales_order_grid');
        if(is_array($orderInfo['order_skus'])) { $orderInfo['order_skus'] = implode(",",$orderInfo['order_skus']); }
        if(is_array($orderInfo['order_types'])) { $orderInfo['order_types'] = implode(",",$orderInfo['order_types']); }
        if(is_array($orderInfo['order_mobile'])) { $orderInfo['order_mobile'] = implode(",",$orderInfo['order_mobile']); }
        $sql = "Update  $tableName Set order_types = '".$orderInfo['order_types']."',order_skus = '".$orderInfo['order_skus']."',
                            order_mobile = '".$orderInfo['order_mobile']."',
                            delivery_info = '".$deliveryInfo."' where entity_id = $orderId";
        $connection->query($sql);
        return true;
    }
}