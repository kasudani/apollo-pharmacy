<?php
/**
 * Copyright © 2017 Sam Granger. All rights reserved.
 *
 * @author Sam Granger <sam.granger@gmail.com>
 */
namespace Apollo\AssignOrders\Plugin;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\View\Page\Config;

class OrdertypeBodyClassPlugin implements ObserverInterface
{
    protected $config;
    public function __construct(
        Config $config
    ){
        $this->config = $config;
    }
    public function execute(Observer $observer){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $deliveryMethod = $checkoutSession->getDeliveryMethod();
        $orderType = $checkoutSession->getItemsTypeInQuote();
        if($orderType == ""){
            //$objectManager->get('Magento\Checkout\Controller\Index\Index')->getquoteItemsType();
            //$orderType = $checkoutSession->getItemsTypeInQuote();
        }
        $catalogSession = $objectManager->get('Magento\Catalog\Model\Session');
        $existingPincode = $catalogSession->getCustomerPincode();
        $deliveryPincode = $checkoutSession->getSelectedPincode();
        $pincodesession = $checkoutSession->getPincodesession();

        if($deliveryPincode!=""){
            $existingPincode = $deliveryPincode;
        }
        if($pincodesession!=""){
            $existingPincode = $pincodesession;
        }
        $codAvailable = 1;
        if($existingPincode != ""){
            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCodShippingAvailbility($existingPincode);
            if($orderType == "Pharma"){
                $codAvailable = ($result['pharma_cod'] == 0) ? 0 : 1;
            }else if($orderType == "Fmcg"){
                $codAvailable = ($result['fmcg_cod'] == 0) ? 0 : 1;
            }else {
                $codAvailable = ($result['fmcg_cod'] == 1 && $result['pharma_cod'] == 1) ? 1 : 0;
            }
        }

        $codClass = ($codAvailable == 1) ? "cod_available" : "cod_not_available";
        $orderType = strtolower($orderType);
        $deliveryMethod = strtolower($deliveryMethod);
        $this->config->addBodyClass($orderType);
        $this->config->addBodyClass($deliveryMethod);
        $this->config->addBodyClass($codClass);
    }
}