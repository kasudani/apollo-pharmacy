<?php
namespace Apollo\AssignOrders\Ui\Component\Sales\Order\MassAction;
use Magento\Framework\UrlInterface;
use Zend\Stdlib\JsonSerializable;
use Apollo\AssignOrders\Model\ResourceModel\AssignOrder\CollectionFactory;

/**
 * Class Options
 */
class Assignorder implements JsonSerializable
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Additional options params
     *
     * @var array
     */
    protected $data;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Base URL for subactions
     *
     * @var string
     */
    protected $urlPath;

    /**
     * Param name for subactions
     *
     * @var string
     */
    protected $paramName;

    /**
     * Additional params for subactions
     *
     * @var array
     */
    protected $additionalData = [];

    /**
     * Constructor
     *
     * @param CollectionFactory $collectionFactory
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->data = $data;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get action options
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $i=0;
        if ($this->options === null) {

           //  $options = array(
           //     array(
           //         "value" => "1",
           //         "label" => ('Hyderabad'),
           //     ),
           //     array(
           //         "value" => "2",
           //         "label" => ('Delhi'),
           //     ),
           //     array(
           //         "value" => "3",
           //         "label" => ('Bangalore'),
           //     )
           // );
            $objectManager =   \Magento\Framework\App\ObjectManager::getInstance();
            $connection = $objectManager->get('Magento\Framework\App\ResourceConnection')->getConnection('\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION'); 
            $result1 = $connection->fetchAll("SELECT * FROM admin_user");

            //echo "<pre>";print_r($result1);exit();
            // get the massaction data from the database table
            //$templateCollection = $this->collectionFactory->create();
            //$newsCollection = $templateCollection->getCollection();
            //$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            //$logger = new \Zend\Log\Logger();
            //$logger->addWriter($writer);
            //$logger->info($templateCollection);


            // if(!count($templateCollection)){
            //     return $this->options;
            // }
            //make a array of massaction
             foreach ($result1 as $key => $badge) {
                $user_id = $badge['user_id'];
                $username = $badge['username'];
                //echo "<pre>";print_r($badge['user_id']);
                //echo "<pre>";print_r($badge['username']);
                 $options[$i]['value']=$user_id;
                 $options[$i]['label']=$username;
                 $i++;
             }
             //exit();
            $this->prepareData();
            foreach ($options as $optionCode) {
                $this->options[$optionCode['value']] = [
                    'type' => 'status_' . $optionCode['value'],
                    'label' => $optionCode['label'],
                ];

                if ($this->urlPath && $this->paramName) {
                    $this->options[$optionCode['value']]['url'] = $this->urlBuilder->getUrl(
                        $this->urlPath,
                        [$this->paramName => $optionCode['label']]
                    );
                }

                $this->options[$optionCode['value']] = array_merge_recursive(
                    $this->options[$optionCode['value']],
                    $this->additionalData
                );
            }

            // return the massaction data
            $this->options = array_values($this->options);
        }
        return $this->options;
    }

    /**
     * Prepare addition data for subactions
     *
     * @return void
     */
    protected function prepareData()
    {

        foreach ($this->data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->urlPath = $value;
                    break;
                case 'paramName':
                    $this->paramName = $value;
                    break;
                default:
                    $this->additionalData[$key] = $value;
                    break;
            }
        }
    }
}