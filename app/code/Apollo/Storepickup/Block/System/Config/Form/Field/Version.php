<?php
namespace Apollo\Storepickup\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * @category   Apollo
 * @package    Apollo_Storepickup
 * @author     apollo@gmail.com
 * @website    http://www.apollo.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Version extends \Magento\Config\Block\System\Config\Form\Field
{
	const EXTENSION_URL = 'http://www.apollo.com/extensions.html';

	/**
	 * @var \Apollo\Storepickup\Helper\Data $helper
	 */
	protected $_helper;

	/**
	 * @param \Magento\Backend\Block\Template\Context $context
	 * @param \Apollo\Storepickup\Helper\Data $helper
	 */
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Apollo\Storepickup\Helper\Data $helper
	) {
		$this->_helper = $helper;
		parent::__construct($context);
	}


	/**
	 * @param AbstractElement $element
	 * @return string
	 */
	protected function _getElementHtml(AbstractElement $element)
	{
		$extensionVersion   = $this->_helper->getExtensionVersion();
		$versionLabel       = sprintf('<a href="%s" title="Home Delivery" target="_blank">%s</a>', self::EXTENSION_URL, $extensionVersion);
		$element->setValue($versionLabel);

		return $element->getValue();
	}
}