<?php
namespace Apollo\Medicine\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context){
        $setup->startSetup();
        
        if (version_compare($context->getVersion(), '2.0.7', '<')) {

            // Get module table
            $tableName = $setup->getTable('ordermedicine_data');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                // Declare data
                $columns = [
                    'type' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_VARBINARY,
                        'nullable' => false,
                        'length' => 55,
                        'default' => "Prescription",
                        'comment' => 'Prescription/Order',
                    ],
                    'prescriptions' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'length' => 255,
                        'comment' => 'Prescriptions',
                    ],
                ];

                $connection = $setup->getConnection();

                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName, $name, $definition);
                }

            }
        }

        $setup->endSetup();
    }
}
