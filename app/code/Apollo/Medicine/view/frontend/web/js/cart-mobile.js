define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/modal'
], function ($, customerData, modal) {

    var options = {
                'type': 'popup',
                'title': 'Choose an option to upload your prescription',
                'modalClass': 'upload_prescription_popup', 
                'responsive': false,
                'innerScroll': false,
                'buttons': [/*{
                    text: $.mage.__('Back'),
                    class: 'back_button_class',
                    click: function () {
                        this.closeModal();
                        // any javascript coode
                    }
                }*/]
            };

  var upload_url  = $("#upload_url").val();
  var login_url  = $("#login_url").val();
  var next_url  = $("#next_url").val();
  //var store_pickup_url = $("#store-pickup-url").val();
  
  //var select_prescription_url  = $("#select_prescription_url").val();
  var choose_prescriptions_url  = $("#choose_prescriptions_url").val();


  var redirectIfNotLoggedIn = function(){
      var customer = customerData.get('customer');
      console.log(customer().fullname);
            if(customer().fullname && customer().firstname)
            {
                return true;
            }
           //window.location = login_url;
  }

  var choosePrescriptions = function(){
    console.log(choose_prescriptions_url);
    window.location = choose_prescriptions_url;
  }

  var fileSelected = function() {
 
        var count = document.getElementById('fileToUpload').files.length;
 
              document.getElementById('details').innerHTML = "";
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload').files[index];
 
                     var fileSize = 0;
 
                     if (file.size > 1024 * 1024)
 
                            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
 
                     else
 
                            fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
 
                     document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;
 
                     document.getElementById('details').innerHTML += '<p>';
 
              }
 
      }
 
      var uploadFile = function() {
 
       $(".loading").show();
        var fd = new FormData();
 
              var count = document.getElementById('fileToUpload').files.length;
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload').files[index];
 
                     fd.append('myFile', file);
 
              }

              var count = document.getElementById('fileToUpload1').files.length;
 
              for (var index = 0; index < count; index ++)
 
              {
 
                     var file = document.getElementById('fileToUpload1').files[index];
 
                     fd.append('myFile', file);
 
              }
 
        var xhr = new XMLHttpRequest();
 
        xhr.upload.addEventListener("progress", uploadProgress, false);
 
        xhr.addEventListener("load", uploadComplete, false);
 
        xhr.addEventListener("error", uploadFailed, false);
 
        xhr.addEventListener("abort", uploadCanceled, false);
 
        xhr.open("POST", upload_url);
 
        xhr.send(fd);
 
      }
 
      var  uploadProgress = function(evt) {
 
        if (evt.lengthComputable) {
 
          var percentComplete = Math.round(evt.loaded * 100 / evt.total);
 
          //document.getElementById('progress').innerHTML = percentComplete.toString() + '%';
 
        }
 
        else {
 
          document.getElementById('progress').innerHTML = 'unable to compute';
          $(".loading").hide();
 
        }
 
      }
 
      var uploadComplete = function(evt) {
 
        /* This event is raised when the server send back a response */
 
        $(".loading").hide();
        localStorage.presentPrescriptions = evt.target.responseText;
        console.log(localStorage.presentPrescriptions);
        window.location = next_url;
        //alert(evt.target.responseText);
 
      }
 
      var uploadFailed = function(evt) {
 
         $(".loading").hide();
        alert("There was an error attempting to upload the file.");
 
      }
 
      var uploadCanceled = function(evt) {
 
         $(".loading").hide();
        alert("The upload has been canceled by the user or the browser dropped the connection.");
 
      } 


	

    $(document).ready(function($) {

  		/*$("#storeDeliveryOption").on("click", function(){
  		   window.location = store_pickup_url;

  		});*/

      var data = {addressInformation : { address: {countryId: "IN", postcode:null, region:'', regionId:'0'}, shipping_carrier_code: "mphomedelivery", shipping_method_code:"mphomedelivery"}};
          //data["selectedPrescriptions"] = selectedPrescriptions;
          $.ajax({
                url: 'https://63.142.255.55/apollodev/rest/default/V1/carts/mine/totals-information',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                showLoader: true,
                dataType: 'json',
            }).done(function (response) {
              console.log(response);
              
              showLoader: false;
            }).fail(function () {
               console.log("Error");
               showLoader: false;
            });


      $(".uploadYourPrescriptionLink").on("click", function(){
          var popup = modal(options, $('#uploadYourPrescription')); 
          $("#uploadYourPrescription").modal("openModal");
        });

        $(".upload-with-camera").on("click", function(){
          redirectIfNotLoggedIn();
        $("#fileToUpload").click();
        

    });
    $(".upload-from-gallery").on("click", function(){
      redirectIfNotLoggedIn();
        $("#fileToUpload1").click(); 
        

    });

    $(".choose-from-uploaded").on("click", function(){
      choosePrescriptions();

    });

    $("#fileToUpload").on("change", function(){
       uploadFile();

    });

		
    });





});