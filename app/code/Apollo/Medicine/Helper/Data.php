<?php
namespace Apollo\Medicine\Helper;


use Magento\Framework\App\Helper\AbstractHelper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{	
    protected $_objectManager;

    protected $_storeManager;

    protected $scopeConfig;

	public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Store\Model\StoreManagerInterface $storeManager
	)
	{
        parent::__construct($context);
        $this->_objectManager = $objectmanager;
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $context->getScopeConfig();
	}

	public function getCodShippingAvailbility($pincode){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_cod_shipping');
        //Select Data from table
        $sql = "Select * FROM  $tableName where pincode = $pincode";
        $result = $connection->fetchRow($sql);
        $catalogSession = $this->_objectManager->create('Magento\Catalog\Model\Session');
        $CodShippingData =  $catalogSession->getCodShippingAvailablity();
        $CodShippingData[$pincode] = $result;
        $catalogSession->setCodShippingAvailablity($CodShippingData);
        $catalogSession->setCustomerPincode($pincode);
        return $result;
    }
	public function getCodShippingAvailbilitys($pincode){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('catalog_cod_shipping');
        //Select Data from table
        $sql = "Select * FROM  $tableName where pincode = $pincode";
        $result = $connection->fetchRow($sql);
        $catalogSession = $this->_objectManager->create('Magento\Catalog\Model\Session');
        $CodShippingData =  $catalogSession->getCodShippingAvailablity();
        $CodShippingData[$pincode] = $result;
        $catalogSession->setCodShippingAvailablity($CodShippingData);
        $catalogSession->setCustomerPincode($pincode);
        return $result;
    }

    public function getStoreLocations($pincode){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('plocations');
        //Select Data from table
        $sql = "Select * FROM  $tableName where pincode = $pincode";// limit 0,4
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getPrescriptions($email){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('ordermedicine_data');
        //Select Data from table
        $sql = "Select * FROM  $tableName where oemail = '$email' order by id desc";
        $result = $connection->fetchAll($sql);
        return $result;
    }

	public function getCityPincodes($term)
	{
        if(isset($_SESSION["city_val"]))
		{
			$city_val = $_SESSION["city_val"];
			return $city_val;
		}
    }

	//public function getCityPincodes2($popupcity,$term)
	public function getCityPincodes2($term)
	{
		//$popupcity = $_SESSION["city_val"];
		//$popupcity = "Hyderabad";
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('plocations');
        //Select Data from table
        $sql = "Select pincode,baddress,pcity,storename FROM  $tableName where baddress like '%$term%' or pincode like '$term%' order by baddress asc limit 10"; //pcity='".$popupcity."' and
        $result = $connection->fetchAll($sql);
        if(count($result) == 0){
            $tableName = $resource->getTableName('plocations_fedex');
            $sql = "Select concat(city_name,' ',pincode) storename,city_name as pcity,concat(city_name,' ',pincode) as baddress,pincode FROM  $tableName where pincode like '$term%'";// limit 0,4
            $result = $connection->fetchAll($sql);
        }
        return $result;
    }

    public function updatedDeliveryInfo($deliveryInfo,$objectManager){
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('quote');
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart');
        $quoteId = $cart->getQuote()->getId();
        $sql = "Update  $tableName Set delivery_info = '".$deliveryInfo."' where entity_id = $quoteId";
        $connection->query($sql);
        return true;
    }

    public function getOrderDeliveryInfo($orderId,$objectManager){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('sales_order_grid');
        //Select Data from table
        $sql = "Select delivery_info FROM  $tableName where entity_id = $orderId";
        $result = $connection->fetchRow($sql);
        $delivery_info = $result['delivery_info'];
        $delivery_info = unserialize($delivery_info);
        //echo "<pre>--";print_R($delivery_info);die;
        if(count($delivery_info) > 0){
            if($delivery_info['method'] == "store_pickup"){
                if(isset($delivery_info['address'])){
                    $locationstableName = $resource->getTableName('plocations');
                    //Select Data from table
                    $sql = "Select storename,baddress,pincode FROM  $locationstableName where slno = ".$delivery_info['address'];// limit 0,4
                    $locationresult = $connection->fetchRow($sql);
                    $delivery_info = array_merge($delivery_info,$locationresult);
                }
            }else if($delivery_info['method'] == "home_delivery"){
                $locationstableName = $resource->getTableName('ordermedicine_data');
                //Select Data from table
                //$prescriptions = implode(",",$delivery_info['method']);
                //$sql = "Select storename,baddress,pincode FROM  $locationstableName where slno = ".$delivery_info['address'];// limit 0,4
                //$locationresult = $connection->fetchRow($sql);
                //$delivery_info = array_merge($delivery_info,$locationresult);
            }
        }
        return $delivery_info;
    }

    public function getPrescriptionFilePath($id){
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('ordermedicine_data');
        //Select Data from table
        $sql = "Select uploaded_file FROM  $tableName where id = $id";
        $result = $connection->fetchRow($sql);
        if(isset($result['uploaded_file']) && !empty($result['uploaded_file'])){
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
            $file = array();
            $file['path'] = $baseUrl."pub/media/medicine_prescription/".$result['uploaded_file'];
            $file['file'] = $result['uploaded_file'];
            return $file;
        }
        return "";
    }

    public function getConfig($config_path)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
?>
