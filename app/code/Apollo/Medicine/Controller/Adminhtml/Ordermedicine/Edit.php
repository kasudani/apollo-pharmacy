<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Controller\Adminhtml\Ordermedicine;

use Apollo\Medicine\Controller\Adminhtml\Ordermedicine as OrdermedicineController;
use Magento\Framework\Controller\ResultFactory;

class Edit extends OrdermedicineController
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $_backendSession;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Apollo\Medicine\Model\OrdermedicineFactory
     */
    protected $_ordermedicine;

    /**
     * @param \Magento\Backend\App\Action\Context           $context
     * @param \Magento\Framework\Registry                   $registry
     * @param \Apollo\Medicine\Model\ordermedicineFactory $ordermedicine
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Apollo\Medicine\Model\OrdermedicineFactory $ordermedicine
    ) {
        $this->_backendSession = $context->getSession();
        $this->_registry = $registry;
        $this->_ordermedicine = $ordermedicine;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $ordermedicine = $this->_ordermedicine->create();
        if ($this->getRequest()->getParam('id')) {
            $ordermedicine->load($this->getRequest()->getParam('id'));
        }
        $data = $this->_backendSession->getFormData(true);
        if (!empty($data)) {
            $ordermedicine->setData($data);
        }
        $this->_registry->register('medicine', $ordermedicine);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Order Medicine Entries'));
        $resultPage->getConfig()->getTitle()->prepend(
            $ordermedicine->getId() ? $ordermedicine->getTitle() : __('New Entry')
        );
        $block = 'Apollo\Medicine\Block\Adminhtml\Ordermedicine\Edit';
        $content = $resultPage->getLayout()->createBlock($block);
        $resultPage->addContent($content);
        return $resultPage;
    }
}
