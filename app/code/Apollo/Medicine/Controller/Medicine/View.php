<?php
/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;


class View extends \Magento\Framework\App\Action\Action
{
    

    public function execute()
    {
         $this->_view->loadLayout();
	     $this->_view->getLayout()->getBlock('ordermedicine');
         $this->_view->renderLayout();
    }
}
