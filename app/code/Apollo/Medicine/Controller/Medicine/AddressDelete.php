<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;

class AddressDelete  extends Action
{
    protected $_addressRepository;

    public function __construct(
        Context $context,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository
    ) {
        $this->_addressRepository = $addressRepository;
        parent::__construct($context);
    }

    public function execute(){
        $addressId = $this->getRequest()->getPost("address_id");
        $response = array();
        if(isset($addressId)){
            try {
                $this->_addressRepository->deleteById($addressId);
                $response = array('success' => true, 'msg' => 'Customer address deleted successfully.');
            } catch(\Exception $e) {
                $response = array('success' => false, 'msg' => 'There is some error. Please try again.');
            }
        } else {
            $response = array('success' => false, 'msg' => 'Invalid request please try again.');
        }
        echo json_encode($response);
        die;
    }
}