<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Guest extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $spselectedPrescriptions = array();
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        $selectedPrescriptions = $checkoutSession->getSelectedPrescriptions();
        $hdselectedPrescriptions = $selectedPrescriptions;
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('ordermedicine_data');
        //Select Data from table
        $email = $_REQUEST['guest_mobile'];
        $sql = "Select * FROM  $tableName where oemail = '$email' order by id desc";
        $prescriptionsList = $connection->fetchAll($sql);
        $html = '<ul class="my_guest_data">';
        if(isset($prescriptionsList) && count($prescriptionsList) > 0){
            foreach ($prescriptionsList as $prescription){
            if(!empty($prescription['uploaded_file'])){
                if(!empty($hdselectedPrescriptions)){
                $checkedstate = in_array($prescription['id'],$hdselectedPrescriptions) ? 'checked="checked"' : '';
            }else{
                $checkedstate = "";
            }
                $html .= '<li><input type="checkbox" '. $checkedstate .' name="previous_prescriptions[]" value="'.$prescription['id'].'">'.$prescription['uploaded_file'].'</li>'; 
            }else{}
            }
            $html .= '</ul>';
        }
        echo $html;
    }
}