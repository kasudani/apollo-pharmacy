<?php
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;


class Setuserlocation extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');                
        $customer = $customerSession->getCustomer();
        $postcode = '1234';
        if ($customer) {
            $shippingAddress = $customer->getDefaultShippingAddress();
            if ($shippingAddress) {
                $postcode = $shippingAddress->getPostcode();
            }
        }

        if($postcode){

            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCityPincodes2($postcode);
           
            $i = 0;
         
            if(sizeof($result) > 0)
            {
                $response = array();
                foreach ($result as $storeinfo)
                {
                    
                    $response = array(
                        'id' => $storeinfo['pincode'],
                        'value' => $storeinfo['baddress'],
                        'city' => $storeinfo['pcity'],
                        'storename' => $storeinfo['storename'],
                    );
                    $i++;
                    break;
                }


                if(isset($response['value']))
                {
                    $pcasession_val = $response['value'];
                    $_SESSION["pcasession_val"] = $pcasession_val;
                }
                if(isset($response['city']))
                {
                    $popupcity_req_val = $response['city'];
                    $_SESSION["city_val"] = $popupcity_req_val;
                }
                if(isset($response['storename']))
                {
                    $selected_storename = $response['storename'];
                    $_SESSION["selected_storename"] = $selected_storename;
                }

                if(isset($response['id']))
                {
                    $pincodesession = $response['id'];
                    
                    $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');                
                    $checkoutSession->setPincodesession($pincodesession);
                    echo "selected"; exit;
                }

                echo "no-selection"; exit;
                
            }else{
                echo "no-store"; exit;
            }

        }else {
            echo "no-pin"; exit;
        }
    
        
	}
}
