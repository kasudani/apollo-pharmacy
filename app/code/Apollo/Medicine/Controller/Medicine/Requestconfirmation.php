<?php
namespace Apollo\Medicine\Controller\Medicine;
/**
 * ExtendTree
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL)
 * This is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 *
 * DISCLAIMER**
 *
 * @category   Magento2 Simple Modules
 * @license    http://opensource.org/licenses/OSL-3.0  Open Software License (OSL)
 * @Website    http://www.extendtree.com/
 */
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Requestconfirmation extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {

    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$customerSession = $objectManager->get('Magento\Customer\Model\Session');
    	$checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        if (!$customerSession->isLoggedIn()) {
        	$resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('sociallogin/mobile/login');
            return $resultRedirect;
        }

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomerId();
      //  print_r($customerSession->getData());
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $billingaddress = $customerObj->getDefaultBilling();
        $customerData = $customerObj->getData();
       // print_r($customerData);
        //exit;
        if(!empty($billingaddress)){
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address = $address->getData();
        }
        else
        {   
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address['city']="-";
            $address['region']="-";
            $address['street']="-";
            $address['postcode']="-";
            $address['telephone']="-";
        }

        $deliverytype = "Home";


        $sprs  = $checkoutSession->getSelectedPrescriptions();

        if(!$sprs){
        	$resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('ordermedicine/medicine/ordermedicinemobile');
            return $resultRedirect;
        }

       /* if($checkoutSession->getSelectedStore()){
        	$deliverytype = "Store";
        	$storetableName = $resource->getTableName('plocations');
            //Select Data from table
            $sql = "Select * FROM  $storetableName where storeid = ".$checkoutSession->getSelectedStore();
            $storeresult = $connection->fetchAll($sql);	
            //print_r($storeresult);
            $storeresult = $storeresult[0];

            $address['street'] = $storeresult['baddress'];
            $address['ocontact'] = $storeresult['baddress'];
            $address['street'] = $storeresult['baddress'];
            $address['street'] = $storeresult['baddress'];
        }
         

        $sql = "Update " . $themeTable . " SET `oaddress`='".$address['street']."', `ocontact`='".$address['telephone']."', `ostate`='".$address['region']."', `ocity`='".$address['city']."', `opincode`='".$address['postcode']."', `odeliverytype`='".$deliverytype."', `is_active` = 1 WHERE id=".$sprs[0];
                $connection->query($sql);
*/

    $this->_view->loadLayout();
    $this->_view->getLayout()->getBlock('requestconfirmation');
    $this->_view->renderLayout();   
    }

}