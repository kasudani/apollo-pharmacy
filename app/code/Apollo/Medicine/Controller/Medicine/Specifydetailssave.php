<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Specifydetailssave extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        //$my_guest = $_REQUEST['my_guest'];
        $my_guest = 'my_guest';
        //echo "<pre>";print_R($_REQUEST);print_R($_FILES);die;
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        //$pathurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'medicine_prescription/';
        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        $mediapath = $this->_mediaBaseDirectory = rtrim($mediaDir, '/');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomerId();
      //  print_r($customerSession->getData());
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $billingaddress = $customerObj->getDefaultBilling();
        $customerData = $customerObj->getData();

        if(!empty($billingaddress)){
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address = $address->getData();
        }
        else
        {   
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address['city']="-";
            $address['region']="-";
            $address['street']="-";
            $address['postcode']="-";
            $address['telephone']="-";
        }

        
        
         $deliveryType = 'Home';
        if(isset($_REQUEST['deliveryType'])){
            $deliveryType = $_REQUEST['deliveryType'];

            if($deliveryType=='Home'){
                if(isset($_REQUEST['addressId']) && !empty($_REQUEST['addressId'])){
                    $addressId = $_REQUEST['addressId'];
                    $address = $objectManager->create('Magento\Customer\Model\Address')->load($addressId);
                    $address = $address->getData();
                }
            }else if($deliveryType=='Store'){
                if(isset($_REQUEST['storeId']) && !empty($_REQUEST['storeId'])){
                    $storeId = $_REQUEST['storeId'];
                    $storeData = $objectManager->create('Apollo\Medicine\Block\Specifydetails')->getStoreDetailsAccordingId($storeId);
                    foreach ($storeData as $value) {
                        $address['city']=$value['storename'];
                        $address['street']=$value['baddress'];
                        $address['postcode']=$value['pincode'];
                        
                    }
                }
            }
        }
       
       $selected_prescription = $checkoutSession->getSelectedPrescriptions();
       if(count($selected_prescription)){


        if(count($selected_prescription)==1)
            $sql = "SELECT * FROM  " . $themeTable . " WHERE ID='".$selected_prescription[0]."'";
        elseif (count($selected_prescription)>1) {
            $sql = "SELECT * FROM " . $themeTable . " WHERE ID IN (".implode(',', $selected_prescription).")";
        }
        $prescriptions_files = [];
        $prescriptions =  $connection->query($sql);
        if($prescriptions && $prescriptions->rowCount()){
              
                while ($row = $prescriptions->fetch()) {
                    $prescriptions_files[] = $row["uploaded_file"];
                }
            }


            if(empty($customerData['firstname'])){
                    $customerData['firstname']=$my_guest;
                }
                if(empty($customerData['email'])){
                    $customerData['email']=$my_guest;
                }

            $sql = "INSERT INTO " . $themeTable . "(oname,oemail,oaddress,ocontact,ostate,ocity,opincode,odeliverytype,created_at,uploaded_file,oorderstatus,ototalqty,oamount,type,prescriptions,is_active) VALUES 
                            ('".$customerData['firstname']."','".$customerData['email']."','".$address['street']."','".$address['telephone']."','".$address['region']."','".$address['city']."','".$address['postcode']."',
                            '".$deliveryType."','".$currenttime."','".implode(',', $prescriptions_files)."','Order Prescription','0','0','Order','".implode(',', $selected_prescription)."','1')";
                $connection->query($sql);
               $order_id = $connection->lastInsertId();

            /*if(count($selected_prescription)==1)
            $sql = "Update " . $themeTable . " SET `ocomments` = '".$_REQUEST['comment']."' WHERE ID='".$selected_prescription[0]."'";
            elseif (count($selected_prescription)>1) {
                $sql = "Update " . $themeTable . " SET `ocomments` = '".$_REQUEST['comment']."' WHERE ID IN (".implode(',', $selected_prescription).")";
            }
                $connection->query($sql);*/
            $_SESSION['POrder_id']  = $order_id;
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultRedirect->setData( array('order_id'=>$order_id));
            return $resultRedirect;
        }else{
             
             $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);
             $resultRedirect->setData(array());
             return $resultRedirect;
        }
        
           
        
    }
}