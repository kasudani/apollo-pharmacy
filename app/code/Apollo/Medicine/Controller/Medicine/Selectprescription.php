<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Apollo\Medicine\Controller\Medicine;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Selectprescription extends \Magento\Framework\App\Action\Action {

    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\ObjectManagerInterface $objectManager, StoreManagerInterface $storeManager, \Magento\Framework\Filesystem $filesystem, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory) {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute() {
        
        $post = $this->getRequest()->getPostValue();

        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');

        $selectedPrescriptions = $checkoutSession->getSelectedPrescriptions();
        if($post["selectedPrescriptions"]){
            foreach ($post["selectedPrescriptions"] as $key => $value) {
                if(!$selectedPrescriptions || !in_array($value, $selectedPrescriptions)){
                    $selectedPrescriptions[] = $value;
                }
            }
        }

        $checkoutSession->setSelectedPrescriptions($selectedPrescriptions);

        
         $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultRedirect->setData(array("status"=>true, "data"=>$selectedPrescriptions));
        return $resultRedirect;

    }
    
          
       public function getExtension($str)
{
 
         $i = strrpos($str,".");
         if (!$i) { return ""; }
 
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}

}
