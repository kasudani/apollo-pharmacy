<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Deleteprescription extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $_REQUEST['id'];
        $my_guest = 'my_guest';
        //echo "<pre>";print_R($_REQUEST);print_R($_FILES);die;
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');

        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');

        //$sql = "DELETE FROM " . $themeTable . " WHERE id='".."'";
          //      $connection->query($sql);
        try{

            $sql = "DELETE FROM " . $themeTable . " WHERE id='".$id."'";
            $connection->query($sql);
        }
        catch(Exception $e) {
            
        }

        $selectedPrescriptions = $checkoutSession->getSelectedPrescriptions();
        if($selectedPrescriptions){
            foreach ($selectedPrescriptions as $key => $value) {
                if($value == $id){
                    unset($selectedPrescriptions[$key]);
                }
                
            }
        }

        $checkoutSession->setSelectedPrescriptions($selectedPrescriptions);

        
         $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
             
        


    }
}