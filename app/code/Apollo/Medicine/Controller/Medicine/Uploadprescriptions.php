<?php

/**
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultFactory;

class Uploadprescriptions extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Contact Us page
     *
     * @return void
     */
    protected $_objectManager;
    protected $_storeManager;
    protected $_filesystem;
    protected $_fileUploaderFactory;
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,\Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->_objectManager = $objectManager;
        $this->_storeManager = $storeManager;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $my_guest = $_REQUEST['my_guest'];
        //echo "<pre>";print_R($_REQUEST);print_R($_FILES);die;
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
        //$pathurl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'medicine_prescription/';
        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        $mediapath = $this->_mediaBaseDirectory = rtrim($mediaDir, '/');

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomerId();
      //  print_r($customerSession->getData());
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $billingaddress = $customerObj->getDefaultBilling();
        $customerData = $customerObj->getData();
       // print_r($customerData);
        //exit;
        if(!empty($billingaddress)){
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address = $address->getData();
        }
        else
        {   
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingaddress);
            $address['city']="-";
            $address['region']="-";
            $address['street']="-";
            $address['postcode']="-";
            $address['telephone']="-";
        }
        //echo "<pre>";print_R($_FILES);die;
        //echo "<pre>";print_R($customerData);print_R($address);echo "</pre>";
        if(isset($_REQUEST['home_delivery_pincode'])){
            $selectedPincode = $_REQUEST['home_delivery_pincode'];
        }
        $selectedPincode="123456";
        if(isset($_REQUEST['store_pickup_pincode'])){
            if(!isset($_REQUEST['store_selection'])){
                $this->messageManager->addError(__('Please choose store'));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            }
            $selectedPincode = $_REQUEST['store_pickup_pincode'];
            $checkoutSession->setSelectedStore($_REQUEST['store_selection']);
        }
        if(!empty($selectedPincode)){
            $result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCityPincodes2($selectedPincode);
            if(isset($result[0])){
                $result = $result[0];
                //echo "<pre>";print_R($result);die;
                $_SESSION["pcasession_val"] = $result['baddress'];
                $_SESSION["city_val"] = $result['pcity'];
                $_SESSION["selected_storename"] = $result['storename'];
                $checkoutSession->setPincodesession($selectedPincode);
            }
        }


        foreach ($_FILES as $key => $uploadedfile){
            if(!empty($key) && !empty($uploadedfile['name'])){
                $uploader = $this->_fileUploaderFactory->create(['fileId' => $key]);
                //$uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $path = $mediapath . '/medicine_prescription/';
                $result = $uploader->save($path);
                if(empty($customerData['firstname'])){
                    $customerData['firstname']=$my_guest;
                }
                if(empty($customerData['email'])){
                    $customerData['email']=$my_guest;
                }
                $sql = "INSERT INTO " . $themeTable . "(oname,oemail,oaddress,ocontact,ostate,ocity,opincode,odeliverytype,created_at,uploaded_file,oorderstatus,ototalqty,oamount,is_active) VALUES 
                            ('".$customerData['firstname']."','".$customerData['email']."','".$address['street']."','".$address['telephone']."','".$address['region']."','".$address['city']."','".$address['postcode']."',
                            'Home','".$currenttime."','".$result['file']."','Order Prescription','0','0','0')";
                $connection->query($sql);
                $_REQUEST['previous_prescriptions'][] = $connection->lastInsertId();
            }
        }
        if(isset($_REQUEST['previous_prescriptions']))
            $checkoutSession->setSelectedPrescriptions($_REQUEST['previous_prescriptions']);


        //$checkoutSession->setSelectedPincode($selectedPincode);
        $checkoutSession->setDeliveryMethod($_REQUEST['delivery_method']);


        $deliveryInfo = array();
        $deliveryInfo['method'] = $_REQUEST['delivery_method'];
        if(isset($_REQUEST['previous_prescriptions'])){
            $deliveryInfo['previous_prescriptions'] = $_REQUEST['previous_prescriptions'];
        }
        if(isset($_REQUEST['store_selection'])){
            $deliveryInfo['address'] = $_REQUEST['store_selection'];
        }
        $deliveryInfo = serialize($deliveryInfo);
        $checkoutSession->setDeliveryInfo($deliveryInfo);
        $objectManager->create('Apollo\Medicine\Helper\Data')->updatedDeliveryInfo($deliveryInfo,$objectManager);



        if(isset($selectedPincode)){
             if(isset($_REQUEST['previous_prescriptions']))
                $this->messageManager->addSuccess("Prescriptions Uploaded successfully");
            
            // Removing session data

            if(isset($_SESSION["pincheck"])){

                unset($_SESSION["pincheck"]);

            }
            $_SESSION["pincheck"]="325325";
            
             $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
             $resultRedirect->setUrl($this->_redirect->getRefererUrl());
             return $resultRedirect;
        }else{
            echo "Invalid Request";die;
        }

    }
}