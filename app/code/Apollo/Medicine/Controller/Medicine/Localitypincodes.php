<?php
namespace Apollo\Medicine\Controller\Medicine;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Localitypincodes extends \Magento\Framework\App\Action\Action
{
	
	public function execute()
    {
	
        if(isset($_REQUEST['term']))
		{
            $term = $_REQUEST['term'];

			if(isset($_REQUEST['term']))
			{
		        $term_val = $_REQUEST['term'];
				$_SESSION["term_val"] = $term_val;
			}

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');
            if(isset($_REQUEST['type']) && ($_REQUEST['type'] == "store_selection"))
			{
                $checkoutSession->setSelectedStore($_REQUEST['store_id']);
                exit;
            }
            
            
			$result = $objectManager->create('Apollo\Medicine\Helper\Data')->getCityPincodes2($term);
           
            $i = 0;
			$htmlREsponse = '';
            if(sizeof($result) > 0)
			{
				$response = array();
                foreach ($result as $storeinfo)
				{
                    
					$response[] = array(
						'id' => $storeinfo['pincode'],
						'value' => $storeinfo['baddress'],
                        'city' => $storeinfo['pcity'],
                        'storename' => $storeinfo['storename'],
					);
                    $i++;
                }
				$htmlREsponse = json_encode($response);
            }
			
            echo $htmlREsponse;
			die;
        }
		else
		{
            echo "Invalid Request";
			die;
        }

    }

}
