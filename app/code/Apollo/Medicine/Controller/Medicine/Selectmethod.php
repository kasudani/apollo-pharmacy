<?php
namespace Apollo\Medicine\Controller\Medicine;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use \Magento\Catalog\Model\Session;

class Selectmethod extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
		if(isset($_REQUEST['deliveryOption']))
		{
            $storetype = $_REQUEST['deliveryOption'];
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');

            
            $deliveryInfo = array();
        
            if($storetype == 'storeDelivery'){
            $checkoutSession->setDeliveryMethod("mpstorepickup_mpstorepickup");
            $deliveryInfo['method'] = "mpstorepickup_mpstorepickup";
            $deliveryInfo['address'] = (isset($_REQUEST['store'])) ? $_REQUEST['store'] : '';
            }
            else{
            $checkoutSession->setDeliveryMethod("mphomedelivery_mphomedelivery");
            $deliveryInfo['method'] = "mphomedelivery_mphomedelivery";
            $deliveryInfo['address'] = (isset($_REQUEST['address'])) ? $_REQUEST['address'] : '';
            } 
            $deliveryInfo = serialize($deliveryInfo);
            $checkoutSession->setDeliveryInfo($deliveryInfo);
            $this->_objectManager->create('Apollo\Medicine\Helper\Data')->updatedDeliveryInfo($deliveryInfo,$this->_objectManager);
            //$checkoutSession->setSelectedPincode($_REQUEST['store_pickup_pincode']);
            //$checkoutSession->setSelectedPrescriptions(array());
             echo "Done";die;
           	exit;
        }else{
            echo "Invalid Request";die;
        }
	}
}
