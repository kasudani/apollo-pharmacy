<?php
/**
 * Apollo Pharmacy.
 * @category Pharmacy
 * @package Apollo_Medicine
 * @author Srinivas
 * @copyright Copyright (c) 2008-2017 Webkul Apollo Hospitals Limited 
 */
?>
<?php
    \Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Apollo_Medicine',
        __DIR__
    );
