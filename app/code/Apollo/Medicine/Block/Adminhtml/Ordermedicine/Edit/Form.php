<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block\Adminhtml\Ordermedicine\Edit;

/**
 * Adminhtml Medicine Ordermedicine Edit Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('ordermedicine_form');
        $this->setTitle(__('Order Medicine Entry Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('medicine');
        $form = $this->_formFactory->create(
            ['data' => [
                        'id' => 'edit_form',
                        'enctype' => 'multipart/form-data',
                        'action' => $this->getData('action'),
                        'method' => 'post']
                    ]
        );
        $form->setHtmlIdPrefix('ordermedicine_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Order Medicine Entry Information'), 'class' => 'fieldset-wide']
        );
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }
        $fieldset->addField(
            'oname',
            'text',
            ['name' => 'oname', 'label' => __('Name'), 'title' => __('Name'), 'required' => true]
        );
        $fieldset->addField(
            'oemail',
            'text',
            ['name' => 'oemail', 'label' => __('Email'), 'title' => __('Email'), 'required' => true]
        );

		 $fieldset->addField(
            'oaddress',
            'textarea',
            ['name' => 'oaddress', 'label' => __('Address'), 'title' => __('Address'), 'required' => false]
        );

        $fieldset->addField(
            'ocontact',
            'text',
            ['name' => 'ocontact', 'label' => __('Contact'), 'title' => __('Contact'), 'required' => true]
        );

        $fieldset->addField(
            'ostate',
            'text',
            ['name' => 'ostate', 'label' => __('State'), 'title' => __('State'), 'required' => false]
        );
        $fieldset->addField(
            'ocity',
            'text',
            ['name' => 'ocity', 'label' => __('City'), 'title' => __('City'), 'required' => true]
        );
        $fieldset->addField(
            'opincode',
            'text',
            ['name' => 'opincode', 'label' => __('Pincode'), 'title' => __('Pincode'), 'required' => true]
        );
		 $fieldset->addField(
            'uploaded_file',
            'file',
            ['name' => 'uploaded_file', 'label' => __('Upload Prescription'), 'title' => __('Upload Prescription')]
        );

	
		$url= $model->getData('uploaded_file');
        
        $imgageurl = explode(',', $url);
       
        end($imgageurl); // move the internal pointer to the end of the array
        $key_last = key($imgageurl);
        $lastkey= $imgageurl[$key_last]."/";
        if(!empty($imgageurl)){
            echo '<h3>View the Uploaded Prescription<h/3>';
        }

        foreach($imgageurl as $key => $value){
            //if($key!=$key_last){}
            if(!empty($value)){
                $medicineUrl = $this->getBaseUrl().'pub/media/medicine_prescription/'.$value;
                //echo "<div style='font-size:15px; font-weight:bold;'><a href='../../../../../../../../pub/media/medicine_prescription/".$value."' target='_new' >Prescription ".$key+1."</a></div>";
	            echo "<div style='font-size:15px; font-weight:bold;'><a href='".$medicineUrl."' target='_new' >Prescription ".($key+1)."</a></div>";
            }
         }
	   
        $fieldset->addField(
            'odeliverytype',
            'select',
            ['name' => 'odeliverytype', 'label' => __('Delivery Type'), 'title' => __('Delivery Type'), 'required' => false, 'values' =>array('-1'=>'Select', 'Home'=>'Home Delivery', 'Store'=>'Store Pickup')]
        );

		 $fieldset->addField(
            'oorderstatus',
            'select',
            ['name' => 'oorderstatus', 'label' => __('Order Status'), 'title' => __('Order Status'), 'required' => true, 'values' =>array('-1'=>'Select', 'Processing'=>'Processing', 'Pending'=>'Pending', 'Cancelled'=>'Cancelled', 'Completed'=>'Completed','Invalid_Prescription'=>'Invalid_Prescription','Delivered'=>'Delivered')]
        );
		

        $fieldset->addField(
            'oamount',
            'text',
            ['name' => 'oamount', 'label' => __('Amount'), 'title' => __('Amount'), 'required' => false]
        );

	 $fieldset->addField(
            'ocomments',
            'textarea',
            ['name' => 'ocomments', 'label' => __('Comments'), 'title' => __('Comments'), 'required' => false]
        );
	
	$fieldset->addField(
            'ocopouncode',
            'text',
            ['name' => 'ocopouncode', 'label' => __('Coupon Code'), 'title' => __('Coupon Code'), 'required' => false]
        );

       
        $form->setValues($model->getData());


        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();


    }
}
