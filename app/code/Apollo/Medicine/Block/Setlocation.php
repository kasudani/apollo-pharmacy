<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Setlocation extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    protected $_customerSession;

    protected $customerRepository;

    protected $formKey;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    ) {
        $this->_customerSession = $customerSession->create();
        $this->customerRepository = $customerRepository;
        $this->_countryFactory = $countryFactory;
        $this->formKey = $formKey;
        parent::__construct($context, $data);
    }

    public function getRefererUrl()
    {
         return $_SESSION['refered_url'];
    }
    public function getFormKey()
    {
         return $this->formKey->getFormKey();
    }

    public function getCustomerId() {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getId();
        }
        return false;
    }

    public function getCustomerAddresses($customerId){

        $customer = $this->customerRepository->getById($customerId);

        return $customer->getAddresses();

    }

    public function selectedStore(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $store = $checkoutSession->getSelectedStore();
        if($store){
            return $store;
        }else{
            return false;
        }

    }

    public function getStoreDetailsAccordingId($storeid){

        if(!empty($storeid)){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('plocations');
            //Select Data from table
            $sql = "Select * FROM  $tableName where storeid = $storeid";
            $result = $connection->fetchAll($sql);
            return $result;
        }
    }

    public function getCountryname($countryCode){    
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        echo $country->getName();
    }
    
}
