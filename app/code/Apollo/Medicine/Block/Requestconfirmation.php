<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Requestconfirmation extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function getPrescriptionId(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        $selectedPrescriptions = $checkoutSession->getSelectedPrescriptions();

        $checkoutSession->setSelectedPrescriptions([]);
        if(isset($_SESSION['POrder_id']) && !empty($_SESSION['POrder_id'])){

        $order_id = $_SESSION['POrder_id'];
        $_SESSION['POrder_id'] = '';
        return $order_id;
        } else{
            return 0;
        }
    
    }
}
