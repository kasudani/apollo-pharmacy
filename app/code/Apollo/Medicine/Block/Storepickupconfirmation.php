<?php
/**
 * Apollo Pharmacy.
 *
 * @category  Pharmacy
 * @package   Apollo_Medicine
 * @author    Srinivas
 * @copyright Copyright (c) 2016-2017 
 */
namespace Apollo\Medicine\Block;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\UrlInterface;

class Storepickupconfirmation extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */

    protected $_objectManager;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->_objectManager = $objectmanager;
        parent::__construct($context, $data);
    }

    public function getStoreDetailsAccordingId($storeid){

        if(!empty($storeid)){
            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('plocations');
            //Select Data from table
            $sql = "Select * FROM  $tableName where storeid = $storeid";
            $result = $connection->fetchAll($sql);
            return $result;
        }
    }
}
