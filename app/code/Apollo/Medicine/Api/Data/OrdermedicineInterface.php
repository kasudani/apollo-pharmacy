<?php
/**
 * Apollo Pharmacy.
 * @category Pharmacy
 * @package Apollo_Medicine
 * @author Srinivas
 * @copyright Copyright (c) 2008-2017 Webkul Apollo Hospitals Limited 
 */
namespace Apollo\Medicine\Api\Data;

interface OrdermedicineInterface
{
    /**
    * Constants for keys of data array. Identical to the name of the getter in snake case
    */
    const ENTITY_ID    = 'id';
    /***/

    /**
    * Get ID
    *
    * @return int|null
    */
    public function getId();

    /**
    * Set ID
    *
    * @param int $id
    *
    * @return \Apollo\Test\Api\Data\RecordInterface
    */
    public function setId($id);
}
