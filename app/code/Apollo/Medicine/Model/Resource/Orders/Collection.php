<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Apollo\Medicine\Model\Resource\Orders;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Apollo\Medicine\Model\Orders', 'Apollo\Medicine\Model\Resource\Orders');
        //$this->_map['fields']['page_id'] = 'main_table.page_id';
    }
 
    
}
