<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ZipCodeValidator
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ZipCodeValidator\Model;

use Webkul\ZipCodeValidator\Api\Data\ZipcodeInterface;

class Zipcode extends \Magento\Framework\Model\AbstractModel implements ZipcodeInterface
{
    /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * Test Record cache tag
     */
    const CACHE_TAG = 'zipcodevalidator_zipcode';

    /**
     * @var string
     */
    protected $_cacheTag = 'zipcodevalidator_zipcode';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'zipcodevalidator_zipcode';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webkul\ZipCodeValidator\Model\ResourceModel\Zipcode');
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteGallery();
        }
        return parent::load($id, $field);
    }
    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
