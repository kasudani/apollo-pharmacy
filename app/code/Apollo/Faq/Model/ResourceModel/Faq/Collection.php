<?php
namespace Apollo\Faq\Model\ResourceModel\Faq;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Apollo\Faq\Model\Faq','Apollo\Faq\Model\ResourceModel\Faq');
    }
}