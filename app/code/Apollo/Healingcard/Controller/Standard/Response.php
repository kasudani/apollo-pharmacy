<?php

namespace Apollo\Healingcard\Controller\Standard;

class Response extends \Apollo\Healingcard\Controller\Healingcard
{

    public function execute()
    {
		$comment = "";
        $request = $_POST;
		if(!empty($_POST)){
			foreach($_POST as $key => $val){
				if($key != "CHECKSUMHASH"){
					$comment .= $key  . "=" . $val . ", \n <br />";
				}
			}
		}
		$errorMsg = '';
		$successFlag = false;
		$resMessage = $_POST['RESPMSG'];
        $orderId = $this->getRequest()->getParam('ORDERID');
        $order = $this->getOrderById($orderId);
        $orderTotal = round($order->getGrandTotal(), 2);
        $orderStatus = $this->getRequest()->getParam('STATUS');
		$resCode = $this->getRequest()->getParam('RESPCODE');
        $orderTxnAmount = $this->getRequest()->getParam('TXNAMOUNT');
        //print_r($request);
        if($this->getHealingcardModel()->validateResponse($request, $orderId))
        {
			if($orderStatus == "TXN_SUCCESS" && $orderTotal == $orderTxnAmount){
			    //echo "<pre>";print_R($_REQUEST);die;
				// Create an array having all required parameters for status query.				
				$requestParamList = array("MID" => $_POST['MID'] , "ORDERID" => $orderId);
				$StatusCheckSum  =  $this->getHealingcardModel()->generateStatusChecksum($requestParamList);
				$requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
				
				// Call the PG's getTxnStatus() function for verifying the transaction status.
				$check_status_url = $this->getHealingcardModel()->getNewStatusQueryUrl(); 				
				$responseParamList = $this->getHealingcardHelper()->callNewAPI($check_status_url, $requestParamList);
				if(1==1)//$responseParamList['STATUS']=='TXN_SUCCESS' && $responseParamList['TXNAMOUNT']==$_POST['TXNAMOUNT']
				{
					$payment = $order->getPayment();
					$payment->registerCaptureNotification($responseParamList['TXNAMOUNT'], true);
                    $payment->setIsTransactionClosed(0);
                    $payment->save();

					$successFlag = true;
					$comment .=  "Success ";
					$order->setStatus($order::STATE_PROCESSING);
					$additionalInformation = $order->getPayment()->getAdditionalInformation();
                    $requstInformation = $this->getRequest()->getParams();
                    $additionalInformation = array_merge($additionalInformation,$requstInformation);
                    $order->getPayment()->setAdditionalInformation($additionalInformation);
					$order->setExtOrderId($orderId);
                    
                    

					$returnUrl = $this->getHealingcardHelper()->getUrl('checkout/onepage/success');
				}
				else{
					$errorMsg = 'Healingcard Transaction Failed ! Fraud has been detected';
					$comment .=  "Fraud Detucted";
					$order->setStatus($order::STATUS_FRAUD);
					$returnUrl = $this->getHealingcardHelper()->getUrl('checkout/onepage/failure');
				}
			}else{
                $order->setStatus($order::STATE_CANCELED);
				if($resCode == "141" || $resCode == "8102" || $resCode == "8103" || $resCode == "14112"){
					$errorMsg = 'Healingcard Transaction Failed ! Healingcard was cancelled.';
					$comment .=  "Payment cancelled by user";
                    $this->_cancelPayment("Payment cancelled by user");
					//$order->save();
					$returnUrl = $this->getHealingcardHelper()->getUrl('checkout/cart');
				}else{
					$errorMsg = 'Healingcard Transaction Failed ! '.$resMessage;
					$comment .=  "Failed";
					
					$order->setStatus($order::STATE_PAYMENT_REVIEW);
					$returnUrl = $this->getHealingcardHelper()->getUrl('checkout/onepage/failure');
				}
			}            
        }
        else
        {
			$errorMsg = 'Healingcard Transaction Failed ! Fraud has been detected';
			$comment .=  "Fraud Detucted";
            $order->setStatus($order::STATUS_FRAUD);
            $returnUrl = $this->getHealingcardHelper()->getUrl('checkout/onepage/failure');
        }
        $status = $order->getStatus();
        //echo $status;
        //die("final");
		$this->addOrderHistory($order,$comment,$status);
        //echo $order->getStatus();
        $order->save();

        //die("final");
		if($successFlag){
			$this->messageManager->addSuccess( __('Healingcard transaction has been successful.') );
		}else{
			$this->messageManager->addError( __($errorMsg) );
		}
        $this->getResponse()->setRedirect($returnUrl);
    }

}
