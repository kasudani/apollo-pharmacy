<?php

namespace Apollo\Healingcard\Controller\Standard;

class Redirect extends \Apollo\Healingcard\Controller\Healingcard
{
    public function execute()
    {
        $order = $this->getOrder();
        $orderId = $order->getId();

		 //  code start

		$orderTotal = round($order->getGrandTotal(), 2);
	
		$address =$order->getBillingAddress();

		// store --> uat = 14079 live =15636

		$ref=date('Ymd').$orderId;

		$params=json_encode(array("reference"=>$ref,"amt"=>$orderTotal,"terminalid"=>"001","Mnum"=>$address->getTelephone(),"store"=>"11265","cardno"=>$_COOKIE['mobile'],"pin"=>$_COOKIE['pinn'],"invamt"=>$orderTotal,"salesid"=>"online"));

	
		$scopeConfig = $this->_objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
		
		$url =  $scopeConfig->getValue(
			'payment/healingcard/transaction_url',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
		);
		
		//$url='http://10.4.14.4:85/ValuHealing.svc/VAREDEEMACTION';
		$url='http://172.31.29.107:80/ValuHealing.svc/VAREDEEMACTION';

		
        	$urlparam = "";
		$urlparam=array("reqdata"=>base64_encode($params));

	
		$curl=curl_init($url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($urlparam));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($urlparam))));

		$tokenn = curl_exec($curl);

		//print_r($tokenn);
		
		//	exit;

		$res=json_decode($tokenn, TRUE);

		$orderStatus=$res['ValuredeemResult']['responsecode'];
		//$orderStatus=0;		
		
		
			if($orderStatus == 0){
						$successFlag = true;
						$comment =  'Healingcard Transaction Success '.$res['ValuredeemResult']['message']."|".$res['ValuredeemResult']['balanceamount']."|".$res['ValuredeemResult']['cardstatus']."|".$res['ValuredeemResult']['rrno'];
						//$order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETED);

						$payment = $order->getPayment();
						$payment->registerCaptureNotification($orderTotal, true);
						$payment->setIsTransactionClosed(0);
						$payment->save();

						
						$newState = \Magento\Sales\Model\Order::STATE_PROCESSING;
						$order->setState($newState)->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
						$order->save();


						$order->setExtOrderId($orderId);


						$returnUrl = $this->_healingcardHelper->getUrl('checkout/onepage/success');
						unset($_COOKIE['mobile']);
						unset($_COOKIE['pinn']);
						
			}else{
						$successFlag = false;
						$errorMsg = 'Healingcard Transaction Failed ! '.$res['ValuredeemResult']['message'];
						$comment =  "Healingcard  Transaction Failed";
						$order->setStatus($order::STATE_CANCELED);
						//$order->save();
						$returnUrl = $returnUrl = $this->_healingcardHelper->getUrl('checkout/onepage/success');
						unset($_COOKIE['mobile']);
						unset($_COOKIE['pinn']);
			}
			
			$this->addOrderHistory($order,$comment);
				  $order->save();
			
			if(isset($successFlag)){
					if($successFlag){
					$this->messageManager->addSuccess(__('Healingcard transaction has been successful.'));
				}else{
					$this->messageManager->addError(__($errorMsg));
				}
			}
			
			$this->getResponse()->setRedirect($returnUrl);
			 
		


		 // code ends

		/*
        $this->updateOrderStatus($order,"pending_payment");
        $order->setStatus($order::STATE_PENDING_PAYMENT);
        if ($order->getBillingAddress())
        {
            $this->getResponse()->setRedirect(
                $this->getHealingcardModel()->buildHealingcardRequest($order)
            );
        }
        else
        {
            $this->_cancelPayment();
            $this->_healingcardSession->restoreQuote();
            $this->getResponse()->setRedirect(
                $this->getHealingcardHelper()->getUrl('checkout')
            );
        } */
    }
    public function updateOrderStatus($order,$orderStatus){
        $orderId = $order->getId();
        $order->setStatus($orderStatus);
        $order->save();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $salesTable = $resource->getTableName('sales_order_status_history');
        $sql = "Update  $salesTable Set status = '".$orderStatus."' where parent_id = $orderId";
        $connection->query($sql);
        return true;
    }
}