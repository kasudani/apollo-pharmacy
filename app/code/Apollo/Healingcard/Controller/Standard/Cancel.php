<?php

namespace Apollo\Healingcard\Controller\Standard;

class Cancel extends \Apollo\Healingcard\Controller\Healingcard
{

    public function execute()
    {
        $this->_cancelPayment();
        $this->_checkoutSession->restoreQuote();
        $this->getResponse()->setRedirect(
            $this->getHealingcardHelper()->getUrl('checkout')
        );
    }

}
