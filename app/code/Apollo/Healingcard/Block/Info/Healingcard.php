<?php

namespace Apollo\Healingcard\Block\Info;

class Healingcard extends \Magento\Payment\Block\Info
{
    protected $_template = 'Apollo_Healingcard::info/healingcard.phtml';
}
