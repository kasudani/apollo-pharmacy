<?php

namespace Apollo\Contactus\Model\ResourceModel\Contact;

use \Apollo\Contactus\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'contact_id';

    protected $_previewFlag;

    protected function _construct()
    {
        $this->_init('Apollo\Contactus\Model\Contact', 'Apollo\Contactus\Model\ResourceModel\Contact');

        $this->_map['fields']['contact_id'] ='main_table.contact_id';
    }
}
