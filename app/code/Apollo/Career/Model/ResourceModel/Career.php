<?php
namespace Apollo\Career\Model\ResourceModel;
class Career extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('apollo_careers','id');
    }
}