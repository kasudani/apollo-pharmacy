<?php
namespace Apollo\Career\Block;

use Magento\Store\Model\ScopeInterface;

/**
 * Main career list block
 */
class Listcareer extends \Magento\Framework\View\Element\Template
{
	protected $careerFactory;

    const XML_PATH_ITEMS_PER_PAGE = 'apollo_career/apollo_career/items';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

	public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \Apollo\Career\Model\Career $careerFactory,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    array $data = []
	) { 
	    $this->careerFactory = $careerFactory;

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
	}

	public function getCareers(){
		return $this->careerFactory->getCollection();
	}


   /**
     * Set news collection
     */
    protected  function _construct()
    {
        parent::_construct();
        $collection = $this->careerFactory->getCollection()
            ->addFilter('is_active', 1)
            ->setOrder('order_career', 'ASC');
        $this->setCollection($collection);
    }
 
   /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        /** @var \Magento\Theme\Block\Html\Pager */
        $pager = $this->getLayout()->createBlock(
           'Magento\Theme\Block\Html\Pager',
           'apollo.career.list.pager'
        );
        $pager->setLimit($this->scopeConfig->getValue(self::XML_PATH_ITEMS_PER_PAGE, ScopeInterface::SCOPE_STORE))
            ->setShowAmounts(false)
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
 
        return $this;
    }
 
   /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * get file url by file name
     * @param $file
     * @return string
     */
    public function getFileUrl($file)
    {
        return $this->getMediaBaseUrl(). 'career/documents/' . $file;
    }

    /**
     * get media url
     * @return mixed
     */
    public function getMediaBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

}
