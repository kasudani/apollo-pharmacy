<?php
namespace Apollo\Career\Block\Adminhtml;

class Region extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Apollo_Career';
        $this->_controller = 'adminhtml_region';
        $this->_headerText = __('Region List');
        $this->_addButtonLabel = __('Add New Region');
        parent::_construct();
    }
}