<?php
namespace Apollo\BannerImage\Model\Category;
  
class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
  
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'banner_image'; // custom image field
         
        return $fields;
    }
}