<?php
/*
 * Quadone_Promotions

 * @category   Quadone
 * @package    Quadone_Promotions
 * @copyright  Copyright (c) 2017 Quadone
 * @license    https://github.com/quadone/magento2-sample-imageuploader/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace Quadone\Promotions\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('quadone_promotions_image');

        if (!$installer->tableExists('quadone_promotions_image')) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'image_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Image ID'
                )
				 ->addColumn(
                    'url',
                    Table::TYPE_TEXT,
                    null,
                    [
                        
                        'nullable' => false,
                    ],
                    'URL'
                )
				 ->addColumn(
                    'link',
                    Table::TYPE_TEXT,
                    null,
                    [
                        
                        'nullable' => false,
                    ],
                    'Link'
                )
                ->addColumn(
                    'image',
                    Table::TYPE_TEXT,
                    255,
                    array(
                        'nullable'  => false,
                    ),
                    'Image'
                )
				->addColumn(
                    'actived',
                    Table::TYPE_TEXT,
                    null,
                    [
                        
                        'nullable' => false,
                    ],
                    'Active'
                );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}
