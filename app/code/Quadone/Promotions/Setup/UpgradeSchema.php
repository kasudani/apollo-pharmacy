<?php

namespace Quadone\Promotions\Setup;
 

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    
	
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){

					$setup->startSetup();

					if (version_compare($context->getVersion(), '1.0.4') < 0) {

						// Get module table

						$tableName = $setup->getTable('quadone_promotions_image');

						// Check if the table already exists

					if ($setup->getConnection()->isTableExists($tableName) == true) {

								// Declare data

								$columns = [

								'width' => [

								'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

								'nullable' => true,

								'comment' => 'Width',

								],
								'height' => [

								'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

								'nullable' => true,

								'comment' => 'Height',

								],

								];

							$connection = $setup->getConnection();

							foreach ($columns as $name => $definition) {

							$connection->addColumn($tableName, $name, $definition);

							}

					 
					}

					}

					 
					$setup->endSetup();

}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}