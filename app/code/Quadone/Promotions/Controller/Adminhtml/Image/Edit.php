<?php
/*
 * Quadone_Promotions

 * @category   Quadone
 * @package    Quadone_Promotions
 * @copyright  Copyright (c) 2017 Quadone
 * @license    https://github.com/quadone/magento2-sample-imageuploader/blob/master/LICENSE.md
 * @version    1.0.0
 */
namespace Quadone\Promotions\Controller\Adminhtml\Image;

use Quadone\Promotions\Controller\Adminhtml\Image;

class Edit extends Image
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $imageId = $this->getRequest()->getParam('image_id');
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Quadone_Promotions::image')
            ->addBreadcrumb(__('Promotions'), __('Promotions'))
            ->addBreadcrumb(__('Manage Promotions'), __('Manage Promotions'));

        if ($imageId === null) {
            $resultPage->addBreadcrumb(__('New Image'), __('New Promotion'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Promotion'));
        } else {
            $resultPage->addBreadcrumb(__('Edit Image'), __('Edit Promotion'));
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Promotion'));
        }
        return $resultPage;
    }
}
