<?php

/**
 * @Author: Ngo Quang Cuong
 * @Date:   2017-10-29 21:58:28
 * @Last Modified by:   https://www.facebook.com/giaphugroupcom
 * @Last Modified time: 2017-10-29 22:26:47
 */

/**
 * Newsletter subscribe block
 *
 * @author      GiaPhuGroup Ltd. <bestearnmoney87@gmail.com>
 */

namespace Quadone\Promotions\Block;

use Quadone\Promotions\Model\Image;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Element\Template;

class Subscribe extends \Magento\Framework\View\Element\Template
{
   private $_contact;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Quadone\Promotions\Model\Image $contact,
		\Magento\Store\Model\StoreManagerInterface $store,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_contact = $contact;
		$this->_store = $store;
        $this->_resource = $resource;

        parent::__construct(
            $context,
            $data
        );
    }

    public function getContacts()
    {
        $collection = $this->_contact->getCollection()->addFieldToFilter('actived',1);
		
        return $collection;
    }
	
	public function getStore()
    {
        $storemanager = $this->_store->getStore();
		
        return $storemanager;
    }
}
