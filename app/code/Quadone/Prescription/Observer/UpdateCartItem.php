<?php
namespace Quadone\Prescription\Observer;

use Magento\Framework\Exception\LocalizedException;

class UpdateCartItem implements \Magento\Framework\Event\ObserverInterface
{
    
    /**
     * @var \Magento\Quote\Model\Quote
     */
    private $productRepository; 

    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
       \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
    }

    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {       
        $items = $observer->getCart()->getQuote()->getItems();
        $info = $observer->getInfo()->getData();
        foreach ($items as $item) {
                $qty= $item->getQty();
                $sku= $item->getSku();
                $qty1= $info[$item->getId()]['qty'];
        }
        $product= $this->productRepository->get($sku);
        $ruleid= $product->getPerCustomer();
        if(isset($ruleid)){
             if($ruleid>0){
                if ($qty1>$ruleid) {
                    throw new LocalizedException(__('Customer Limit '.$ruleid.' items only'));
                }
             }
        }
    }
}
