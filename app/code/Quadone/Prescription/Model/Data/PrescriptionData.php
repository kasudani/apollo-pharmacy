<?php

namespace Quadone\Prescription\Model\Data;
 
use \Magento\Framework\Api\AttributeValueFactory;
 
/**
 * Class Custom Data
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class PrescriptionData extends \Magento\Framework\Api\AbstractExtensibleObject implements
\Quadone\Prescription\Api\Data\PrescriptionDataInterface
{
	
	/**
	 * Get Id.
	 *
	 * @return int|null
	 */
	public function getId()
	{
		return $this->_get(self::ID);
	}
	
	/**
	 * Set Id.
	 *
	 * @param int $id
	 * @return $this
	 */
	public function setId($id = null)
	{
		return $this->setData(self::ID, $id);
	}
	
	/**
	 * Get Customer Id.
	 *
	 * @return int|null
	 */
	public function getCustomerId()
	{
		return $this->_get(self::CUSTOMER_ID);
	}
	
	/**$testId
	 * Set Customer Id.
	 *
	 * @param int $customerId
	 * @return $this
	 */
	public function setCustomerId($customerId = null)
	{
		return $this->setData(self::CUSTOMER_ID, $customerId);
	}
	
	/**
	 * Get Product Id.
	 *
	 * @return int|null
	 */
	public function getProductId()
	{
		return $this->_get(self::PRODUCT_ID);
	}
	
	/**
	 * Set Product Id.
	 *
	 * @param int $productId
	 * @return $this
	 */
	public function setProductId($productId = null)
	{
		return $this->setData(self::PRODUCT_ID, $productId);
	}
    
    
    /**
	 * Get Customer Id.
	 *
	 * @return string|null
	 */
	public function getTest()
	{
		return  $this->_get(self::TEST);
	}
	
	/**
	 * Set Customer Id.
	 *
	 * @param string $test
	 * @return $this
	 */
	public function setTest($test= null)
	{
		return $this->setData(self::TEST, $test);
	}
	
	
	 /**
	 * Get Name.
	 *
	 * @return string|null
	 */
	public function getName()
	{
		return  $this->_get(self::NAME);
	}
	
	/**
	 * Set Name.
	 *
	 * @param string $name
	 * @return $this
	 */
	public function setName($name= null)
	{
		return $this->setData(self::NAME, $name);
	}
	
	/**
	 * Get Email.
	 *
	 * @return string|null
	 */
	public function getEmail()
	{
		return  $this->_get(self::EMAIL);
	}
	
	/**
	 * Set Email.
	 *
	 * @param string $email
	 * @return $this
	 */
	public function setEmail($email= null)
	{
		return $this->setData(self::EMAIL, $email);
	}
    
	/**
	 * Get Phone.
	 *
	 * @return string|null
	 */
	public function getPhone()
	{
		return  $this->_get(self::PHONE);
	}
	
	/**
	 * Set Phone.
	 *
	 * @param string $phone
	 * @return $this
	 */
	public function setPhone($phone= null)
	{
		return $this->setData(self::PHONE, $phone);
	}
 
	/**
	 * Get Image1.
	 *
	 * @return string|null
	 */
	public function getImage1()
	{
		return  $this->_get(self::IMAGE1);
	}
	
	/**
	 * Set Image1.
	 *
	 * @param string $image1
	 * @return $this
	 */
	public function setImage1($image1= null)
	{
		return $this->setData(self::IMAGE1, $image1);
	}
	
	/**
	 * Get Image2.
	 *
	 * @return string|null
	 */
	public function getImage2()
	{
		return  $this->_get(self::IMAGE2);
	}
	
	/**
	 * Set Image2.
	 *
	 * @param string $image2
	 * @return $this
	 */
	public function setImage2($image2= null)
	{
		return $this->setData(self::IMAGE2, $image2);
	}
	
	/**
	 * Get Image3.
	 *
	 * @return string|null
	 */
	public function getImage3()
	{
		return  $this->_get(self::IMAGE3);
	}
	
	/**
	 * Set Image3.
	 *
	 * @param string $image3
	 * @return $this
	 */
	public function setImage3($image3= null)
	{
		return $this->setData(self::IMAGE3, $image3);
	}
	
}