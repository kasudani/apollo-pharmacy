<?php

namespace Quadone\Prescription\Model\Checkout\Cart;

use Magento\Framework\Exception\LocalizedException;

class Plugin {

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;
    protected $customerSession;
    protected $orders;

    /**
     * Plugin constructor.
     *
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
    \Magento\Checkout\Model\Session $checkoutSession, \Magento\Customer\Model\Session $customerSession, \Magento\Sales\Model\ResourceModel\Order\Collection $orders
    ) {
        $this->quote = $checkoutSession->getQuote();
        $this->customerSession = $customerSession;
        $this->orders = $orders;
    }

    /**
     * beforeAddProduct
     *
     * @param      $subject
     * @param      $productInfo
     * @param null $requestInfo
     *
     * @return array
     * @throws LocalizedException
     */
    public function beforeAddProduct($subject, $productInfo, $requestInfo = null) {
        $customer_id = $this->customerSession->getCustomer()->getId();

        if (isset($customer_id)) {
            $orderCollection = $this->orders->addFieldToFilter('customer_id', $customer_id)->load();
            $qtys = 0;
            foreach ($orderCollection->getItems() as $order) {
                $items = $order->getAllItems();
                foreach ($items as $item) {
                    $sku = $item->getSku();
                    if ($productInfo->getSku() == $sku) {
                        $conditionsku = $sku;
                        $qtys += $item->getQtyOrdered();
                    }

                    $qty = (int) $item->getQtyOrdered();
                }
            };

            if (isset($qtys)) {
                if ($qtys != 0) {
                    $rule = $productInfo->getPerCustomer();
                    if ($qtys == $rule) {
                        throw new LocalizedException(__('You have already ordered ' . $qtys . ' items and allowed '.$rule.' items only'));
                    }

                    $qitems = $this->quote->getItems();
                    // $info = $observer->getInfo()->getData();
                    if (count($qitems)) {
                        foreach ($qitems as $qitem) {
                            $qqty = $qitem->getQty();
                            $qsku = $qitem->getSku();
                            if ($qsku == $conditionsku) {
                                $rule = $productInfo->getPerCustomer();
                                $qtys += $qqty;
                                //$ruleqty=$rule-$qtys; 
                                if ($rule == $qtys) {
                                    throw new LocalizedException(__('You have already ordered ' . $qtys . ' items and allowed '.$rule.' items only'));
                                }
                            }
                        }
                    }
                    return [$productInfo, $requestInfo];
                } else {
                    $items = $this->quote->getItems();
                    // $info = $observer->getInfo()->getData();
                    if (isset($items)) {
                        foreach ($items as $item) {
                            $qty = $item->getQty();
                        }
                    }
                    $ruleqty = $productInfo->getPerCustomer();

                    //print_r("dsg");print_r($productInfo->getPerCustomer());exit;
                    if (isset($qty)) {
                        if ($ruleqty == $qty) {
                            throw new LocalizedException(__('Customer Limit ' . $ruleqty . ' items only'));
                        }
                    }
                }
            }
        } else {
            $items = $this->quote->getItems();
            // $info = $observer->getInfo()->getData();
            if (isset($items)) {
                foreach ($items as $item) {
                    $qty = $item->getQty();
                }
            }
            $ruleqty = $productInfo->getPerCustomer();
            //print_r("dsg");print_r($productInfo->getPerCustomer());exit;
            if (isset($qty)) {
                if ($ruleqty == $qty) {
                    throw new LocalizedException(__('Customer Limit ' . $ruleqty . ' items only'));
                }
            }
        }

        return [$productInfo, $requestInfo];
    }

}
