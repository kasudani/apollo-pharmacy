<?php
namespace  Quadone\Prescription\Model;

 
use Quadone\Prescription\Api\ProductListInterface;
 
/**
* Defines the implementaiton class of the calculator service contract.
*/
class ProductList implements ProductListInterface
{
    protected $_objectManager = null;
      protected $_tokenkFactory;
 
      /**
      * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
    */
      protected $collectionFactory;
 
      /**
    * @var \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface
    */
    protected $extensionAttributesJoinProcessor;
 
    /**
    * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
    */
    protected $metadataService;
 
    /**
    * @var \Magento\Framework\Api\SearchCriteriaBuilder
    */
  protected $searchCriteriaBuilder;
    
     /**
    * @var \Magento\Framework\Api\SearchCriteriaBuilder
    */
  protected $pres;
 
  /**
    * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
    */
  protected $searchResultsFactory;
   
      public function __construct(
       \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
       \Magento\Catalog\Model\ResourceModel\Product $resourceModel,
       \Magento\Framework\ObjectManagerInterface $objectManager,
       \Magento\Store\Model\StoreManagerInterface $storeManager,
       \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor,
       \Magento\Catalog\Api\ProductAttributeRepositoryInterface $metadataServiceInterface,
       \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
       \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
       \Magento\CatalogInventory\Model\StockRegistry $stock,
           \Magento\Catalog\Model\ProductRepository $pres
    )
      {
          $this->_objectManager = $objectManager;
          $this->_storeManager = $storeManager;
          $this->collectionFactory = $collectionFactory;
          $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
          $this->metadataService = $metadataServiceInterface;
          $this->searchCriteriaBuilder = $searchCriteriaBuilder;
          $this->searchResultsFactory = $searchResultsFactory;
          $this->resourceModel = $resourceModel;
          $this->stockRegistryProvider = $stock;
          $this->pres = $pres;
      }
 
 
   public function getProductList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,$websiteIds)
  {
 
     $webSites = explode(',', $websiteIds);
 
      /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
      $collection = $this->collectionFactory->create();
      $this->extensionAttributesJoinProcessor->process($collection);
 
      foreach ($this->metadataService->getList($this->searchCriteriaBuilder->create())->getItems() as $metadata) {
          
          
          $collection->addAttributeToSelect($metadata->getAttributeCode());
      }
      //$collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
      //$collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
       
        
      $collection->addWebsiteFilter($webSites);
        $categoryFilter = [];
       $categoryFilter['eq'][] = 24;
     $collection->addCategoriesFilter($categoryFilter);
     echo "dsg";exit;
 
       
       
       $collection->setCurPage($searchCriteria->getCurrentPage());
      $collection->setPageSize($searchCriteria->getPageSize());
      $collection->load();
       
       
   
       print_r($collection->getData());exit;

      // filter for out of stock product
    $cond = array(
      '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=1 AND {{table}}.is_in_stock=1',
      '{{table}}.use_config_manage_stock = 0 AND {{table}}.manage_stock=0',
     );
 
 
     $cond[] = '{{table}}.use_config_manage_stock = 1 AND {{table}}.is_in_stock=1';
       
      $collection->joinField(
          'inventory_in_stock',
          'cataloginventory_stock_item',
          'is_in_stock',
          'product_id=entity_id',
          '(' . join(') OR (', $cond) . ')'
      );
 
      //Add filters from root filter group to the collection
      foreach ($searchCriteria->getFilterGroups() as $group) {
          $this->addFilterGroupToCollection($group, $collection);
      }
      /** @var SortOrder $sortOrder */
      foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
          $field = $sortOrder->getField();
          $collection->addOrder(
              $field,
              ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
          );
      }
      $collection->setCurPage($searchCriteria->getCurrentPage());
      $collection->setPageSize($searchCriteria->getPageSize());
      $collection->load();
 
      $searchResult = $this->searchResultsFactory->create();
      $searchResult->setSearchCriteria($searchCriteria);
      $searchResult->setItems($collection->getItems());
      $searchResult->setTotalCount($collection->getSize());
      return $searchResult;
  }
 
  /**
    * Helper function that adds a FilterGroup to the collection.
    *
    * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
    * @param Collection $collection
    * @return void
    */
  protected function addFilterGroupToCollection(
      \Magento\Framework\Api\Search\FilterGroup $filterGroup,
      $collection
  ) {
      $fields = [];
      $categoryFilter = [];
      foreach ($filterGroup->getFilters() as $filter) {
          $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
 
          if ($filter->getField() == 'category_id') {
              $categoryFilter[$conditionType][] = $filter->getValue();
              continue;
          }
          $fields[] = ['attribute' => $filter->getField(), $conditionType => $filter->getValue()];
      }
 
      
    
      
      if ($categoryFilter) {
          $collection->addCategoriesFilter($categoryFilter);
      }
 
      if ($fields) {
          $collection->addFieldToFilter($fields);
      }
  }
}