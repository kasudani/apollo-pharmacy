<?php
namespace Quadone\Prescription\Model;

use Quadone\Prescription\Api\PrescriptionRepositoryInterface;
use Quadone\Prescription\Api\Data\PrescriptionDataInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\CouldNotSaveException;

class PrescriptionRepository implements PrescriptionRepositoryInterface
{
	
	public function __construct(
			\Quadone\Prescription\Api\Data\PrescriptionDataInterfaceFactory $customFactory,
			\Magento\Framework\ObjectManagerInterface $objectManager,
			\Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
	) {
		$this->customFactory=$customFactory;
		$this->_objectManager = $objectManager;
		$this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
	}
	
	
		public function create(PrescriptionDataInterface $data){
		
		$rand = rand(10000, 990000);
        $sDirPath = 'pub/media/medicine_prescription/'.$rand.'/'; //Specified Pathname
         if (!file_exists ($sDirPath))
         { 
			mkdir($sDirPath,0777,true); 
		 }
		
		$image1 =$data->getImage1();
		$image2 =$data->getImage2();
		$image3 =$data->getImage3();
		if(!empty($image1)){
			$data1 = base64_decode($image1);
			$f1 = finfo_open();
			$mime_type1 = finfo_buffer($f1, $data1, FILEINFO_MIME_TYPE); 
			if($mime_type1=='application/pdf'){
				$type1= substr($mime_type1,12);
			}else{
				$type1= substr($mime_type1,6);
			}
			$i1=uniqid();
			$iname1 = $i1 . '.'.$type1;
			$file1 = $sDirPath. $i1 . '.'.$type1;
			$success1 = file_put_contents($file1, $data1);
			$result_file[]=$iname1;
		}
		
		if(!empty($image2)){
			$data2 = base64_decode($image2);
			$f2 = finfo_open();
			$mime_type2 = finfo_buffer($f2, $data2, FILEINFO_MIME_TYPE); 
			if($mime_type2=='application/pdf'){
				$type2= substr($mime_type2,12);
			}else{
				$type2= substr($mime_type2,6);
			}
			$i2=uniqid();
			$iname2 = $i2 . '.'.$type2;
			$file2 = $sDirPath. $i2 . '.'.$type2;
			$success2 = file_put_contents($file2, $data2);
			$result_file[]=$iname2;
		}
		
		if(!empty($image3)){
			$data3 = base64_decode($image3);
			$f3 = finfo_open();
			$mime_type3 = finfo_buffer($f3, $data3, FILEINFO_MIME_TYPE); 
			if($mime_type3=='application/pdf'){
				$type3= substr($mime_type3,12);
			}else{
				$type3= substr($mime_type3,6);
			}
			$i3=uniqid();
			$iname3 = $i3 . '.'.$type3;
			$file3 = $sDirPath. $i3 . '.'.$type3;
			$success3 = file_put_contents($file3, $data3);
			$result_file[]=$iname3;
		}
		
		chmod($sDirPath, 0777);
		//print $success ? $file : 'Unable to save the file.';
		$result_file[] = $rand;
		$save_files = implode(',', $result_file);
	
		$name =$data->getName();
		$email =$data->getEmail();
		$phone =$data->getPhone();
	
		$sql = "INSERT INTO ordermedicine_data (oname, oemail, ocontact, ostate, oaddress, ocity, ocomments, oprescriptionid, uploaded_file, opincode, odeliverytype, oorderstatus, is_active, created_by) VALUES ('$name', '$email', '$phone', ' ', ' ', ' ', ' ', ' ', '$save_files', ' ', ' ', ' ', 1, ' ')";

		
		$this->_resources = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\ResourceConnection');
        $connection = $this->_resources->getConnection();

        $themeTable = $this->_resources->getTableName('ordermedicine_data');
		//$connection->query($sql);
		//$result = mysqli_query($conn,$sql);
		  $connection->query($sql);
			$last_id = $connection->lastInsertId();
			
			return $last_id;
		
		/*
		$customerData=$this->_objectManager->create('Magento\Customer\Model\Customer')->load($data->getCustomerId());
		$productData=$this->_objectManager->create('Magento\Catalog\Model\Product')->load($data->getProductId());
		//Check if customer id exists
		if(!$customerData->getEntityId()){
			throw new InputException(__("Customer ID do not exist",$data->getCustomerId()));
		}
		//Check if product id exists
		elseif(!$productData->getEntityId()){
			throw new InputException(__("Product ID do not exist",$data->getProductId()));
		}
		$customDataArray = $this->extensibleDataObjectConverter
		->toNestedArray($data, [], 'CodeTheatres\CustomApi\Api\Data\CustomDataInterface');
		//Saving custom data in the table
		$customModel=$this->_objectManager->create('CodeTheatres\CustomApi\Model\Custom');
		$customModel->setData($customDataArray);
		$customModel->save();
		$customId=$customModel->getId();
		$data->setId($customId);
		return $data;*/
	}
	
}