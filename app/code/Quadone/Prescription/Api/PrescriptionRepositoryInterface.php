<?php

namespace Quadone\Prescription\Api;
 
interface PrescriptionRepositoryInterface
{
	/**
	 * Create custom Api.
	 *
	 * @param \CodeTheatres\CustomApi\Api\Data\CustomDataInterface $entity
	 * @return \CodeTheatres\CustomApi\Api\Data\CustomDataInterface
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function create(
			\Quadone\Prescription\Api\Data\PrescriptionDataInterface $entity
			);
	
	
}