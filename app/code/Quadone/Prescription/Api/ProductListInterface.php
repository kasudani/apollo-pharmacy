<?php
namespace Quadone\Prescription\Api;

    interface ProductListInterface
    {
        /**
        * Get product list
        *
        * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
        * @param string $websiteIds
        * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
        */
        public function getProductList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria,$websiteIds);
    }