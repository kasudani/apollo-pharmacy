<?php

namespace Quadone\ReviewEmail\Plugin\Magento\Review\Block\Adminhtml\Edit;

class Form extends \Magento\Review\Block\Adminhtml\Edit\Form
{
    public function beforeSetForm(\Magento\Review\Block\Adminhtml\Edit\Form $object, $form) {

        $review = $object->_coreRegistry->registry('review_data');

        $fieldset = $form->addFieldset(
            'review_details_extraa',
            ['legend' => __(''), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'email',
            'text',
            ['label' => __('E-mail'), 'required' => false, 'name' => 'email']
        );
        $fieldset->addField(
                    'mobile',
                    'text',
                    ['label' => __('Mobile'), 'required' => false, 'name' => 'mobile']
                );

        $form->setValues($review->getData());

        return [$form];
    }
}





