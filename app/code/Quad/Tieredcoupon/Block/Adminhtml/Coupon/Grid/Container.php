<?php
namespace Quad\Tieredcoupon\Block\Adminhtml\Coupon\Grid;

class Container extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_tieredcoupon';
        $this->_headerText = __('Combine Coupons');
        $this->_addButtonLabel = __('Add New Combine Coupon');
        parent::_construct();
    }
}
