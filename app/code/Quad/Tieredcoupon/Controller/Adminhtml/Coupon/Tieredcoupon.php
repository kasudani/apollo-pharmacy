<?php
namespace Quad\Tieredcoupon\Controller\Adminhtml\Coupon;

abstract class Tieredcoupon extends \Magento\Backend\App\Action
{
    /**
     * Initialize requested tiered coupon and put it into registry.
     *
     * @param bool $getRootInstead
     * @return \Quad\Tieredcoupon\Model\Tieredcoupon
     */
    protected function _initTieredcoupon()
    {
        $tieredcouponId = (int)$this->getRequest()->getParam('id', false);
        $tieredcoupon = $this->_objectManager->create(\Quad\Tieredcoupon\Model\Tieredcoupon::class);

        if ($tieredcouponId) {
            $tieredcoupon->load($tieredcouponId);
        }

        $this->_objectManager->get(\Magento\Framework\Registry::class)->register(\Quad\Tieredcoupon\Model\RegistryConstants::CURRENT_COUPON, $tieredcoupon);

        return $tieredcoupon;
    }
}
