<?php
namespace Quad\Tieredcoupon\Model\ResourceModel\Tieredcoupon;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Quad\Tieredcoupon\Model\Tieredcoupon', 'Quad\Tieredcoupon\Model\ResourceModel\Tieredcoupon');
    }

    public function joinSubCoupons(){
        $select = $this->getSelect();
        $select->joinInner(
            'quad_tieredcoupon_coupon'
        );
    }
}
