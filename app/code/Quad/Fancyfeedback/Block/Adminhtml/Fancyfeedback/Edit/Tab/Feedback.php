<?php
namespace Quad\Fancyfeedback\Block\Adminhtml\Fancyfeedback\Edit\Tab;
class Feedback extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Quad\Fancyfeedback\Model\Status
     */
    protected $_status;

    /**
     * @var \Quad\Fancyfeedback\Model\callback
     */
    //protected $_callback;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Quad\Fancyfeedback\Model\Status $status,
        //\Quad\Fancyfeedback\Model\Callback $callback,
        array $data = array()
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        //$this->_callback = $callback;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
		/* @var $model \Magento\Cms\Model\Page */
        $model = $this->_coreRegistry->registry('fancyfeedback_fancyfeedback');
		$isElementDisabled = false;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('fdb_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => __('Feedback')));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array('name' => 'id'));
            $isElementDisabled = true;
        }

		$fieldset->addField(
            'name',
            $model->getId() ? 'label' : 'text',
            array(
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            )
        );
		$fieldset->addField(
            'email',
            $model->getId() ? 'label' : 'text',
            array(
                'name' => 'email',
                'label' => __('Email'),
                'title' => __('Email'),
                'required' => true,
            )
        );
		$fieldset->addField(
            'phone',
            $model->getId() ? 'label' : 'text',
            array(
                'name' => 'phone',
                'label' => __('Phone'),
                'title' => __('Phone'),
                'disabled' => $isElementDisabled
            )
        );

		$fieldset->addField(
            'message',
            $model->getId() ? 'label' : 'textarea',
            array(
                'name' => 'message',
                'label' => __('Message'),
                'title' => __('Message'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('status'),
                'options' => $this->_status->getOptionArray()
            ]
        );
        if (!$model->getStatus()) {
            $model->setData('status', '0');
        }

		/*{{CedAddFormField}}*/
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();   
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Feedback');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Feedback');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
