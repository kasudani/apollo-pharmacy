<?php
namespace Quad\Fancyfeedback\Block\Adminhtml\Fancyfeedback\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
		
        parent::_construct();
        $this->setId('checkmodule_fancyfeedback_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Fancyfeedback Information'));
    }
}