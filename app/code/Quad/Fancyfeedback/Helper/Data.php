<?php
namespace Quad\Fancyfeedback\Helper;
 
/**
 * Custom Module Email helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'ws_fdb/feedback_group/ws_fdb_customer_email_template';
    /* Here section and group refer to name of section and group where you create this field in configuration*/
 
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
 
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
 
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;
 
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
     
    /**
     * @var string
    */
    protected $temp_id;
 
    /**
    * @param Magento\Framework\App\Helper\Context $context
    * @param Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    * @param Magento\Store\Model\StoreManagerInterface $storeManager
    * @param Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
    * @param Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->_scopeConfig = $context;
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder; 
    }
 
    /**
     * Return store configuration value of your template field that which id you set for template
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    public function getConfigValue($path, $storeId=null)
    {
        if (!$storeId) {
            $storeId = $this->getStore()->getStoreId();
        }
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
 
    /**
     * Return store 
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }
 
    /**
     * Return template id according to store
     *
     * @return mixed
     */
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }
 
    /**
     * [generateTemplate description]  with template file and tempaltes variables values                
     * @param  Mixed $emailTemplateVariables 
     * @param  Mixed $senderInfo             
     * @param  Mixed $receiverInfo           
     * @return void
     */
    public function generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo)
    {
        $template =  $this->_transportBuilder->setTemplateIdentifier($this->temp_id)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE, /* here you can defile area and store of template for which you prepare it */
                        'store' => $this->_storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'], $receiverInfo['name']);
        return $this;        
    }
 
    /**
     * [sendMailToCustomer description]                  
     * @param  \Quad\Fancyfeedback\Model\Feedback $feedback
     * @return void
     */
    public function sendMailToCustomer($feedback)
    {
        $receiverInfo = [
            'name' => $feedback['name'],
            'email' => $feedback['email'],
        ];
        $senderInfo = [
            'name' => $this->getConfigValue('ws_fdb/feedback_group/ws_fdb_sender_name', $this->getStore()->getStoreId()),
            'email' => $this->getConfigValue('ws_fdb/feedback_group/ws_fdb_sender_email', $this->getStore()->getStoreId()),
        ];

        if (!isset($feedback['admin_comments']) || empty($feedback['admin_comments'])) {
            return;
        }
        $body = __('Dear') . ' ' . $feedback['name'] . ',' . PHP_EOL;
        $body .= $feedback['admin_comments'] . PHP_EOL . PHP_EOL;
        $body .= $senderInfo['name'];

        $this->temp_id = $this->getTemplateId(self::XML_PATH_EMAIL_TEMPLATE_FIELD);
        $this->inlineTranslation->suspend();    
        $this->generateTemplate(['body'=>$body], $senderInfo, $receiverInfo);    
        $transport = $this->_transportBuilder->getTransport();
        $transport->sendMessage();        
        $this->inlineTranslation->resume();
    }
 
    /**
     * [sendMailToCustomer description]                  
     * @param  Mixed $emailTemplateVariables 
     * @param  Mixed $senderInfo             
     * @param  Mixed $receiverInfo           
     * @return void
     */
    public function sendMailToAdmin($feedback)
    {
        try{
            $feedback = (array)($feedback);
            $emailTemplateVariables = [];
            $body = __('Dear Admin,') . PHP_EOL;
            $body .= __('You have received a new feedback on your site.') . PHP_EOL . PHP_EOL;
            foreach ($feedback as $key=>$value) {
                $body .= ucfirst($key) . ': ' . $value . PHP_EOL;
            }
            $emailTemplateVariables['body'] = $body;
            $receiverInfo = [
                'name' => 'Admin',
                'email' => $this->getConfigValue('ws_fdb/feedback_group/ws_fdb_receiver_email', $this->getStore()->getStoreId()),
            ];
            $senderInfo = [
                'name' => $this->getConfigValue('ws_fdb/feedback_group/ws_fdb_sender_name', $this->getStore()->getStoreId()),
                'email' => $this->getConfigValue('ws_fdb/feedback_group/ws_fdb_sender_email', $this->getStore()->getStoreId()),
            ];
            $this->temp_id = $this->getTemplateId(self::XML_PATH_EMAIL_TEMPLATE_FIELD);
            $this->inlineTranslation->suspend();    
            $this->generateTemplate($emailTemplateVariables, $senderInfo, $receiverInfo);    
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();        
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
        }
    }
 
}