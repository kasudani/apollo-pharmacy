require([
    "jquery"
], function($){

//<![CDATA[

	$(document).ready(function() {
		var _FF = window._FF || {};
		_FF.formBtn = $('#feedback_holder');
		_FF.formEl = $('#formdiv');
		_FF.formCloseBtn = $('#feed_close');
		_FF.formBtn.on('click', function() {
			_FF.formBtn.hide();
			_FF.formEl.show();
			_FF.formEl.find('#fdb_name').focus();
		});

		_FF.formCloseBtn.on('click', function() {
			_FF.formBtn.show();
			_FF.formEl.hide();
		});

		$('#fdb_form').on('submit', function(e) {
			e.preventDefault();
			var $form = $(this),
				$error = $form.find('#fdb_error'),
				$btn = $form.find('#fdb_submit_btn'),
				_name = $.trim($form.find('#fdb_name').val()),
				_email = $.trim($form.find('#fdb_email').val()),
				_phone = $.trim($form.find('#fdb_phone').val()),
				_callme = $form.find('#fdb_call_me').is(':checked'),
				_website = $.trim($form.find('#fdb_website').val()),
				_message = $.trim($form.find('#fdb_msg').val())
				;
			if (_callme === true) {
				_callme = 1;
			} else {
				_callme = 0;
			}
			if (!_name.length) {
				$error.text('Please tell us your good name.');
				$form.find('#fdb_name').focus();
				return false;
			}
			if (!_email.length) {
				$form.find('#fdb_email').focus();
				$error.text('Please tell us your email.');
				return false;
			}
			if (!validate_email(_email)) {
				$form.find('#fdb_email').focus();
				$error.text('Your email seems to be invalid!');
				return false;
			}
			if (!_message.length) {
				$form.find('#fdb_msg').focus();
				$error.text('Please say a few words.');
				return false;
			}

			$error.text('');
			$btn.attr('disabled', 'disabled').val('Please wait...');

			var a = $.ajax({
				url: $form.attr('action'),
				method: 'post',
				dataType: 'json',
				data: {name:encodeURIComponent(_name), email:encodeURIComponent(_email), phone:encodeURIComponent(_phone), callme:encodeURIComponent(_callme), website:encodeURIComponent(_website), message:encodeURIComponent(_message)}
			}).done(function(response) {
				if (response && response.status) {
					$error.text(response.message);
					if (response.status == 'success') {
						$form.find('input[type=text], textarea').each(function(i, el) {
							$(el).val('');
						});
						setTimeout(function() {
							_FF.formCloseBtn.click();
						}, 3000);
					}
				}
			}).fail(function() {
				$error.text('Oops! Some error occurred.');
			}).always(function() {
				$btn.removeAttr('disabled').val('Submit');
			});
		});
	});

	function validate_email(str) {
		return /^([\w-_.]+)(\.[\w-_.]+)*@([\w\-]+)(\.[\w]{2,7})(\.[a-z]{2})?$/i.test(str);
	}

//]]>

});