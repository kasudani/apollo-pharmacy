require([
    "jquery"
], function($){

//<![CDATA[

	$(document).ready(function() {
		var _FF = window._FF || {};
		_FF.formBtn = $('#feedback_holder');
		_FF.formEl = $('#formdiv');
		_FF.formCloseBtn = $('#feed_close');
		_FF.formBtn.on('click', function() {
			_FF.formBtn.hide();
			_FF.formEl.show();
			_FF.formEl.find('#fdb_name').focus();
		});

		_FF.formCloseBtn.on('click', function() {
			//alert("here...");
			_FF.formBtn.show();
			_FF.formEl.hide();
		});

		$('#fdb_form').on('submit', function(e) 
		{
			e.preventDefault();
			var $form = $(this),
				$error = $form.find('#fdb_error'),
				$btn = $form.find('#fdb_submit_btn'),
				_name = $.trim($form.find('#fdb_name').val()),
				_email = $.trim($form.find('#fdb_email').val()),
				_phone = $.trim($form.find('#fdb_phone').val()),
				_callme = $form.find('#fdb_call_me').is(':checked'),
				_website = $.trim($form.find('#fdb_website').val()),
				_message = $.trim($form.find('#fdb_msg').val())
				;
			if (_callme === true) {
				_callme = 1;
			} else {
				_callme = 0;
			}

			if(!_name.length || !_email.length || !validate_email(_email) || !_message.length)
			{
				if(!_name.length)
				{
					$("#fdb_name_error").css('color', 'red');
					$("#fdb_name_error").text('Please enter the name.');	
					$form.find('#fdb_name').focus();
				}
				else 
				{ 
					$("#fdb_name_error").css('color', ''); 
					$("#fdb_name_error").text('');
				}

				if(!_email.length)
				{
					$("#fdb_email_error").css('color', 'red');
					$("#fdb_email_error").text('Please enter the email.');	
					$form.find('#fdb_email').focus();
				}
				else if(!validate_email(_email))
				{
					$("#fdb_email_error").css('color', 'red');
					$("#fdb_email_error").text('Please enter valid email');	
					$form.find('#fdb_email').focus();	
				}	
				else 
				{ 
					$("#fdb_email_error").css('color', ''); 
					$("#fdb_email_error").text('');
				}
				
				if(!_message.length)
				{
					$("#fdb_msg_error").css('color', 'red');
					$("#fdb_msg_error").text('Please say a few words.');	
					$form.find('#fdb_msg').focus();
				}
				else 
				{ 
					$("#fdb_msg_error").css('color', ''); 
					$("#fdb_msg_error").text('');
				}

				return false;	
			}
			else
			{
				$("#fdb_name_error").css('color', ''); 
				$("#fdb_name_error").text('');
				$("#fdb_email_error").css('color', ''); 
				$("#fdb_email_error").text('');
				$("#fdb_msg_error").css('color', ''); 
				$("#fdb_msg_error").text('');

				$error.text('');
				$btn.attr('disabled', 'disabled').val('Please wait...');

				var a = $.ajax({
					url: $form.attr('action'),
					method: 'post',
					dataType: 'json',
					data: {name:encodeURIComponent(_name), email:encodeURIComponent(_email), phone:encodeURIComponent(_phone), callme:encodeURIComponent(_callme), website:encodeURIComponent(_website), message:encodeURIComponent(_message)}
				}).done(function(response) {
					if (response && response.status) 
					{
						$error.text(response.message);
						if (response.status == 'success') 
						{
							$form.find('input[type=text], textarea').each(function(i, el) {
								//$(el).val('');
								$error.css({ 'color': 'blue', 'font-size': '100%' });
								$error.text('Your feedback has been received.');
							});

							setTimeout(function() {
								//_FF.formCloseBtn.click();
								$(".modals-wrapper").css("display", "none");
							}, 3000);
							
						}
					}
				}).fail(function() {
					$error.text('Oops! Some error occurred.');
				}).always(function() {
					$btn.removeAttr('disabled').val('Submit');
				});
			} //end of else  
			
		});
	});

	function validate_email(str) {
		return /^([\w-_.]+)(\.[\w-_.]+)*@([\w\-]+)(\.[\w]{2,7})(\.[a-z]{2})?$/i.test(str);
	}

//]]>

});