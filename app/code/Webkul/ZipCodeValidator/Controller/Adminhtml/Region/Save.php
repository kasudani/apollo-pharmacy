<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ZipCodeValidator
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ZipCodeValidator\Controller\Adminhtml\Region;

use Magento\Backend\App\Action;
use Magento\Framework\Filesystem\Driver\File;

class Save extends Action
{
    /**
     * @var \Webkul\ZipCodeValidator\Model\RegionFactory
     */
    protected $_region;

    /**
     * @var \Webkul\ZipCodeValidator\Model\ZipcodeFactory
     */
    protected $_zipcode;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploader;

    /**
     * @var time()
     */
    protected $_time;

    /**
     * Filesystem driver to allow reading of module.xml files which live outside of app/code
     *
     * @var DriverInterface
     */
    private $filesystemDriver;

    /**
     * @param Action\Context                                  $context
     * @param Webkul\ZipCodeValidator\Model\RegionFactory   $region
     * @param Webkul\ZipCodeValidator\Model\ZipcodeFactory  $zipcode
     * @param Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        Action\Context $context,
        \Webkul\ZipCodeValidator\Model\RegionFactory $region,
        \Webkul\ZipCodeValidator\Model\ZipcodeFactory $zipcode,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        File $filesystemDriver
    ) {
        $this->_region = $region;
        $this->_zipcode = $zipcode;
        $this->_fileUploader = $fileUploaderFactory;
        $this->_time = time();
        $this->filesystemDriver = $filesystemDriver;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_ZipCodeValidator::region');
    }

    /**
     * Save action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if (isset($data['region_id'])) {
            $msg = $this->updateRegion();
        } else {
            $regionId = 0;
            $collection = $this->_region
                ->create()
                ->getCollection();
            foreach ($collection as $key => $value) {
                if (strcasecmp($data['region_name'], $value->getRegionName()) == 0) {
                    $regionId = $value->getId();
                }
            }
            if (!$regionId) {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $region = $this->_region->create()->setData($data)->save();
                $regionId = $region->getId();
                $this->messageManager->addSuccess(__('Region Added Successfully'));
            }
            $msg = $this->processCsvData($regionId);
        }
        if ($msg) {
            foreach ($msg as $err) {
                $this->messageManager->addError($err);
            }
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
    /**
     * Update Region Details
     * @return array
     */
    public function updateRegion()
    {
        $regionId = 0;
        $data = $this->getRequest()->getParams();
        $this->_region->create()
            ->load($data['region_id'])
            ->setRegionName($data['region_name'])
            ->setStatus($data['status'])
            ->setUpdatedAt(date('Y-m-d H:i:s'))
            ->save();
        $this->messageManager->addSuccess(__('Region Updated Successfully'));
        $msg = $this->processCsvData($data['region_id']);
        return $msg;
    }
    /**
     * process csv data
     * @param integer $regionId
     * @return array
     */
    public function processCsvData($regionId)
    {
        $msg = [];
        $data = $this->getRequest()->getParams();
        $time = $this->_time;
        // Checking csv file
        try {
            $csvUploader = $this->_fileUploader->create(['fileId' => 'zipcodes-csv']);
            $csvUploader->setAllowedExtensions(['csv']);
            $validateData = $csvUploader->validateFile();
            $csvFilePath = $validateData['tmp_name'];
            $csvFile = $validateData['name'];
            $csvExt = explode('.', $csvFile);
            $csvFile = $time.'.'.$csvExt[1];
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $csvData = [];
        if (isset($csvFilePath)) {
            // Reading csv file
            $file = $this->filesystemDriver->fileOpen($csvFilePath, 'r');
            while (!$this->filesystemDriver->endOfFile($file)) {
                $csvData[] = $this->filesystemDriver->fileGetCsv($file, 1024);
            }
            $this->filesystemDriver->fileClose($file);
        }
        if ($csvData) {
            $msg = $this->saveCsvData($csvData, $regionId);
        } elseif (!isset($data['region_id'])) {
            $check = $this->checkZipcode($data['region_name'], $regionId);
            if (!$check) {
                $this->_zipcode->create()
                    ->setRegionZipcode($data['region_name'])
                    ->setRegionId($regionId)
                    ->setCreatedAt(date('Y-m-d H:i:s'))
                    ->setUpdatedAt(date('Y-m-d H:i:s'))
                    ->save();
            }
        }
        return $msg;
    }
    /**
     * save csv zipcodes
     * @param array $csvData
     * @param integet $regionId
     * @return array
     */
    public function saveCsvData($csvData, $regionId)
    {
        $msg = [];
        foreach ($csvData as $key => $zipcode) {
            $row = $key+1;
            $check = $this->checkZipcode($zipcode, $regionId);
            if ($check) {
                $msg[$key] = 'Skipped row '.$row.'. '.$zipcode[0].' already exists.';
            } elseif ($zipcode[0]) {
                $zipData['region_id'] = $regionId;
                $zipData['region_zipcode'] = $zipcode[0];
                $zipData['created_at'] = date('Y-m-d H:i:s');
                $zipData['updated_at'] = date('Y-m-d H:i:s');
                $this->saveZipcode($zipData);
            } else {
                $msg[$key] = 'Skipped row '.$row.'.';
            }
        }
        return $msg;
    }
    /**
     * check zipcode already saved or not
     * @param string $zipcode
     * @param integer $regionId
     * @return integer
     */
    public function checkZipcode($zipcode, $regionId)
    {
        $collection = $this->_zipcode->create()
            ->getCollection()
            ->AddFieldToFilter('region_id', $regionId)
            ->AddFieldToFilter('region_zipcode', $zipcode);
        return count($collection);
    }
    /**
     * save zipcodes
     * @return void
     */
    public function saveZipcode($data)
    {
        $this->_zipcode->create()->setData($data)->save();
    }
}
