<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Model;

use Webkul\PushNotification\Api\Data\UsersTokenInterface;
use Webkul\PushNotification\Model\ResourceModel\UsersToken\Collection;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class UsersTokenRepository implements \Webkul\PushNotification\Api\UsersTokenRepositoryInterface
{
    /**
     * resource model
     * @var \Webkul\PushNotification\Model\ResourceModel\Ebayaccounts
     */
    protected $_resourceModel;

    public function __construct(
        UsersTokenFactory $usersTokenFactory,
        \Webkul\PushNotification\Model\ResourceModel\UsersToken\CollectionFactory $collectionFactory,
        \Webkul\PushNotification\Model\ResourceModel\UsersToken $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_usersTokenFactory = $usersTokenFactory;
        $this->_collectionFactory = $collectionFactory;
    }
    
    /**
     * get by token id
     * @param  string $token
     * @return object
     */
    public function getByToken($token)
    {
        return $this->_collectionFactory->create()->addFieldToFilter('token', $token);
    }
}
