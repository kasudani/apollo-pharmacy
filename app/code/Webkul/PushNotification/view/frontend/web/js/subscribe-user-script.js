/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

/*jshint jquery:true*/
define([
    'jquery',
], function ($) {
    'use strict';
    var globalThis;
    $.widget('pushNotification.subscribeUser', {
        _create: function () {
            globalThis = this;
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register(globalThis.options.swPath).then(
                    function (reg) {
                    return reg.pushManager.getSubscription()
                    .then(function (sub) {
                        if (sub) {
                            return sub;
                        }
                        return reg.pushManager.subscribe({
                            userVisibleOnly: true
                        }).then(function (sub) {
                            var verOffset,browserName;
                            var agent = navigator.userAgent;
                            if ((verOffset=agent.indexOf("Chrome"))!=-1) {
                                browserName = "Chrome";
                                var token = sub.endpoint.split('send/').slice(-1)[0];
                            } else if ((verOffset=agent.indexOf("Firefox"))!=-1) {
                                browserName = "Firefox";
                                var token = sub.endpoint.split('v1/').slice(-1)[0];
                            }

                            $.ajax({
                                url : globalThis.options.ajaxUrl,
                                'data' : {
                                    'browser' : browserName,
                                    'token' : token
                                },
                                dataType: 'json',
                                success : function (response) {
                                    if (!response.error) {
                                        console.log('created user id '+response.info);
                                    } else {
                                        console.warn(response.error);
                                    }
                                },
                                error : function () {
                                    console.error(' user token didn\'t save in databse ');
                                }
                            });
                        });
                    });
                    }
                ).catch(function (error) {
                   console.log(error);
                });
            }
        }
    });
    return $.pushNotification.subscribeUser;
});