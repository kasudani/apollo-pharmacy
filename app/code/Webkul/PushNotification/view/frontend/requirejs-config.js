/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            subscribeUsers : 'Webkul_PushNotification/js/subscribe-user-script'
        }
    }
};