<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Api;

/**
 * @api
 */
interface TemplatesRepositoryInterface
{

    /**
     * get collection by template id
     * @param  int $templateId
     * @return object
     */
    public function getById($templateId);
}
