<?php
/**
 * @category   Webkul
 * @package    Webkul_PushNotification
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\PushNotification\Controller\Users;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Webkul\PushNotification\Model\UsersToken;
use Magento\Framework\Controller\Result\JsonFactory;
use Webkul\PushNotification\Api\UsersTokenRepositoryInterface;

class Save extends Action
{
    const NAME = 'guest';

    /**
     * @var UsersToken
     */
    protected $_userTokenModel;

    /**
     * @var Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var UsersTokenRepository
     */
    protected $_usersTokenRepository;

    /**
     * [__construct description]
     * @param Context                                     $context
     * @param UsersToken                                  $userTokenModel
     * @param JsonFactory                                 $resultJsonFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param UsersTokenRepositoryInterface               $usersTokenRepository
     */
    public function __construct(
        Context $context,
        UsersToken $userTokenModel,
        JsonFactory $resultJsonFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        UsersTokenRepositoryInterface $usersTokenRepository,
        \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct($context);
        $this->_userTokenModel = $userTokenModel;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_date = $date;
        $this->_usersTokenRepository = $usersTokenRepository;
        $this->_customerSession = $customerSession;
    }


    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $result = null;
        $time = $this->_date->gmtDate();
        $params = $this->getRequest()->getParams();
        if ($this->_customerSession->isLoggedIn()) {
            $params['name'] = $this->_customerSession->getCustomer()->getName();
        } else {
            $params['name'] = self::NAME;
        }
        $tokenCollection = $this->_usersTokenRepository->getByToken($params['token']);
        if (!$tokenCollection->getSize()) {
            $params['created_at'] = $time;
            $id = $this->_userTokenModel
                ->addData($params)->save()
                ->getId();
            if ($id) {
                $result = ['error' => false, 'info' => $id];
            } else {
                $result = ['error'=> 'user token did not saved in database' ];
            }
        }
        return $this->_resultJsonFactory->create()->setData($result);
    }
}
