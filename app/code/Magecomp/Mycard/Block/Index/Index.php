<?php
namespace Magecomp\Mycard\Block\Index;

use Magento\Framework\View\Element\Template\Context;
use Magecomp\Mycard\Model\Data;
use Magento\Framework\View\Element\Template;


class Index extends Template
{
        
	public function __construct(Context $context, Data $model)
	{
        $this->model = $model;
		parent::__construct($context);
                
	}
        
	public function sayHello()
	{
		return __('Hello World');
	}

    public function getHelloCollection()
    {
        $helloCollection = $this->model->getCollection();
        return $helloCollection;
    }

    public function getActiveSubscriptionCollection(){
    	$date = date("Y/m/d");
		$activeSubscriptionCollection = $this->model->getCollection()->addFieldToFilter('end_date',array('gt'=>$date))->setOrder('id','DESC');
		return $activeSubscriptionCollection;
    }

    public function getInactiveSubscriptionCollection(){
    	$date = date("Y/m/d");
		$inactiveSubscriptionCollection = $this->model->getCollection()->addFieldToFilter('end_date',array('lt'=>$date))->setOrder('id','DESC');
		return $inactiveSubscriptionCollection;
    }
}
?>