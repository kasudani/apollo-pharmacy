<?php

namespace Magecomp\Mycard\Block\Product;

use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection;

class CustomList extends ListProduct
{
	 protected $dataHelper;
     protected $prdCollection;
	 public function __construct(
            \Magento\Catalog\Block\Product\Context $context,        
            \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
            \Magento\Catalog\Model\Layer\Resolver $layerResolver,
            \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
            \Magento\Framework\Url\Helper\Data $urlHelper,
            \Retailinsights\CustomProdList\Helper\Data $dataHelper,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
            array $data = []
        ) {
            $this->dataHelper = $dataHelper;
            $this->prdCollection = $collectionFactory->create();
            parent::__construct(
                $context,
                $postDataHelper,
                $layerResolver,
                $categoryRepository,
                $urlHelper,
                $data
            );
        }

    public function getLoadedProductCollection()
    {
        return $this->_productCollection;
    }
    public function getCardProductCollection()
    {
        /*$collection = $this->prdCollection->addFieldToSelect('*')
                     ->addAttributeToFilter('ss_plan','493')
                     ->load();*/
        return $this->_productCollection;
    }
    public function setProductCollection(AbstractCollection $collection)
    {
        $this->_productCollection = $collection;
    }
    public function getProductCouponPrice($id){
    	 $this->dataHelper->getProductCouponPrice($id);
    }
    public function getCouponFinalPrice($id){
    	
    	 $this->dataHelper->getCouponFinalPrice($id);
    }
    public function getProductDiscount($id){
             
         $this->dataHelper->getProductDiscount($id);
    }

}