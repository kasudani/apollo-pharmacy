<?php
namespace Magecomp\Mycard\Model;
	 
use Magento\Framework\Model\AbstractModel;
	 
	class Data extends AbstractModel
	{	
	    protected function _construct()
	    {
	        $this->_init('Magecomp\Mycard\Model\ResourceModel\Data');
	    }
	}