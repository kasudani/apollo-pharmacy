<?php
namespace Magecomp\Mycard\Model\ResourceModel\Data;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
	    'Magecomp\Mycard\Model\Data',
	    'Magecomp\Mycard\Model\ResourceModel\Data'
	);
    }
}