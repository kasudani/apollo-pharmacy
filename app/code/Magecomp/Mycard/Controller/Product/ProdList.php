<?php
namespace Magecomp\Mycard\Controller\Product;

use Magecomp\Mycard\Block\Product\CustomList;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class ProdList extends Action
{
    protected $pageFactory;
    protected $productCollection;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->productCollection = $collectionFactory->create();
        parent::__construct($context);
    }
    public function execute()
    {
       $result = $this->pageFactory->create();
       $this->productCollection->addFieldToSelect('*');
       $this->productCollection->addAttributeToFilter('ss_plan','493');
        $collection = $this->productCollection->addAttributeToFilter('ss_plan','493')
        ->load();
        $list = $result->getLayout()->getBlock('custom.product.lists');
        $list->setProductCollection($collection);
        //$list->setTemplate('Magecomp_Mycard::product/list.phtml');
        return $result;
    }
}