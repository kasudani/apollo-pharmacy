<?php
/**
 * Catalog layer filter renderer
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Emizentech\Priceslider\Block;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;

class FilterRenderer extends \Magento\LayeredNavigation\Block\Navigation\FilterRenderer
{
    /**
     * @param FilterInterface $filter
     * @return string
     */
    public function render(FilterInterface $filter)
    {
        $this->assign('filterItems', $filter->getItems());
        $this->assign('filter' , $filter);
        $html = $this->_toHtml();
        $this->assign('filterItems', []);
        return $html;
    }

    public function getPriceRange($filter){
    	$Filterprice = array('min' => 0 , 'max'=>0);
    	if($filter instanceof Magento\CatalogSearch\Model\Layer\Filter\Price){
			$priceArr = $filter->getResource()->loadPrices(10000000000);
     		// $Filterprice['min'] = reset($priceArr);
     		// $Filterprice['max'] = end($priceArr);

            
    	}
        /* test code */
            $Filterprice['min'] = 0;
            $Filterprice['max'] = 1000;
    	return $Filterprice;
    }

    public function current_url()
    {
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true]);
    }

    public function url_query_params()
    {
        $current_url = $this->current_url();
        $url_params = parse_url($current_url);
        $url_query_params_array = [];

        if (array_key_exists('query', $url_params)) {
            # code...
            parse_str($url_params['query'], $url_query_params_array);
        }       

        return $url_query_params_array;
    }

    public function url_params_parsed($requested_url_param_key)
    {
        $url_query_params = $this->url_query_params();

        /* default values to set if query param is not in url */
        $response_url_params[0] = 0;
        $response_url_params[1] = 100;

        if (array_key_exists($requested_url_param_key, $url_query_params)) {
           
            $response_url_params = explode('-', $url_query_params[$requested_url_param_key]);
        }

        return $response_url_params;

    }


    public function test_discount_url()
    {
        $current_url_params = $this->url_query_params();
        $_is_custom_discount_exists_in_url = (array_key_exists('custom_discount', $current_url_params)) ? true : false;
        $query = [];

        # declare custom discount value to empty
        $query['custom_discount'] =  '';

        # if custom discount params is present in the url than re-create the params array
        if ($_is_custom_discount_exists_in_url) {
            
            foreach ($current_url_params as $key => $value) {
                
                if ($key !== 'custom_discount') {
                    
                    $query[$key] =  $current_url_params[$key];
                }

            }

        }        
        # return the new url by keeping custom discount params as empty...
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }

    public function test_package_url()
    {
        $current_url_params = $this->url_query_params();
        $_is_custom_discount_exists_in_url = (array_key_exists('package', $current_url_params)) ? true : false;
        $query = [];

        # declare custom discount value to empty
        $query['package'] =  '';

        # if custom discount params is present in the url than re-create the params array
        if ($_is_custom_discount_exists_in_url) {
            
            foreach ($current_url_params as $key => $value) {
                
                if ($key !== 'package') {
                    
                    $query[$key] =  $current_url_params[$key];
                }

            }

        }        
        # return the new url by keeping custom discount params as empty...
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }


    public function getFilterUrl($filter){
    		$query = ['price'=> ''];
    	 return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }

    public function getDiscountFilterUrl($filter){

        $current_url_params = $this->url_query_params();
        $query = ['custom_discount'=> ''];
        return $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true, '_query' => $query]);
    }

    public function getMinMaxForPackage($packageArray)
    {
        $min_max_array = [];
        $package_value_list = [];

        if (count($packageArray) == 1) {
            # code...
            $min_max_array['min'] = '0';
            $min_max_array['max'] = $packageArray[0]['from'];
        }
        else
        {
            foreach ($packageArray as $key => $value) {
                # store the package values by the 'from' index.
                array_push($package_value_list, $value['from']);
            }
            # sort it in ascending order
            sort($package_value_list);

            # set the minimum and maximum values for the package
            $min_max_array['min'] = reset($package_value_list);
            $min_max_array['max'] = end($package_value_list);

        }

        return $min_max_array;

    }


}
