/**
 * Solwin Infotech
 * Solwin ScrolltoTop Extension
 *
 * @category   Solwin
 * @package    Solwin_ScrolltoTop
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */

var popupcityval='';
require(['jquery'], function () {
    jQuery(document).ready(function () {

    //in cart and checkout pages we are getting conflict, so for now we have restricted this feature in cart and checkout pages.
    //if (!jQuery("body").hasClass("checkout-cart-index") && !jQuery("body").hasClass("checkout-index-index"))
    //{

        var availableTags = [
            'ACHAMPET',
            'ADANKI',
            'ADILABAD',
            'ADONI',
            'AHMEDABAD',
            'AKIVEEDU',
            'ALLAGADDA',
            'ALLAHABAD',
            'ALWAR',
            'AMALAPURAM',
            'AMBALA',
            'AMBASAMUDRAM',
            'AMBUR',
            'AMRITSAR',
            'AMUDALAVALASA',
            'ANAKAPALLI',
            'ANAND',
            'ANANTHAPUR',
            'ANNUR',
            'ANTHIYUR',
            'ARAKKONAM',
            'ARANI',
            'ARANTHANGI',
            'ARCOT',
            'ARMOOR',
            'ARUPPUKOTTAI',
            'ATTUR',
            'B KOTHA KOTA',
            'BADWEL',
            'BANAGANA PALLY',
            'BANGALORE',
            'BANGARUPALEM',
            'BANSWADA',
            'BAPATLA  ',
            'BAPULAPADU',
            'BARAKPOUR',
            'BARODA',
            'BELGAUM',
            'BELLARY',
            'BHADRACHALAM',
            'BHADRAVATHI',
            'BHAVANI',
            'BHEEMAVARAM',
            'BHUBNESHWAR',
            'BHUVANAGIRI',
            'BIJAPUR (KAR)',
            'BIKANER',
            'BILASPUR',
            'BOBBILI',
            'CHALLAPALLI',
            'CHANDIGARH',
            'CHANNAPATNA',
            'CHENGAM',
            'CHENNAI',
            'CHETPET',
            'CHEYYAR',
            'CHIDAMBARAM',
            'CHIKMAGALUR',
            'CHILAKALURIPETA',
            'CHINNA SALEM',
            'CHINNAMUNSHIDIVADA',
            'CHIPURUPALLI',
            'CHIRALA',
            'CHITRADURGA',
            'CHITTOOR',
            'COIMBATORE',
            'COLACHEL',
            'COOCH BEHAR',
            'CUDDALLORE',
            'CUDDAPAH',
            'CUMBUM',
            'CUTTACK',
            'DAKSHINA KANNADA',
            'DAVANAGERE',
            'DAVANGERE',
            'DEVAKOTTAI',
            'DEVARA KONDA',
            'DHARAPURAM',
            'DHARMAPURI',
            'DHARMAVARAM',
            'DHARWAD',
            'DHONE',
            'DINDIGUL',
            'DOWLESWARAM',
            'DRAKSHARAMAM',
            'DURG',
            'ELURU',
            'ERODE',
            'FARIDABAD',
            'GADWAL',
            'GAJAPATHINAGARAM',
            'GAJWEL',
            'GALIVEEDU',
            'GANDHI NAGAR',
            'GANDHINAGAR',
            'GANJAM',
            'GANNAVARAM',
            'GHAZIABAD',
            'GIDDALUR',
            'GINGEE',
            'GOA',
            'GODIKONDLA',
            'GUDIVADA',
            'GUDIYATHAM',
            'GUDUVANCHERRY',
            'GULBARGA',
            'GUNTAKAL',
            'GUNTUR',
            'GURGAON',
            'GUWAHATI',
            'HARUR',
            'HARYANA',
            'HASSAN',
            'HAVERI',
            'HINDUPUR',
            'HOOGHLY',
            'HOSUR',
            'HOWRAH',
            'HUBLI',
            'HUZURABAD',
            'HYDERABAD',
            'JADCHERLA',
            'JAIPUR',
            'JAJAPUR',
            'JALPAIGURI',
            'JAMMALAMADUGU',
            'JAMMIKUNTA',
            'JANGAON',
            'JANGAREDDY GUDEM',
            'JEYAMKONDAM',
            'JHAJJAR',
            'K.V.RANGAREDDY',
            'KADAPA',
            'KADIRI',
            'KADUR',
            'KAKINADA',
            'KALIKIRI',
            'KALLAKURICHI',
            'KALWAKURTHY',
            'KALYANDURGAM',
            'KAMAREDDY',
            'KANCHEEPURAM',
            'KANIPAKAM',
            'KANNAMANGALAM',
            'KANYAKUMARI',
            'KARAIKAL',
            'KARAIKUDI',
            'KARAMBAKUDI',
            'KARIM NAGAR',
            'KARIMANGALAM',
            'KARIMNAGAR',
            'KARUNGAL',
            'KARUR',
            'KARWAR',
            'KATPADI',
            'KATTUMANAAR KOIL',
            'KAVALI',
            'KAVERIPATNAM',
            'KAYALPATINAM',
            'KENDRAPARA',
            'KEONJHAR',
            'KHAMMAM',
            'KODADA',
            'KODAIKANAL',
            'KOILAKUNTA',
            'KOLAR',
            'KOLKATA',
            'KONDA MALLAPALLY',
            'KOOTHANALLUR',
            'KORBA',
            'KOTABOMMALI',
            'KOTHAKOTA',
            'KOTHAPETA',
            'KOTHUR',
            'KOVILPATTI',
            'KOVVUR',
            'KRISHNA',
            'KRISHNAGIRI',
            'KULITHALAI',
            'KUMARAPALAYAM',
            'KUMBAKONAM',
            'KUPPAM',
            'KURNOOL',
            'KURUKSHETRA',
            'KUTHALAM',
            'LUCKNOW',
            'MACHARLA',
            'MADANAPALLI',
            'MADHIRA',
            'MADURAI',
            'MAHABOOBABAD',
            'MAHABOOBNAGAR',
            'MAHABUBBAD',
            'MANAMADURAI',
            'MANDAPETA',
            'MANDYA',
            'MANGALORE',
            'MANNARGUDI',
            'MARTHANDAM',
            'MAYILADUDURAI',
            'MEDAK',
            'MELUR',
            'METTUR',
            'MIDNAPORE',
            'MIRYALAGUDA',
            'MITHAPUR',
            'MOHALI',
            'MOULIVAKKAM',
            'MUGALIVAKKAM',
            'MULAKALACHERVU',
            'MULBAGAL',
            'MUMBAI',
            'MUTHUPET',
            'MYLAVARAM',
            'MYSORE',
            'NADIA',
            'NAGAPATTINAM',
            'NAGAR KURNOOL',
            'NAGARI',
            'NAGERCOIL',
            'NAGPUR',
            'NALGONDA',
            'NAMAKKAL',
            'NANDIGAMA',
            'NANDIKOTKUR',
            'NANDYAL',
            'NARASANNAPETA',
            'NARASAPURAM',
            'NARASARAO PETA',
            'NARAYANAPET',
            'NARKETPALLY',
            'NASHIK',
            'NATHAM',
            'NAZARET',
            'NELLIMARLA',
            'NELLORE',
            'NEW BOMBAY',
            'NEW DELHI',
            'NEYVELI',
            'NIDADHAVOLU',
            'NIRMAL',
            'NITHRAVILAI',
            'NIZAMABAD',
            'NOIDA',
            'NORTH 24 PARGANAS',
            'NORTH GOA',
            'NUZVID',
            'ONGOLE',
            'OOTY',
            'OTHAKADAI',
            'OTTANCHATRAM',
            'PAKALA',
            'PALAKOLLU',
            'PALAKONDA',
            'PALANI',
            'PALASA',
            'PALAYANKOTTAI',
            'PALLIPATU',
            'PALMANER',
            'PANACHAMODU',
            'PANCHKULA',
            'PANRUTI',
            'PAPANASAM',
            'PAPPIREDDIPATTI',
            'PARVATHIPURAM',
            'PATNA',
            'PATTUKOTTAI',
            'PEBBAIR',
            'PEDANA',
            'PEDDAPURAM',
            'PENDURTHI',
            'PENUGONDA',
            'PENUMOOR',
            'PERAMBALUR',
            'PERAVURANI',
            'PERIYAKULAM',
            'PIDUGURAALLA',
            'PILERU',
            'PITAPURAM',
            'PODICHERRY',
            'POLLACHI',
            'PONDY',
            'PONNERI',
            'PONNUR',
            'PORTBLAIR',
            'PORUMAVILLA',
            'PRODDUTUR',
            'PUDUKOTTAI',
            'PULIAMPATTI',
            'PULIVENDULA',
            'PUNE',
            'PUNGANOOR',
            'PURI',
            'PUTTAPARTHI',
            'PUTTUR',
            'RAIGAD',
            'RAIPUR',
            'RAJAHMUNDRY',
            'RAJAM',
            'RAJAPALAYAM',
            'RAJKOT',
            'RAJUMPET',
            'RAMANAGAR',
            'RAMAVARAPPADU',
            'RAMESWARAM',
            'RANCHI',
            'RANIPET',
            'RASIPURAM',
            'RAYACHOTI',
            'RC PURAM',
            'REDHILLS',
            'REPALEE',
            'ROURKELA',
            'RUPNAGAR',
            'S KOTA',
            'SALEM',
            'SALURU',
            'SAMBALPUR',
            'SANGAREDDY',
            'SANKARANKOVIL',
            'SANKARAPURAM',
            'SATHYAMANGALAM',
            'SATTENA PALLI',
            'SATTUPALLI ',
            'SHADNAGAR',
            'SHIMOGA',
            'SHOLINGUR',
            'SINGAMPUNARI',
            'SIRKAZHI',
            'SIVAGANGAI',
            'SIVAKASI',
            'SOMPETA',
            'SOUTH 24 PARGANAS',
            'SOUTH GOA',
            'SRIKAKULAM',
            'SRIKALAHASTI',
            'SRIVELLIPUTTUR',
            'SULLURIPET',
            'SUNDERGARH',
            'SURAT',
            'SURYAPETA',
            'TADEPALLIGUDAM',
            'TADIPATRI',
            'TAGARAPUVALASA',
            'TANUKU',
            'TEKKALI',
            'TENALI',
            'TENKASI',
            'THADEPALLIGUDAM',
            'THAKALAE',
            'THANDUR',
            'THANE',
            'THANE DIST',
            'THANJAVUR',
            'THENI',
            'THIAGATHURUVAM',
            'THIRUCHENDUR',
            'THIRUMANGALAM',
            'THIRUNINDRAVUR',
            'THIRUPATTUR',
            'THIRUTHANI',
            'THIRUTURAIPUNDI',
            'THIRUVALLUR',
            'THIRUVANNAMALAI',
            'THIRUVARUR',
            'THITTAKUDI',
            'THONDI',
            'THRISSUR',
            'TIRUCHIRAPPALLI',
            'TIRUNELVELI',
            'TIRUVURU',
            'TRICHY',
            'TUMKUR',
            'TUNI',
            'TUTICORIN',
            'UDUMALAIPET',
            'UDUPI',
            'UP',
            'USILAMPATTI',
            'UTTHANGARAI',
            'VADDALORE',
            'VADODARA',
            'VANDAVASI',
            'VANIAMBADI',
            'VAYALPADU',
            'VAZHAPADI',
            'VEDHARANYAM',
            'VEERA GHATTAM',
            'VELLORE',
            'VEMPALLY',
            'VENKATAGIRI KOTA',
            'VIJAYAWADA',
            'VILLUPURAM',
            'VINUKONDA',
            'VIRUDHACHALAM',
            'VIRUDHUNAGAR',
            'VIRUPACHIPURAM',
            'VIRUTHACHALAM',
            'VISAKHAPATNAM',
            'VISHAKAPATNAM',
            'VIZIANAGARAM',
            'VUYYURU',
            'WALAJAPET',
            'WALLAJABAD',
            'WANAPARTHY',
            'WARANGAL',
            'YALLANDU',
            'YAMUNA NAGAR',
            'YELAMANCHALI',
            'YEMMIGANUR',
            'YERRAGUNTLA'
        ];
        
        jQuery(document).ready(function() {
            /*jQuery( "#popupcity" ).autocomplete({
              source: availableTags,
              select: function (event, ui) 
              { 
                
                //alert("test : "+ui.item.value);
                jQuery('#locality_name').val(ui.item.value); 
                var city_val = ui.item.value;
                var applicationUrl = jQuery('#application_url').val();
                //alert("applicationUrl : "+ applicationUrl);
                
                jQuery.ajax({
                        url: 'http://20.0.0.195/apollo/ordermedicine/medicine/citysession',
                        //url: 'http://eshoptest.apollopharmacy.org/webuat/apollo/ordermedicine/medicine/citysession',
                        method: "post",
                        data: {"city_val": city_val},
                        dataType: "html",
                        success: function (response) { }
                    });
                }
            });   */
            


         });
            
        

        
        
        
        //}//End of the restriction

    });

        

    
});


require(
        [
            'jquery',
            'Magento_Ui/js/modal/modal',
            'mage/cookies'
        ],
        function(
            $,
            modal
        ) {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                title: 'Service Locator',
                autoOpen: true,
                buttons: [{
                    text: $.mage.__('Submit Selected Locality'),
                    class: '',
                    click: function () 
                    {
                        //jQuery('#localityform').submit();
                        var popupcity_req_val = jQuery('#popupcity').val();
                        var localitypincode_req_val = jQuery('#localitypincode').val();
                        var pincodesession_req_val = jQuery('#pincodesession').val();
                        var pcasession_req_val = jQuery('#pcasession').val();
                        var locality_name_req_val = jQuery('#locality_name').val();
                        var selected_storename = jQuery('#selected_storename').val();

                        /*
                        if(locality_name_req_val=="")
                        {
                            locality_name_req_val = '<?php print $_SESSION["city_val"];';
                        }
                        */

                        var noresultcounter=0;
                        jQuery(".ui-helper-hidden-accessible").each(function() {
                            if(jQuery(".ui-helper-hidden-accessible").html()=="No search results.")
                            {
                                noresultcounter=parseInt(noresultcounter)+1;
                            }
                            else
                            {
                                noresultcounter=parseInt(noresultcounter)-1;
                            }  
                        });
                        
                        /*if(popupcity_req_val=="")
                        {
                            alert("Please enter the city");
                            jQuery("#popupcity").focus();
                            return false;
                        }
                        else if(localitypincode_req_val=="")
                        {
                            alert("Please enter the locality/pincode");
                            jQuery("#localitypincode").focus();
                            return false;
                        }
                        else */
                        if(localitypincode_req_val != "" && noresultcounter > 0 )
                        {
                            alert("Please select the locality/pincode");
                            jQuery('#localitypincode').val('');
                            return false;
                        }
                        else if(popupcity_req_val != "" && noresultcounter > 0)
                        {
                            alert("Please select the city");
                            
                            jQuery('#popupcity').val('');
                            jQuery('#locality_name').val('');
                    
                            jQuery('#localitypincode').val('');
                            jQuery('#pincodesession').val(); 
                            jQuery('#pcasession').val();

                            return false;
                        }
                        else if(pincodesession_req_val=="")
                        {
                            alert("Please select the locality/pincode");
                            jQuery("#localitypincode").val('');
                            jQuery("#localitypincode").focus();
                            return false;
                        }
                        else if(popupcity_req_val!="" && localitypincode_req_val!="")
                        {
                            //alert(popupcity_req_val+":"+localitypincode_req_val+":"+pincodesession_req_val);
                            //return false;
            
                            //alert("form is submitting...");
                            //return false;
                            var applicationUrl = jQuery('#application_url').val();
                            //alert("applicationUrl : "+applicationUrl);
                            //alert("selected_storename : "+selected_storename);
                            //jQuery('#service_locator_id a p').html(selected_storename);
                            jQuery('#service_locator_id a p').html(selected_storename);
                            jQuery('#service_locator_id a p').append("<br> <span>Home Delivery Available</span>");
                            //jQuery('#service_locator_id a').html('test area..');
                            jQuery.ajax({
                                url: applicationUrl+'ordermedicine/medicine/pincodesession',
                                //url: 'http://eshoptest.apollopharmacy.org/webuat/apollo/ordermedicine/medicine/pincodesession',
                                method: "post",
                                data: {"pincodesession_req_val": pincodesession_req_val,"pcasession_req_val":pcasession_req_val,"popupcity_req_val":popupcity_req_val,"selected_storename":selected_storename},
                                dataType: "html",
                                success: function (response) { 
                                    $.cookie("popup_cookie", "popup_cookie_exist");
                                }
                            });
                            
                            this.closeModal();
                        }
                    
                    }
                }]
            };

            /*
            if (!$("body").hasClass("checkout-index-index")) 
            {
                var popup = modal(options, $('#popup-modal'));
                $('#popup-modal').modal('openModal');   
            }
            */
            jQuery('.skip_and_explore').click(function () {
                var applicationUrl = jQuery('#application_url').val();
                jQuery.ajax({
                    url: applicationUrl+'ordermedicine/medicine/pincodesession',
                    //url: 'http://eshoptest.apollopharmacy.org/webuat/apollo/ordermedicine/medicine/pincodesession',
                    method: "post",
                    data: {"skip_and_explore": "skipped"},
                    dataType: "html",
                    success: function (response) {
                        //$.cookie("popup_cookie", "popup_cookie_exist");
                    }
                });
            });

            jQuery('#service_locator_id,#service_locator_id_mobile').click(function () {
                if ($("body").hasClass("checkout-index-index")) {
                    var shipping_policy_block = $('#opc-shipping_method').is(':visible');
                    if(shipping_policy_block == true){
                        var popup = modal(options, $('#popup-modal'));
                        $('#popup-modal').modal();
                    }
                }else{
                    var popup = modal(options, $('#popup-modal'));
                    $('#popup-modal').modal();
                }
            });

            //jQuery('#localitypincode').blur(function () {
            jQuery(document).on('focus keyup blur change', '#localitypincode', function() {
                var localitypincodeval = jQuery("#localitypincode").val();
                if(localitypincodeval.length <= 3){
                    jQuery("#popupcity").val("");
                    jQuery("#pincodesession").val("");
                    jQuery("#pcasession").val("");
                    jQuery("#selected_storename").val("");
                }
            });

            jQuery(document).on('dblclick', '.customer_account_link', function() {
                var applicationUrl = jQuery('#application_url').val();
                window.location.href = applicationUrl+'customer/account';
            });



            
            jQuery(document).ready(function() {
            
                //in cart and checkout pages we are getting conflict, so for now we have restricted this feature in cart and checkout pages.
                //if (!jQuery("body").hasClass("checkout-cart-index") && !jQuery("body").hasClass("checkout-index-index"))
                //{

                //}

                /*jQuery('#popupcity').click(function () {
                    jQuery('#popupcity').val('');
                    jQuery('#locality_name').val('');
                    
                    jQuery('#localitypincode').val('');
                    jQuery('#pincodesession').val(); 
                    jQuery('#pcasession').val();

                    return false;
                });*/

                /*jQuery('#localitypincode').click(function () {
                    jQuery('#localitypincode').val('');
                    jQuery('#pincodesession').val(); 
                    jQuery('#pcasession').val();
                    return false;
                });*/

            
            
                /*if($.cookie('popup_cookie')) //if cookie
                {
                    //alert("cookie exist");
                    //alert($.cookie("popup_cookie"));
                } 
                else //no cookie
                {*/
                    //Close Popup in home page
                    /*if ($("body").hasClass("cms-index-index"))
                    {
                        var selectedlocation = $("#localitypincode").val();
                        var skip_and_explore = $("#skip_and_explore").val();
                        if(selectedlocation == "" && skip_and_explore == ""){
                            jQuery(document).ready(function() {
                                var popup = modal(options, $('#popup-modal'));
                                $('#popup-modal').modal();

                            });
                        }
                    } */
                //}

            });

            
            
        }
    );


//***************************//

require(['jquery'], function () {
    //jQuery(window).load(function(){
        jQuery("form.form-create-account :input[type=text],form.form-create-account :input[type=email],form.form-create-account :input[type=password]").each(function(index, elem) {
            var eId = jQuery(elem).attr("id");
            var label = null;
            if (eId && (label = jQuery(elem).parents("form").find("label[for="+eId+"]")).length == 1) {
                var fieldlabel = jQuery(label).html();
                fieldlabel = jQuery.trim(fieldlabel); //trim spaces
                fieldlabel = fieldlabel.replace(/<(?:.|\n)*?>/gm, ''); //striphtml
                jQuery(elem).attr("placeholder", fieldlabel);
                jQuery(label).hide();
            }
        });


        jQuery('.expland_in_mobile').click(function () {
            var selector = jQuery(this).attr("content_selector");
            jQuery(selector).slideToggle(500);
        });
    //});
});


require([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate',
    'Magento_Ui/js/modal/modal'
], function ($) {
    //Validate Image FileSize
    $.validator.addMethod(
        'validate-filesize', function (v, elm) {
            var maxSize = 2 * 1000000;
            if (navigator.appName == "Microsoft Internet Explorer") {
                if (elm.value) {
                    var oas = new ActiveXObject("Scripting.FileSystemObject");
                    var e = oas.getFile(elm.value);
                    var size = e.size;
                }
            } else {
                if (elm.files[0] != undefined) {
                    size = elm.files[0].size;
                }
            }

            if (size != undefined && size > maxSize) {
                return false;
            }
            return true;
        }, $.mage.__('The file size should not exceed 2MB'));
    //Validate Image Extensions
    $.validator.addMethod(
        'validate-document', function (v, elm) {
            var extensions = ['jpg', 'png', 'doc', 'docx','pdf'];
            var prescriptionshistory = [];
            $.each($("input[name='previous_prescriptions[]']:checked"), function(){
                prescriptionshistory.push($(this).val());
            });
            if (prescriptionshistory != "") {
                return true;
            }
            if (!v) {
                return false;
            }
            with (elm) {
                var ext = value.substring(value.lastIndexOf('.') + 1);
                for (i = 0; i < extensions.length; i++) {
                    if (ext == extensions[i]) {
                        return true;
                    }
                }
            }
            return false;
        },
        function (v, elm) {
            var prescriptionshistory = [];
            $.each($("input[name='previous_prescriptions[]']:checked"), function(){
                prescriptionshistory.push($(this).val());
            });
            if (prescriptionshistory == "") {
                return $.mage.__('Please choose file & submit.')
            }else{
                return $.mage.__('Disallowed file type.')
            }
        }
    );



    if (jQuery(".scrollup").length > 0) {
        // scroll-to-top button show and hide
        jQuery(document).ready(function () {
            jQuery(window).scroll(function () {
                if (jQuery(this).scrollTop() > 100) {
                    jQuery('.scrollup').fadeIn();
                } else {
                    jQuery('.scrollup').fadeOut();
                }
            });
            // scroll-to-top animate
            jQuery('.scrollup').click(function () {
                jQuery("html, body").animate({scrollTop: 0}, 600);
                return false;
            });
        });
    }
    jQuery('.delivery_option').click(function () {  //alert("delivery option clicked...");
        var divid = jQuery(this).attr("id");
        jQuery('.delivery_type').hide();
        var selectedoption = jQuery(this).val();
        //if(selectedoption == "home_delivery"){
            var applicationUrl = jQuery('#application_url').val();
            var customerLoggedInStatus = jQuery("#customer_logged_in_status").val();
            if(customerLoggedInStatus == 0){
                //if (confirm("You need to login to proceed") == true) {
                    //window.location.href = applicationUrl+'customer/account/login/referer/aHR0cDovL2VzaG9wdGVzdC5hcG9sbG9waGFybWFjeS5vcmcvd2VidWF0L2Fwb2xsby9jaGVja291dC9jYXJ0L2luZGV4Lw,,/';
                    jQuery(".social-login").trigger( "click" );
                //}
                //return false;
            }
        /*}
        else
        {
            var applicationUrl = jQuery('#application_url').val();
            var store_pickup_pincode =jQuery('#store_pickup_pincode').val();
            var store_selection = jQuery('input[name=store_selection]:checked').val();

            jQuery.ajax({
                url: applicationUrl+'/ordermedicine/medicine/storesession',
                method: "post",
                data: {"storetype": selectedoption,"store_pickup_pincode":store_pickup_pincode,"store_selection":store_selection},
                dataType: "html",
                success: function (response) { }
            });

        }*/
        jQuery('.' + divid + '_div').show();
    });
    var i = 0;
    jQuery('#moreprescription,#moreprescription_store').click(function () {
        var thisid = jQuery(this).attr('id');
        var container_class = (thisid == "moreprescription") ? ".prescription_add" : ".prescription_add_store";
        var more_prescriptions_class = (thisid == "moreprescription") ? "more_prescriptions" : "more_prescriptions_store";

        i++;
        var addinput = '<div class="'+more_prescriptions_class+' upload_more_prescription_' + i + '">'
            + '<span>Choose Prescription</span>'
            + '<input data-validate="{required:true,\'validate-document\':true}" type="file" name="upload_prescription_'+i+'" value="">'
            + '<button type="button" inc_id="' + i + '" class="delete_more_prescription" id="delete_more_prescription_' + i + '">Delete</button></div>';
        jQuery(container_class).append(addinput);

        if (jQuery("."+more_prescriptions_class).length < 4) {
            jQuery('#'+thisid).show();
        } else {
            jQuery('#'+thisid).hide();
        }

    });
    jQuery("body").on("click", ".delete_more_prescription", function () {
        var id = jQuery(this).attr('inc_id');
        jQuery('.upload_more_prescription_' + id).remove();
        if (jQuery('.more_prescriptions').length < 4) {
            jQuery('#moreprescription').show();
        } else {
            jQuery('#moreprescription').hide();
        }
    });

    /*
    var selected_store_count = 0;
    var selected_store_id = "";

    jQuery(".store_selection").each(function() {
        //alert("in each loop...");
        selected_store_count = jQuery("input[name='store_selection']:checked").val();

        if(selected_store_count>0)
        {
            //alert("in if....");
            //jQuery(".store_selection").click();
            store_selection_auto_submission(selected_store_count);
            return false;
        }
        else{ alert("in else..."); }
    });
    */

    function store_selection_auto_submission(store_id)
    {
        //alert("store_id");
        var selectedoption = jQuery('.delivery_option').val();
        alert(selectedoption); //return false;

        var pincode = jQuery('#store_pickup_pincode').val();
        var applicationUrl = jQuery('#application_url').val();
        if(pincode.length == 6){

        }


    }

    /*jQuery("body").on("click", ".store_selection", function () {

        var selectedoption = jQuery('.delivery_option').val();
        //alert(selectedoption); //return false;

        var pincode = jQuery('#store_pickup_pincode').val();
        var applicationUrl = jQuery('#application_url').val();
        if(pincode.length == 6){//alert("here");
            jQuery('body').loader('show');
            jQuery.ajax({
                url: applicationUrl+'/ordermedicine/medicine/storelocations',
                method: "post",
                data: {"pincode": pincode, type: 'store_selection',store_id: jQuery(this).val()},
                dataType: "html",
                success: function (response) {
                    if (response != '') {
                        jQuery('.store_locations_result').html(response);
                    }

                    //03112017
                    //jQuery(".product_page_cod_check_form,#cart_store_pickup_form").submit();
                    //jQuery(".check_store_availability").click();

                    jQuery('body').loader('hide');
                }
            });
        }
    })*/

    jQuery( ".product_page_cod_check_form,#localityform" ).submit(function( event ) { //#cart_store_pickup_form
        //alert("submitting...");
        event.preventDefault();
    });


    jQuery(".check_pincode_availability").click(function () {
        var pincode = jQuery('#cod_pincode').val();
        var product_type = jQuery('#product_type_hidden').val();
        var applicationUrl = jQuery('#application_url').val();
        if(pincode.length == 6 && jQuery.isNumeric( pincode )){
            jQuery('body').loader('show');
            jQuery.ajax({
                url: applicationUrl+'/ordermedicine/medicine/productzipcodecheck',
                method: "post",
                data: {"pincode": pincode, type: 'html', product_type: product_type},
                dataType: "html",
                success: function (response) {
                    if (response != '') {
                        jQuery('#pincode_check_message').html(response);
                    }
                    jQuery('body').loader('hide');
                }
            });
        }
    });
    jQuery(".check_store_availability").click(function () {
        var pincode = jQuery('#store_pickup_pincode').val();
        var applicationUrl = jQuery('#application_url').val();
        if(pincode.length == 6){
            jQuery('body').loader('show');
            jQuery.ajax({
                url: applicationUrl+'/ordermedicine/medicine/storelocations',
                method: "post",
                data: {"pincode": pincode, type: 'html'},
                dataType: "html",
                success: function (response) {
                    if (response != '') {
                        jQuery('.store_locations_result').html(response);
                    }
                    jQuery('body').loader('hide');
                }
            });
        }
    });

    jQuery(".localitypincode-choosen").autocomplete({
        //source: "http://20.0.0.195/apollo/ordermedicine/medicine/localitypincodes",
        source: function(request, response) {
            jQuery(".locality_loading").show();
            var applicationUrl = jQuery('#application_url').val();
            jQuery.ajax({
                url: applicationUrl+"ordermedicine/medicine/localitypincodes",
                dataType: "json",
                cache: true,
                type: "get",
                data: { term: request.term },
                beforeSend: function(){
                    jQuery(".locality_loading").show();
                    //jQuery('.ui-helper-hidden-accessible').html("");
                },
            }).done(function(data) {
                jQuery(".locality_loading").hide();
                response(data);
            });
        },
        minLength: 4,
        //source: "http://eshoptest.apollopharmacy.org/webuat/apollo/ordermedicine/medicine/localitypincodes",
        select: function(event, ui) {
            console.log(ui.item);
            //alert(ui.item.value);
            jQuery('#pincodesession').val(ui.item.id);
            jQuery('#pcasession').val(ui.item.value);
            jQuery('#popupcity').val(ui.item.city);
            jQuery('#selected_storename').val(ui.item.storename);
            jQuery('.ui-helper-hidden-accessible').html("");


            jQuery('#localitypincode').val(ui.item.value);
            jQuery('.localitypincode-choosen').val(ui.item.id);
            jQuery('.localitypincode-address').html(ui.item.value);
            jQuery('.localitypincode-selector').html('X');
            return false;

        },
        messages: {
            noResults: 'No locations are available for selected pincode',
            results: function() {}
        }
    });

    /* delete selelcted locality pincode */
    jQuery('.localitypincode-selector').click(function() {
        console.log('.localitypincode-selector clicked');

        jQuery(".localitypincode-choosen").val('');
        jQuery('#localitypincode').val('');
        jQuery('.localitypincode-address').html('');
        jQuery('.localitypincode-selector').html('');
    });

    //new

});




