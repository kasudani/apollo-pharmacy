<?php
namespace Retail\OutofstockNotify\Controller\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    
    protected $_objectManager;
    
    protected $_notifyFactory;

    protected $_transportBuilder;

    protected $_productRepopsitory;

    protected $_imageHelper;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Retail\OutofstockNotify\Model\NotifyFactory $nt,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Helper\ImageFactory $imageHelperFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_notifyFactory = $nt;
        $this->_transportBuilder = $transportBuilder;
        $this->_productRepopsitory = $productRepository;
        $this->_imageHelper = $imageHelperFactory;
        parent::__construct($context);
    }
    public function execute()
    {
       
       $data = $this->getRequest()->getPostValue();
       $model = $this->_notifyFactory->create();
       $collection = $model->getCollection();
       $collection->addFieldToFilter('customer_mail', $data['stockalert_email']);
       $collection->addFieldToFilter('product_id',  $data['product_id']);

       # load product
       $product = $this->_productRepopsitory->getById($data['product_id']);
       #set title and url
       $product_title = $product->getName();
       $product_url = $product->getProductUrl();

       if (empty($collection->getData())) {
                $model->setData('customer_mail', $data['stockalert_email']);
                $model->setData('product_id', $data['product_id']);
                $model->save();
                $templateVars = array(
                                        'product_title'=> $product_title,
                                        'product_url' => $product_url
                                    );
                /* send mail to customer */
                $cust_email = $data['stockalert_email'];
                 $transport = $this->_transportBuilder->setTemplateIdentifier(9)
                    ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
                    ->setTemplateVars($templateVars)
                    ->setFrom('general')
                    ->addTo($cust_email)
                    ->getTransport();
                    $transport->sendMessage();

                echo "submitted";
            } else {
               echo "already there";
            }
    }
}
