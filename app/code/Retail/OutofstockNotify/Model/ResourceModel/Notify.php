<?php
namespace Retail\OutofstockNotify\Model\ResourceModel;

class Notify extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('out_of_stock_notification', 'id');
    }
}
