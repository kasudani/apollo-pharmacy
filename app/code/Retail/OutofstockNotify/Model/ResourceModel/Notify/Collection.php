<?php

namespace Retail\OutofstockNotify\Model\ResourceModel\Notify;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\OutofstockNotify\Model\Notify', 'Retail\OutofstockNotify\Model\ResourceModel\Notify');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
