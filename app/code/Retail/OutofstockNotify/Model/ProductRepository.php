<?php
namespace Retail\OutofstockNotify\Model;

use Magento\Catalog\Model\ResourceModel\Product\Collection;

    class ProductRepository extends \Magento\Catalog\Model\ProductRepository
    {
            
            public function get($sku, $editMode = false, $storeId = null, $forceReload = false)
			    {
			    	if($sku == 'HEA0265-HEA0265-HEA0265'){
			    		$sku = 'HEA0265';
			    	}
			        $cacheKey = $this->getCacheKey([$editMode, $storeId]);
			        if (!isset($this->instances[$sku][$cacheKey]) || $forceReload) {
			            $product = $this->productFactory->create();

			            $productId = $this->resourceModel->getIdBySku($sku);
			            if (!$productId) {
			                throw new NoSuchEntityException(__('Requested product doesn\'t exist'));
			            }
			            if ($editMode) {
			                $product->setData('_edit_mode', true);
			            }
			            if ($storeId !== null) {
			                $product->setData('store_id', $storeId);
			            }
			            $product->load($productId);
			            $this->instances[$sku][$cacheKey] = $product;
			            $this->instancesById[$product->getId()][$cacheKey] = $product;
			        }
			        return $this->instances[$sku][$cacheKey];
			    }

		    public function getById($productId, $editMode = false, $storeId = null, $forceReload = false)
			    {
			        $cacheKey = $this->getCacheKey([$editMode, $storeId]);
			        if (!isset($this->instancesById[$productId][$cacheKey]) || $forceReload) {
			            $product = $this->productFactory->create();
			            if ($editMode) {
			                $product->setData('_edit_mode', true);
			            }
			            if ($storeId !== null) {
			                $product->setData('store_id', $storeId);
			            }
			            $product->load($productId);
			            if (!$product->getId()) {
			                throw new NoSuchEntityException(__('Requested product doesn\'t exist'));
			            }
			            $this->instancesById[$productId][$cacheKey] = $product;
			            $this->instances[$product->getSku()][$cacheKey] = $product;
			        }
			        return $this->instancesById[$productId][$cacheKey];
			    }

    }
?>