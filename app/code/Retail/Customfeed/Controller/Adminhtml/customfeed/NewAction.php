<?php

namespace Retail\Customfeed\Controller\Adminhtml\customfeed;

class NewAction extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $resultForwardFactory;
    protected $_messageManager;
    protected $scopeConfig;
    protected $productRepository;
    protected $_stockItemRepository;
    protected $_product;
    protected $_productCollectionFactory;
    protected $_stockRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Retail\Customfeed\Model\CustomfeedFactory $CustomfeedFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
        $this->_messageManager = $messageManager;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->_customfeedFactory = $CustomfeedFactory;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_product = $product;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_stockRegistry = $stockRegistry;
        parent::__construct($context);
    }

    /**
     * Forward to edit
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId);
    }
    public function loadMyProduct($sku)
    {
        return $this->productRepository->get($sku);
    }

    public function stockInformation($product_id)
    {
        return $this->_stockRegistry->getStockItem($product_id);
    }

    public function customeFeedFactory()
    {
        return $this->_customfeedFactory->create();
    }

    public function getProductIfAvailable($sku)
    {
        # code...
        $productCollection = $this->_productCollectionFactory->create()->addAttributeToSelect('*');        
        $collection = $productCollection->addFieldToFilter('sku', array('eq' => $sku));
        if ($collection->getData()) {
            # if product exists...
            return $this->loadMyProduct($sku);
        }
        else
        {
            # if product do not exists...
            return false;
        }

    }

    public function execute()
    {
                
        $attribute_option_id = '';
        $attribute_option_text = '';

        $collection1 = $this->_customfeedFactory->create()->getCollection();

        $dat=$collection1->addFieldToSelect('sku');

        $values=array();

        foreach($dat as $dt){
            array_push($values,$dt['sku']);
        }     

        $data=$this->scopeConfig->getValue('drc_productfeed/configurable_exclude_products/skus', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $skus=  (explode(",",$data)); 
        $missing_skus=array_diff($skus,$values); 
        
        $html="Following sku's are not found..... ";

        $html1="Following sku's are added successfully..... ";
        $error=1;
        $error1=0;

        /* attribute code */
        $attribute_code = 'brand';
       
        if(!empty($missing_skus))
        {
              
            foreach($missing_skus as $sku)
            {
                try 
                {
                    // $product=$this->loadMyProduct($sku);
                    $productInformation = $this->loadMyProduct($sku);
                    $brand = $productInformation->getResource()->getAttribute($attribute_code)->getFrontend()->getValue($productInformation);
                    
                    # get option text and Id for the product brand
                    $attribute = $productInformation->getResource()->getAttribute($attribute_code);

                    # get attribute option id of product brand from brand code
                    if ($attribute->usesSource()) {
                        $attribute_option_id = $attribute->getSource()->getOptionId($brand);
                    }

                    if ($attribute_option_id) {
                        # code...
                        $attribute_option_text = $attribute->getSource()->getOptionText($attribute_option_id);                        
                    }

                    $sku= $productInformation->getSku();
                    $name=$productInformation->getName();
                    $price=$productInformation->getPrice();
                    
                    $model = $this->_customfeedFactory->create();
                    
                    $stockdata=$this->getStockItem($productInformation->getId());
                    
                    $model->setData("sku",$sku);
                    $model->setData("name",$name);
                    $model->setData("price",$price);
                    $model->setData("inventory",$stockdata['qty']);
                    $model->setData("attribute_code",$attribute_code);
                    $model->setData("attribute_option_id",$attribute_option_id);
                    $model->setData("attribute_option_text",$attribute_option_text);

                    $model->save();
                    $error=0;

                    $html1.=$sku.",";
                }
                catch(\Magento\Framework\Exception\NoSuchEntityException $e)
                {
                    $error1=1;
                    $html.=$sku.",";
                    continue;
                }
                
            }
        }
        

        # update existing sku's
        $this->updateCustomFeedTable();
             
        $this->messageManager->addSuccess($html1);
        $this->messageManager->addError($html);  
        $resultForward = $this->resultForwardFactory->create();
        
        return $resultForward->forward('index');   
    }

    public function updateCustomFeedTable()
    {

        $customFeedFactory = $this->customeFeedFactory();      

        $customFeedSkuCollection = $customFeedFactory->getCollection()->addFieldToSelect('sku');
        $customFeedSkuArray = [];
        foreach($customFeedSkuCollection as $customFeed){
            array_push($customFeedSkuArray,$customFeed['sku']);
        }

        foreach ($customFeedSkuArray as $customFeedSku) {

            # load a row from custom feed
            $customFeedRow = $customFeedFactory->load($customFeedSku,'sku');

            # Magento Default product table 
            $productDetails = $this->getProductIfAvailable($customFeedRow->getSku());

            if ($productDetails !== false) {

                $category_ids = implode(',', $productDetails->getCategoryIds());

                # Stock information
                $stockInfo = $this->stockInformation($productDetails->getId());

                # Update custom table with the magento product tabe data            
                $customFeedRow->setData('inventory', $stockInfo->getQty());
                $customFeedRow->setData('price', $productDetails->getPrice());
                $customFeedRow->setData('category_ids', $category_ids);
                $customFeedRow->setData('attribute_stock', $stockInfo->getIsInStock());
                $customFeedRow->save();
                
            }

        }
    }

}

        