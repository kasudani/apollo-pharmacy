<?php
namespace Retail\Customfeed\Controller\Adminhtml\Customfeed;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;


class Savenotes extends \Magento\Backend\App\Action
{
   protected $_customfeedFactory;
    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context,
        \Retail\Customfeed\Model\CustomfeedFactory $CustomfeedFactory)
    {

        $this->_customfeedFactory = $CustomfeedFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
    	 try{
    	 $params=$this->getRequest()->getParams();
       $model = $this->_customfeedFactory->create();
        $model->load($params['id'][0],'id');
         $model->setData("notes",$params['note']);
         $model->save();
          echo 1;
     }
     catch(Exception $e){
     	 echo $e->getMessage(); 
     }
    }
}