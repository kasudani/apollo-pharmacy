<?php

namespace Retail\Customfeed\Controller\Adminhtml\customfeed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPagee;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Retail_Customfeed::customfeed');
        $resultPage->addBreadcrumb(__('Retail'), __('Retail'));

         $params=$this->getRequest()->getParams(); 
         if(array_key_exists("id",$params))
                {
                if($params['id']==1)
                {
        $resultPage->addBreadcrumb(__('Manage item'), __('Approved Products'));
        $resultPage->getConfig()->getTitle()->prepend(__('Approved Products'));
                }
                if($params['id']==2)
                {
        $resultPage->addBreadcrumb(__('Manage item'), __('Disapproved Products'));
        $resultPage->getConfig()->getTitle()->prepend(__('Disapproved Products'));
                }
            }
            else{

        $resultPage->addBreadcrumb(__('Manage item'), __('All Products'));
        $resultPage->getConfig()->getTitle()->prepend(__('All Products'));
            }
        return $resultPage;
    }
}
?>