<?php
namespace Retail\Customfeed\Controller\Adminhtml\customfeed;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassApprove extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $itemIds = $this->getRequest()->getParam('customfeed');
      
        if (!is_array($itemIds) || empty($itemIds)) {
            $this->messageManager->addError(__('Please select item(s).'));
        } else {
            try {
                foreach ($itemIds as $itemId) {
                    $post = $this->_objectManager->get('Retail\Customfeed\Model\Customfeed')->load($itemId);
                    $post->setData("status","Approved");
                      $post->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($itemIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('customfeed/*/index');
    }
}