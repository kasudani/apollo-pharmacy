<?php
namespace Retail\Customfeed\Model\ResourceModel;

class Customfeed extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('feed_report', 'id');
    }
}
?>