<?php

namespace Retail\Customfeed\Model\ResourceModel\Customfeed;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\Customfeed\Model\Customfeed', 'Retail\Customfeed\Model\ResourceModel\Customfeed');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
?>