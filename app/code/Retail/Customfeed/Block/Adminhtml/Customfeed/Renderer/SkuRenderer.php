<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Retail\Customfeed\Block\Adminhtml\Customfeed\Renderer;

use Magento\Framework\DataObject;

class SkuRenderer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_product;
    protected $_productFactory;
    protected $_productCollectionFactory;
    protected $_storeManager;
    protected $_localeCurrency;
    protected $_customFeed;
    protected $_stockRegistry;

    public function __construct(
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\CurrencyInterface $currencyInterface,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Retail\Customfeed\Model\CustomfeedFactory $customfeedFactory
    )
    {
        $this->_product = $product;
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_localeCurrency = $currencyInterface;
        $this->_customFeed = $customfeedFactory->create();
        $this->_stockRegistry = $stockRegistry;
        
    }

    public function render(DataObject $row)
    {
        # fetch sku
        $product_sku = $row->getData($this->getColumn()->getIndex());
        return $product_sku;
    }

    public function loadProductById($product_id)
    {
        $product = $this->_product->load($product_id);
        return $product;
    }

    public function loadProductBySku($sku)
    {
        $product = $this->_product->loadByAttribute('sku', $sku);
        return $product;
    }

    public function stockInformation($product_id)
    {
        return $this->_stockRegistry->getStockItem($product_id);
    }
    
}
