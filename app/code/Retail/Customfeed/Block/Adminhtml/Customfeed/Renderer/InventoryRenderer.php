<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Retail\Customfeed\Block\Adminhtml\Customfeed\Renderer;

use Magento\Framework\DataObject;

class InventoryRenderer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_product;
    protected $_productFactory;
    protected $_productCollectionFactory;
    protected $_storeManager;
    protected $_localeCurrency;

    public function __construct(
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\CurrencyInterface $currencyInterface
    )
    {
        $this->_product = $product;
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_localeCurrency = $currencyInterface;
        
    }

    public function render(DataObject $row)
    {
        $invetory = $row->getData($this->getColumn()->getIndex());
        return sprintf('%0.0f', $invetory);

    }

    public function loadProduct($product_id)
    {
        $product = $this->_product->load($product_id);
        return $product;
    }

    public function getcurrencySymbol(){
        $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $currencySymbol = $this->_localeCurrency->getCurrency($currencyCode)->getSymbol();
        return $currencySymbol;
    }
}
