<?php 
namespace Retail\Customfeed\Block\Adminhtml\Customfeed\Edit\Tab\Renderer; 
use Magento\Framework\DataObject;
 class Savenotes extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
 protected $_customerRepositoryInterface;
    
    public function __construct(
	\Magento\Customer\Model\Customer $customerRepositoryInterface
    ) {
	 $this->_customerRepositoryInterface = $customerRepositoryInterface;
     }
    public function render(DataObject $row)
    {
        $productId = $row->getData('id');  ?>

        <div id="main-feed<?php echo $row->getData('id');?>" class="main-feed">
        <?php if($row->getData('notes')!=null){
        ?>
         <input type="text" name="noted<?php echo $row->getData('id'); ?>" id="noted<?php echo $row->getData('id'); ?>" value="<?php echo $row->getData('notes'); ?>"  class="noted">
     </br>
       <button type="button" style="display:none;" name="delete<?php echo $row->getData('id'); ?>" id="delete<?php echo $row->getData('id'); ?>" class="action- scalable primary deletefeed">Reset</button>
         
         <button type="button" style="display:none;" name="button<?php echo $row->getData('id'); ?>" id="button<?php echo $row->getData('id'); ?>" class="action- scalable primary savefeed">Save</button>
         <?php } 
         else{ ?>

         <input type="text" name="noted<?php echo $row->getData('id'); ?>" id="noted<?php echo $row->getData('id'); ?>" value=""  class="noted">
         </br>
         <button type="button" style="display:none;" name="button<?php echo $row->getData('id'); ?>" id="button<?php echo $row->getData('id'); ?>" class="action- scalable primary savefeed">Save</button>
         <button type="button" style="display:none;" name="delete<?php echo $row->getData('id'); ?>" id="delete<?php echo $row->getData('id'); ?>" class="action- scalable primary deletefeed">Reset</button>

         <?php }
         ?> 
       </div>
      <?php   
    }
}