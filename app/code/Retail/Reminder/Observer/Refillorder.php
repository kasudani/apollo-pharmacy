<?php
namespace Retail\Reminder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;

class Refillorder implements \Magento\Framework\Event\ObserverInterface
{
    protected $order;
    protected $_moduleFactory;
    protected $_coreSession;
    protected $_exampleFactory;
    protected $_reminderFactory;    
    protected $_session;

    public function __construct(
      \Magento\Sales\Model\Order $order, 
      \Magento\Customer\Model\Session $session, 
      \Magento\Framework\Session\SessionManagerInterface $coreSession, 
      \Retail\RefillReminder\Model\ReminderFactory $cu
    )
    {
        
        $this->_reminderFactory = $cu;
        $this->_coreSession     = $coreSession;
        $this->_session         = $session;
        $this->order            = $order;
    }
    public function unsRefillReminder()
    {
        $this->_coreSession->start();
        $this->_coreSession->unsRefillReminder();
    }
    public function setRefillReminder($value)
    {
        $this->_coreSession->start();
        $this->_coreSession->setRefillReminder($value);
    }
    public function getRefillReminder()
    {
        $this->_coreSession->start();
        return $this->_coreSession->getRefillReminder();
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        $orderId         = $observer->getEvent()->getOrderIds();
        $order           = $this->order->load($orderId);
        $shippingAddress = $order->getShippingAddress();
        
        $itemCollection = $order->getItemsCollection();
        $model          = $this->_reminderFactory->create();
        
        
        foreach ($itemCollection as $item) {
            $product_id = $item->getProductId();
            $collection = $model->getCollection();
            
            $collection->addFieldToFilter('product_id', $product_id);
            $collection->addFieldToFilter('customer_id', $this->_session->getCustomer()->getId());
            $collection->addFieldToFilter('is_in_cart', 1);
            
            $res = $collection->getData();
            if (!empty($res)) {
                
                if ($product_id == $res[0]['product_id']) {
                    $model->load($res[0]['id']);
                    $flag = $model->getData('flag');
                    if ($model->getData('start_date') == null || $model->getData('start_date') == "") {
                        
                        $model->setData('start_date', date('Y-m-d'));
                        
                    }
                    switch ($flag) {
                        case null:
                            
                            $frequency = $model->getData('frequency');
                            $notify    = $frequency - 5;
                            $new_date  = date('Y-m-d', strtotime('+ ' . $notify . ' days'));
                            $model->setData('notify_date', $new_date);
                            $flag = 5;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        
                        case 5:
                            
                            //5 days before
                            $notify_date = $model->getData('notify_date');
                            $frequency   = $model->getData('frequency');
                            $new_date    = date('Y-m-d', strtotime($notify_date . ' + 2 days'));
                            $model->setData('notify_date', $new_date);
                            $flag = 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        case 0:
                            
                            //5 days before
                            $frequency = $model->getData('frequency');
                            $notify    = $frequency - 5;
                            $new_date  = date('Y-m-d', strtotime('+ ' . $notify . ' days'));
                            $model->setData('notify_date', $new_date);
                            $flag = $flag + 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        case 1:
                            //3 days before order
                            $notify_date = $model->getData('notify_date');
                            $frequency   = $model->getData('frequency');
                            
                            $new_date = date('Y-m-d', strtotime($notify_date . ' + 2 days'));
                            $model->setData("notify_date", $new_date);
                            $flag = $flag + 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        case 2:
                            //2 days before order
                            $notify_date = $model->getData('notify_date');
                            $new_date    = date('Y-m-d', strtotime($notify_date . ' + 1 days'));
                            $model->setData("notify_date", $new_date);
                            $flag = $flag + 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        
                        case 3:
                            
                            //1 day before order
                            $notify_date = $model->getData('notify_date');
                            
                            $new_date = date('Y-m-d', strtotime($notify_date . ' + 1 days'));
                            $model->setData("notify_date", $new_date);
                            $flag = $flag + 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                        case 4:
                            //order day
                            $notify_date = $model->getData('notify_date');
                            $frequency   = $model->getData('frequency');
                            $notify      = $frequency - 5;
                            $new_date    = date('Y-m-d', strtotime('+ ' . $notify . ' days'));
                            $model->setData("notify_date", $new_date);
                            $flag = 1;
                            $model->setData("flag", $flag);
                            $model->save();
                            break;
                    }
                    
                    $model->setData('shipping_name', $shippingAddress->getData('firstname') . " " . $shippingAddress->getData('lastname'));
                    $model->save();
                }
                
                
            }
            
            
        }
        $this->unsRefillReminder();
    }
}