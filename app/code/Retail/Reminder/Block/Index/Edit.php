<?php

namespace Retail\Reminder\Block\Index;


class Edit extends \Magento\Framework\View\Element\Template {

protected $_session;
 protected $_addressFactory;
protected $_productRepository; 
protected $_reminderFactory;
protected $_urlInterface;
protected $addressRepository;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
	\Magento\Customer\Model\Session $session, 
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	 \Magento\Framework\UrlInterface $urlInterface,  
	 \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
	 \Magento\Customer\Model\AddressFactory $addressFactory,
	array $data = []) {
        
    $this->_session = $session;
	$this->_addressFactory = $addressFactory;
    $this->_reminderFactory = $cu;
	 $this->_productRepository = $productRepository;
	 $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);

    }
	 public function getmyUrl()
	 {
		 return $this->_urlInterface->getBaseUrl();
	 }
     public function getIdFromSku($id)
	 {
		   	
       $product  = $this->_productRepository->getById($id);
	    return $product->getSku();
	 }
	  public function getNewData($id)
	  {
		   $shippingAddress = $this->_addressFactory->create()->load($id);
		   return $shippingAddress;
	  }
    
    public function getCustomData()
    {	
         $params= $this->getRequest()->getParams();
		 	  

		  $model=$this->_reminderFactory->create();
		   $model->load($params['id'],'id');
		   return $model->getData(); 
		 			
    }

}