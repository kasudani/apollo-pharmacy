<?php

namespace Retail\Reminder\Block\Index;


class Index extends \Magento\Framework\View\Element\Template {

protected $_session;
protected $_productRepository; 
protected $_reminderFactory;
protected $_urlInterface;
protected $_customerSession;
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
	\Magento\Customer\Model\Session $session, 
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
	 \Magento\Framework\UrlInterface $urlInterface,
	 \Magento\Customer\Model\SessionFactory $customerSession,  
	array $data = []) {
         
    $this->_session = $session;
	 
         $this->_customerSession = $customerSession->create();
    $this->_reminderFactory = $cu;
	 $this->_productRepository = $productRepository;
	 $this->_urlInterface = $urlInterface;
        parent::__construct($context, $data);

    }
	 public function getmyUrl()
	 {
		 return $this->_urlInterface->getBaseUrl();
	 }
     public function getIdFromSku($id)
	 {
		   	
       $product  = $this->_productRepository->getById($id);
	    return $product->getSku();
	 }
	  
    
    public function getCustomData()
	{

if ($this->_customerSession->isLoggedIn()) {


       $result=$this->getRequest()->getParams();
        
        $model = $this->_reminderFactory->create();	
	        $collection=$model->getCollection(); 
			$collection->addFieldToFilter('customer_id',$this->_customerSession->getCustomer()->getId());
			
			$collection->addFieldToFilter('is_in_cart',1);

			$collection->addFieldToFilter('flag', ['neq' => '0']);
		  return $collection->getData();

    	 }
		 else{
		    echo "Need to Login First";
		 }
    }

}