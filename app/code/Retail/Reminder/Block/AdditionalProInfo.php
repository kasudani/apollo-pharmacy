<?php 
namespace Retail\Reminder\Block;
 
/**
 * AdditionalProInfo
 */
class AdditionalProInfo extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
    */
	
            protected $_reminderFactory;
            protected $coreSession;	
            protected $_customerSession;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Session\SessionManagerInterface $coreSession,
	\Magento\Customer\Model\Session $session,
    \Magento\Catalog\Model\ProductRepository $productRepository,
    \Magento\Customer\Model\SessionFactory $customerSession,
        array $data = []
    ) {
         $this->_customerSession = $customerSession->create();
		     $this->_productRepository = $productRepository;
    $this->_session = $session;
    $this->_reminderFactory = $cu;
		$this->_coreSession = $coreSession; 
        parent::__construct($context, $data);
    }

     
    public function getProd($id)
            {
                return $this->_productRepository->getById($id);
            }
        	/*  public function setRefillReminder($value)
        	{
            $this->_coreSession->start();
            $this->_coreSession->setRefillReminder($value);
             }
        	 public function getRefillReminder()
        	 {
            $this->_coreSession->start();
            return $this->_coreSession->getRefillReminder();
             }*/
    public function isCustLoggedin()
     {
         if($this->_customerSession->isLoggedIn()){
         return $this->_customerSession->isLoggedIn();
         }
          else{
               return 0;
          }
     }
    public function getAdditionalData($id)
    {
		 
        $model = $this->_reminderFactory->create();	
	        $collection=$model->getCollection(); 
    if ($this->_customerSession->isLoggedIn()) {

        
        $model = $this->_reminderFactory->create();	
	        $collection=$model->getCollection(); 
			$collection->addFieldToFilter('customer_id',$this->_customerSession->getCustomer()->getId());
			
			$collection->addFieldToFilter('product_id',$id);
			$collection->addFieldToFilter('is_in_cart',1);
		  return $collection->getData();

    	 }
    }
}