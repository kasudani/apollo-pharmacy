<?php

namespace Retail\Reminder\Controller\Index;

class Delete extends \Magento\Framework\App\Action\Action
{
protected $_session;
protected $_urlInterface;

    protected $_reminderFactory; 
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
	\Magento\Framework\UrlInterface $urlInterface,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Message\ManagerInterface $messageManager
) {
    $this->_session = $session;
    $this->_reminderFactory = $cu;
	 $this->_messageManager = $messageManager;
	$this->_urlInterface = $urlInterface;
    parent::__construct($context);

  }
    public function execute()
    {
		  $params=$this->getRequest()->getParams();
		 try{
      
        $model = $this->_reminderFactory->create();	
			$model->load($params['id'],'id');	
			$model->delete();
			
			 $this->_messageManager->addSuccess(__("Deleted Successfully"));
                   $this->_redirect('reminder/customer/refill');
		 }
		 catch(Exception $e)
		 {
			 
			 $this->_messageManager->addError(__("Deletion Failed"));
                   $this->_redirect('reminder/customer/refill');
		 }
    }

}