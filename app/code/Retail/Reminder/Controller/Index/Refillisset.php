<?php

namespace Retail\Reminder\Controller\Index;

ob_start();
class Refillisset extends \Magento\Framework\App\Action\Action
{
protected $_session;

    protected $_reminderFactory; 
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Session\SessionManagerInterface $coreSession
) {
	
		$this->_coreSession = $coreSession; 
    $this->_session = $session;
    $this->_reminderFactory = $cu;
    parent::__construct($context);

  }
    public function execute()
    {
    	 if($this->_session->isLoggedIn()){


       $result=$this->getRequest()->getParams();
	       
	    
       try
         {
        $model = $this->_reminderFactory->create();
              $collection=$model->getCollection();
			  $collection->addFieldToFilter('product_id',$result['product_id']);
			  
			  $collection->addFieldToFilter('customer_id',$this->_session->getCustomer()->getId());
			   if(empty($collection->getData())){
		        
                               echo 0;
	              }
	    else{
			      echo 1;
		  }
	   }
	      catch(\Exception $e){
			  echo $e->getMessage();
		 }
	       
          
    	 }
		 
		 
    }

}