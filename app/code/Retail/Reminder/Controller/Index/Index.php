<?php

namespace Retail\Reminder\Controller\Index;

ob_start();
class Index extends \Magento\Framework\App\Action\Action
{
protected $_session;

    protected $_reminderFactory; 
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\Session\SessionManagerInterface $coreSession
) {
	
		$this->_coreSession = $coreSession; 
    $this->_session = $session;
    $this->_reminderFactory = $cu;
    parent::__construct($context);

  }
  public function setRefillReminder($value)
	{
    $this->_coreSession->start();
    $this->_coreSession->setRefillReminder($value);
     }
	 public function getRefillReminder()
	 {
    $this->_coreSession->start();
    return $this->_coreSession->getRefillReminder();
     }
    public function execute()
    {
    	 if($this->_session->isLoggedIn()){


       $result=$this->getRequest()->getParams();
	       
	    
       try{
        $model = $this->_reminderFactory->create();
              $collection=$model->getCollection();
			  $collection->addFieldToFilter('product_id',$result['product_id']);
			  
			  $collection->addFieldToFilter('customer_id',$this->_session->getCustomer()->getId());
			   if(empty($collection->getData())){
		        
                                $model->setData('product_id',$result['product_id']);
                                $model->setData('customer_id',$this->_session->getCustomer()->getId());
                                $model->setData('quantity',$result['qty']);
                                $model->setData('frequency',$result['fre']);
                                 $model->setData('start_date',null);
                                 $model->setData('notify_date',null);
                                $model->setData('flag',null);/* 
								$model->setData('start_date',date("Y-m-d"));
								$model->setData('notify_date',date('Y-m-d', strtotime("+".$result['fre']." days"))); */
								 if(array_key_exists('is_in_cart', $result))
								 {
									 $model->setData('is_in_cart',1); 
								 }
							   
                                $model->save();
								
								
	   }
	    else{
			
			           $res=$collection->getData();
					   $id=$res[0]['id'];
					   
						$model->load($id,'id');
						 $model->setData('product_id',$result['product_id']);
                                $model->setData('customer_id',$this->_session->getCustomer()->getId());
                                $model->setData('quantity',$result['qty']);
                                $model->setData('frequency',$result['fre']);
                                $model->setData('flag',0);
                                $model->setData('start_date',null);
                                $model->setData('notify_date',null);
								/* $model->setData('start_date',date("Y-m-d"));
								$model->setData('notify_date',date('Y-m-d', strtotime("+".$result['fre']." days"))); */
								
							    if(array_key_exists('is_in_cart', $result))
								 {
									 $model->setData('is_in_cart',1); 
								 }
                                $model->save();
								
								
		}
	   }
	      catch(\Exception $e){
			  echo $e->getMessage();
		 }
	       
          
    	 }
		 
		 
    }

}