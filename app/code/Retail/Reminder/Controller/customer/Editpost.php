<?php

namespace Retail\Reminder\Controller\Customer;

class Editpost extends \Magento\Framework\App\Action\Action
{
protected $_session;

    protected $_reminderFactory; 
	protected $_urlInterface;
	protected $_messageManager;
	protected $resultRedirect;
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\UrlInterface $urlInterface,
	\Magento\Framework\Message\ManagerInterface $messageManager,
	\Magento\Framework\Controller\ResultFactory $result
) {
	$this->_messageManager = $messageManager;
	  $this->_urlInterface = $urlInterface;
    $this->_session = $session;
    $this->_reminderFactory = $cu;
	$this->_urlInterface = $urlInterface;
    parent::__construct($context);

  }
	  public function getmyUrl()
		 {
			 return $this->_urlInterface->getBaseUrl();
		 }
    public function execute()
    {      
	          
		    $params=$this->getRequest()->getParams();
			 
			 try{
			 $model=$this->_reminderFactory->create();
			  $model->load($params['id'],'id');
			  $model->setData('quantity',$params['quantity']);
			  
			  $model->setData('frequency',$params['frequency']);
              $model->setData('start_date',date('Y-m-d'));
               $notify=$params['frequency']-5;
                $new_date= date('Y-m-d', strtotime('+ '.$notify. ' days'));
              $model->setData('notify_date',$new_date);
              $model->setData('flag',1);
			   $model->save();
			 $this->_messageManager->addSuccess(__("Edited Successfully"));
                   $this->_redirect('reminder/customer/refill');
				   
			 }
			 catch(Exception $e)
			 {
				 
			 $this->_messageManager->addError(__("Error Message"));
			 $this->_redirect('reminder/customer/refill');
			 }
    }

}