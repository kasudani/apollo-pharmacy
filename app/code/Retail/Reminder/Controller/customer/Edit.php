<?php

namespace Retail\Reminder\Controller\Customer;

class Edit extends \Magento\Framework\App\Action\Action
{
protected $_session;

    protected $_reminderFactory; 
	protected $_urlInterface;
public function __construct(
		\Magento\Framework\App\Action\Context $context,
    \Magento\Customer\Model\Session $session,
    \Retail\RefillReminder\Model\ReminderFactory $cu,
	\Magento\Framework\UrlInterface $urlInterface
) {
	  $this->_urlInterface = $urlInterface;
    $this->_session = $session;
    $this->_reminderFactory = $cu;
    parent::__construct($context);

  }
	  public function getmyUrl()
		 {
			 return $this->_urlInterface->getBaseUrl();
		 }
    public function execute()
    {
		 if($this->_session->isLoggedIn()){

     
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Refill Reminder Edit'));
        $this->_view->renderLayout();
		
    	 }
		 
    }

}