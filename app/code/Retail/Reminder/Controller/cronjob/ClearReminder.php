<?php
namespace Retail\Reminder\Controller\Cronjob;
class ClearReminder extends \Magento\Framework\App\Action\Action
{
    protected $_messageManager;
    protected $cart;
    protected $product;
    protected $resultPageFactory;
    protected $customerRepositoryInterface;
    protected $quoteModel;
    protected $productRepository;
    protected $cartManagementInterface;
    protected $cartRepositoryInterface;
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context, 
       \Retail\RefillReminder\Model\ReminderFactory $cu,
        \Magento\Checkout\Model\Cart $cart) {
        parent::__construct($context);
		
			$this->_reminderFactory = $cu;
    }
    public function execute()
    {
		
        $model = $this->_reminderFactory->create();
              $collection=$model->getCollection();
              $collection->addFieldToFilter('notify_date', ['lt' => date('Y-m-d')]); 
               foreach($collection->getData() as $data)
               {
                    $model->load($data['id']); 
                    $model->delete();
               }
  }
 }	 
