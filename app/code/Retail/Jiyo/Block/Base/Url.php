<?php

namespace Retail\Jiyo\Block\Base;

class Url extends \Magento\Framework\View\Element\Template
{
    public $_storeManager; 

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_storeManager=$storeManager;
    }
    public function siteUrl()
    {
     $base_url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
     return $base_url;
    }
    public function currentUrl(){

     $base_url = $this->_storeManager->getStore()->getCurrentUrl(false);
     return $base_url;
     }

}
