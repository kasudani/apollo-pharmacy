<?php
namespace Retail\RefillReminder\Model;

class Reminder extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\RefillReminder\Model\ResourceModel\Reminder');
    }
}
