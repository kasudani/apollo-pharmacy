<?php
namespace Retail\RefillReminder\Block\Adminhtml\Reminder\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('reminder_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Reminder Information'));
    }
}