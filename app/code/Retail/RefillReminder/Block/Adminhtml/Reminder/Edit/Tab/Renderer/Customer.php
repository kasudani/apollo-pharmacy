<?php 
namespace Retail\RefillReminder\Block\Adminhtml\Reminder\Edit\Tab\Renderer; 
use Magento\Framework\DataObject;
 class Customer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
 protected $_customerRepositoryInterface;
    
    public function __construct(
	\Magento\Customer\Model\Customer $customerRepositoryInterface
    ) {
	 $this->_customerRepositoryInterface = $customerRepositoryInterface;
     }
 
    
    public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
    public function render(DataObject $row)
    {
        $customerId = $row->getData('customer_id');
          $customer = $this->_customerRepositoryInterface->load($customerId);
		   return $customer->getName();
    }
}