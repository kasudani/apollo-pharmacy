<?php
namespace Retail\Subscribe\Plugin\Model;

class Config
{
    /**
     * Adding custom options and changing labels
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param [] $options
     * @return []
     */
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
        //Remove default sorting options
        unset($options['position']);
        //unset($options['price']);
      //  unset($options['name']);

        //Change label of default sorting options if needed
        //$options['position'] = __('Relevance');

        //New sorting options

        $options['new_coupon'] = __('New Coupon');

        $options['old_coupon'] = __('Old Coupon');

        $options['most_used_coupon'] = __('Most Used Coupon');

        $options['discount_percent'] = __('Discount Percent');


        $options['expiring_coupon'] = __('Expiring Coupon');
        // $options['price_desc'] = __('Price High - Low');
        // $options['price_asc'] = __('Price Low - High');
        $options['rating_summary'] = __('Most Popular');
        return $options;
    }
}