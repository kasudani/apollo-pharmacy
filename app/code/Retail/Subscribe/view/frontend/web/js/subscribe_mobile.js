require([ 'jquery', 'jquery/ui'], function($){

  function jsfunction(my_val){
    var key = _.findKey(Apo.month, function(v) {
      return v == my_val;
    });

    var discount_value = Apo.discount[key];
    jQuery(".per_discount").text(discount_value+ "%");
    jQuery(".per_discounts").text(discount_value+ "%");
    jQuery("#discount").val(discount_value);
    jQuery("#period").val(my_val);
  }

  jQuery(document).ready(function(){ 
    
    /*jQuery(document).on('change','#period',function(){
      var my_val = this.value;
      jsfunction(my_val);
    });*/
    
    jQuery(document).on('click','radio',function(){
      var sub_id =  $(this).val();
      console.log(sub_id);
      var sub_period =  $('input[name="period_'+sub_id+'"]').val();
    });

    jQuery( ".periods" ).each(function( index ) {
      var id = $(this).attr('id');
      var value = $(this).attr('value');
      jQuery('input[name='+id+'][value='+value +']').click();
    });

    jQuery(document).on('click','.select-subscription-period',function(){
      var my_val = jQuery(this).attr('data-value');
      jQuery('.select-subscription-period').removeClass('active');
      jQuery(this).addClass('active');
      jsfunction(my_val);
    });

    /*showing subscribe method based on product */
    var min_qty = "";
    var max_qty = "";
    var login_url = "";
    jQuery(document).on('change','input[name="subscribe"]',function(){
        var sub = $("input[name='subscribe']:checked").val();
        if(sub == 'ss'){
          var cust_flag = $("#cust_flag").val();
          var login_url = $("#login_url").val();
          if(cust_flag == ""){
            window.location = login_url;
          } else {
            var min_qty = $("#min_qty").val();
            var max_qty = $("#max_qty").val();
            jQuery(".subscribe_period").show();
            jQuery("#subscribe_btn").show();
            jQuery("#qty").val(min_qty);
            //jQuery("#product-addtocart-button").hide();
          }
        } else if(sub == 'otp') {
          jQuery(".subscribe_period").hide();
          jQuery("#subscribe_btn").hide();
          jQuery("#product-addtocart-button").show();
          jQuery("#qty").val(1);
          jQuery("#alert_msg").hide();
        }
    });

    /*subscription jquery code for my subscribed products page*/
    jQuery(document).on('click','.jq_subscribe_btn',function(){
      var cont_url =  $("#cont_url").val();
      var sub_id   =  $(this).attr('data-id');
      var sub_period =  $('input[name="period_'+sub_id+'"]:checked').val();
      var discount =  $('#save_'+sub_id+'_'+sub_period).attr('data-value');
      $.ajax({
        showLoader: true,
        url: cont_url,
        data: {"id":sub_id,"sub_period":sub_period,"discount":discount},
        type: "POST"
        }).done(function (data) {
          console.log(data);
          if(data == 'updated'){
           jQuery("#alert_msg").show();
           jQuery("#alert_msg").html('<span class="success-message no-padding-l">Subscription has been updated</span>');
           setTimeout(function() {
              jQuery('#alert_msg').hide();
              window.location.reload(true); 
            }, 3000);
          } else {
            jQuery("#alert_msg").hide();
          }
        });
    });

    /*subscription jquery code for project description page*/
    jQuery(document).on('click','#subscribe_btn',function(){
     var cont_url =  $("#cont_url").val();
     var cust_id =  $("#cust_id").val();
     var prod_id =   $("#prod_id").val();
     var cust_flag = $("#cust_flag").val();
     var min_qty =   $("#min_qty").val();
     var max_qty =   $("#max_qty").val();
     var discount =  $("#discount").val();
     var qty =       $("#qty").val();
     var sub_period = $("#period").val();

     if(parseInt(qty) < parseInt(min_qty) || parseInt(qty) > parseInt(max_qty)){
        
        jQuery("#alert_msg").show();
        jQuery("#alert_msg").html('Required quantity is '+ min_qty + '-'+ max_qty);

      } else {
        $.ajax({
        showLoader: true,
        url: cont_url,
        data: {"product_id":prod_id,"customer_id":cust_id,"sub_period":sub_period,"discount":discount,"qty":qty,"flag":'0'},
        type: "POST"
        }).done(function (data) {
          console.log(data);
          if(data == 'already'){
           jQuery("#alert_msg").show();
           jQuery("#alert_msg").html('<div class="success-message text-center" style="display: block; margin-left: -20px;">Subscription has been updated</div>');
           setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else if(data == 'alreadys'){
            jQuery("#alert_msg").show();
            jQuery("#alert_msg").html('<div class="error-message text-center" style="display: block; margin-left: -20px;">You have already subscribed for this product.</div>');
            setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else if(data == 'submitted'){
            jQuery("#alert_msg").show();
            jQuery("#alert_msg").html('<div class="success-message text-center" style="display: block; margin-left: -20px;">You have subscribed for this product.</div>');
            setTimeout(function() {
              jQuery('#alert_msg').hide();
            }, 3000);
          } else {
            jQuery("#alert_msg").hide();
          }
        });
      }
    });
  });
});