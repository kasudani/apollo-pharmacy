require(['jquery', 'jquery/ui'], function($) {

    /* Quantity per discount start */
    var closestParentElement, priceContainerElement, element_qty, element_sku;
    jQuery(document).on('keyup', 'input.item-qty.cart-item-qty', function() {

        element_qty = jQuery(this).val();
        element_sku = jQuery(this).data('cart-item-id');

        if(jQuery(this).val() == '' || jQuery(this).val() <= 0) {
            return false;
        }

        if(jQuery(this).data('cart-item-id') == "HEA0265") {
            return false;
        }
        
        closestParentElement = jQuery(this).closest('div.product-item-details');
        priceContainerElement = closestParentElement.find('div.price-container');

        // clear message
        closestParentElement.find('.qty-minicart-discount-message').html("");

        // call ajax
        $.ajax({
            showLoader: true,
            url: window.location.origin+'/subscribe/discount/calculate',
            data: {
                "product_id": '',
                "product_qty": element_qty,
                "product_sku": element_sku
            }
        }).done(function(data) {

            if(data.discounterPrice !== '') {

                discount_applied_msg = new String('You save '+data.appliedDiscount+' % on this product with '+data.qty+' quantity.');
                priceContainerElement.find('.price').html("₹"+data.discounterPrice);
                closestParentElement.find('.qty-minicart-discount-message').html(discount_applied_msg);
            }
        });

    });
    /* Quantity per discount end */

    /* add to wishlist, show popup if not loggedin code starts here */
    jQuery('.towishlist').on('click', function(event) {
        var cust_flag = jQuery('#cust_flag').val();
        var guest_log = localStorage.getItem('guest_contact');
        if (cust_flag == "" || cust_flag == null) {
            if (guest_log == "" || guest_log == null) {
                jQuery(".authorization-link .social-login").trigger('click');
                jQuery('#guest_otp').hide();
                return false;
            } else {
                localStorage.removeItem('guest_contact');
                jQuery(".authorization-link .social-login").trigger('click');
                jQuery('#guest_otp').hide();
                return false;
            }

        }
    });
    /* add to wishlist, show popup if not loggedin code ends here */

    jQuery('#social-form-create #firstname, #social-form-create #lastname').on('keypress', function(event) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        var a = event.charCode;
        if (a == 0 || a == 32) {
            return true;
        } else if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    jQuery('#social-form-create #mobile_number, .login-otp #mobile, .guest_form_verify #mobile_no, .login-otp #otp, .guest_form_verify #my_check_otp').on('keypress', function(event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        var a = event.charCode;
        if (a == 0) {
            return true;
        } else if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    /*Create user form validation code ends here */

    /* setting expiry times for guest verification starts here */
    window.setInterval(function() {
        if (localStorage.getItem('guest_contact')) {
            var res = localStorage.getItem('guest_contact').split("-");
            var guest_mobile = res[0];
            var guest_time = res[1];
            var d3 = new Date();
            var d4 = new Date(guest_time);
            d4.setMinutes(d4.getMinutes() + 30);
            var dift = ((d3 - d4) / (60 * 1000));
            if (dift >= +0) {
                localStorage.removeItem('guest_contact');
            } else {}
        }
    }, 5 * 60 * 1000);
    /* setting expiry times for guest verification ends here */

    /* Tagalays Rating Coding starts here */
    var count = 0;
    var timIntrvl;

    function getRatingsHtml(low, high, count) {
        var html = '';
        html += '<span id="rating-changed" class="rating-changed" style="display:none;">yes</span>';
        html += '<input class="rating-changed-input" style="display:none;" value="yes"/>';
        html += '<div class="rating-summary" style="display: inline-block;margin-top: -5px;">';
        html += '<div class="rating-result" title="' + low + '% - ' + high + '%">';
        html += '<span style="width:' + high + '%; display: block !important; margin-top: -20px; overflow: hidden; float:left;" class="rating_orange_star"></span>';
        html += '</div>';
        html += '</div>';
        html += '<span class="count">' + count + '</span>';
        return html;
    }

    function getRatingsData() {

        jQuery('#tagalys-filter-custom_rating_tag_set').find('.tagalys-managed-list-link').each(function() {
            count = count + 1;
            var is_rating_text_changed_input = jQuery(this).find('.rating-changed-input').val();
            var is_rating_text_changed = jQuery(this).find('.rating-changed').text();
            var is_rating_text_changed2 = jQuery('#rating-changed').text();
            var rating_and_count = jQuery(this).text();
            var rating_htmlText = jQuery(this).html();
            var rating_count = jQuery(this).find('.count').text();
            var rating_htmlText_array = rating_htmlText.split('<');
            var rating = rating_htmlText_array[0];

            if (is_rating_text_changed2 === '' || is_rating_text_changed_input === undefined) {
                if (rating >= 0 && rating <= 20) {
                    var htmlData = getRatingsHtml(0, 20, rating_count);
                    jQuery(this).html(htmlData);
                } else if (rating >= 21 && rating <= 40) {
                    var htmlData = getRatingsHtml(0, 20, rating_count);
                    jQuery(this).html(htmlData);
                } else if (rating >= 41 && rating <= 60) {
                    var htmlData = getRatingsHtml(40, 60, rating_count);
                    jQuery(this).html(htmlData);
                } else if (rating >= 61 && rating <= 80) {
                    var htmlData = getRatingsHtml(60, 80, rating_count);
                    jQuery(this).html(htmlData);
                } else if (rating >= 81 && rating <= 100) {
                    var htmlData = getRatingsHtml(80, 100, rating_count);
                    jQuery(this).html(htmlData);
                } else {}
            } else {}

        });

    }
    /* Insert Ratings Star */
    jQuery('#tagalys-namespace').bind("DOMNodeInserted", function() {
        if (jQuery('#tagalys-filter-custom_rating_tag_set').length > 0) {
            getRatingsData()
        }
    });

    /* Change Selected Rating Text */
    jQuery('#tagalys-namespace').bind("DOMNodeInserted", function() {
        var selected_ratings_text;
        if (jQuery('.selected-filters').length > 0) {
            jQuery('.selected-filters').find('a.selected').each(function() {
                selected_ratings_text = jQuery(this).find('.tag').text();
                if (selected_ratings_text >= 0) {
                    if (selected_ratings_text >= 0 && selected_ratings_text <= 20) {
                        jQuery(this).find('.tag').text("1 Star");
                    } else if (selected_ratings_text >= 21 && selected_ratings_text <= 40) {
                        jQuery(this).find('.tag').text("2 Star");
                    } else if (selected_ratings_text >= 41 && selected_ratings_text <= 60) {
                        jQuery(this).find('.tag').text("3 Star");
                    } else if (selected_ratings_text >= 61 && selected_ratings_text <= 80) {
                        jQuery(this).find('.tag').text("4 Star");
                    } else if (selected_ratings_text >= 81 && selected_ratings_text <= 100) {
                        jQuery(this).find('.tag').text("5 Star");
                    } else {}
                }
            });
        }
    });
    /* Tagalays Rating Coding ends here */

    jQuery(document).ready(function() {

        /* Rating filter add label code starts here */
        var curr_url = jQuery('#curr_url').val();
        var bottom;
        var multi;
        var mul_split;
        var str_append="";
        if(curr_url.indexOf('rating=') != -1){
                bottom = curr_url.split("rating=", 2);
                multi  = bottom['1'];
                if(multi.indexOf(',') != -1){
                 mul_split = multi.split(",");
                 jQuery.each(mul_split, function( i, val ) {
                    if (i === (mul_split.length - 1)) {
                          str_append += val+' Star';
                      }else{
                          str_append += val+' Star, ';
                      }
                    });
                 jQuery('.filter-current .items li:last-child .filter-value').text(str_append);
                }else{
                    jQuery('.filter-current .items li:last-child .filter-value').text(multi + ' Star');
                }
            }
        /* Rating filter add label code ends here */

        var base_url = jQuery('#base_url').val();
        var cust_flag = jQuery('#cust_flag').val();
        if(cust_flag == ""){
            localStorage.removeItem('upload_presc');
        }
        if (localStorage.getItem('guest_contact')) {
            var res = localStorage.getItem('guest_contact').split("-");
            var guest_mobile = res[0];
            var guest_time = res[1];
            var d3 = new Date();
            var d4 = new Date(guest_time);
            d4.setMinutes(d4.getMinutes() + 30);
            var dift = ((d3 - d4) / (60 * 1000));
            if (dift >= +0) {
                localStorage.removeItem('guest_contact');
            } else {}
        }
        /*Saving guest mobile number in quote table coding starts here*/
        var guest_save_url = jQuery('#guest_save_url').val();
        var curr_url = jQuery('#curr_url').val();
        if (curr_url == base_url + 'checkout') {
            $.ajax({
                showLoader: true,
                url: guest_save_url,
                data: {
                    "guest_mobile": guest_mobile
                },
                type: "POST"
            }).done(function(data) {});
        }
        /*Saving guest mobile number in quote table coding ends here*/

        /* For Guest User, show list of previous prescriptions starts here */
        if (guest_mobile !== "" || guest_mobile !== null) {
            jQuery('#my_guest').val(guest_mobile);
        }
        if (cust_flag == "") {
            if (guest_mobile) {
                var guest_pharma_url = jQuery('#guest_pharma_url').val();
                $.ajax({
                    showLoader: true,
                    url: guest_pharma_url,
                    data: {
                        "guest_mobile": guest_mobile
                    },
                    type: "POST"
                }).done(function(data) {
                    jQuery(".my_guest_data").replaceWith(data);
                });
            } else {}
        }
        /* For Guest User, show list of previous prescriptions ends here */

        /* Recently viewed slider css starts here */
        jQuery(".block-viewed-products-grid .block-title strong").css("font-size", "24px");
        jQuery(".block-viewed-products-grid .block-title strong").css("color", "#007C9D");
        jQuery(".block-viewed-products-grid .block-title strong").css("text-transform", "uppercase");
        jQuery(".block-viewed-products-grid .block-title strong").css("text-decoration", "underline");
        jQuery(".block-viewed-products-grid .block-title strong").css("font-weight", "200");
        jQuery(".widget-viewed-grid li .product-item-info a .product-image-container").css("border", "1px solid #cccccc");
        /* Recently viewed slider css starts here */

        /* Upload prescripton file validation starts here */
        jQuery(document).on('change', '#hd_upload_prescription, .more_prescriptions input', function(event) {
            var fileExtension = ['jpeg', 'jpg', 'png', 'doc', 'docx', 'pdf'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $(this).css("box-shadow", "red 0px 0px 1px 1px");
                jQuery(".allowed_extensions").addClass("invalid_format");
                jQuery(".checkout-methods-items .checkout").attr("disabled", "disabled");
                jQuery(".store_pickup_home_delivery1 .btn2").attr("disabled", "disabled");
            } else {
                $(this).css("box-shadow", "0px");
                jQuery(".allowed_extensions").removeClass("invalid_format");
                jQuery(".checkout-methods-items .checkout").prop("disabled", false);
                jQuery(".store_pickup_home_delivery1 .btn2").prop("disabled", false);
            }
        });

        /* Upload prescripton file validation starts here */

        /* Clicking on choose prescription, restriction if customer not verified code starts here*/
        jQuery(document).on('click', '#hd_upload_prescription', function() {
            var sat_flag = true;
            if (cust_flag == "") {
                if (guest_mobile == "" || guest_mobile == null) {
                    jQuery(".social-login").trigger('click');
                    sat_flag = false;
                }
                return sat_flag;
            } else {

            }
        });
        jQuery(document).on('click', '.btn2', function() {
            var sat_flag1 = true;
            if (cust_flag == "") { /* customer is not logged in  */
                if (guest_mobile == "" || guest_mobile == null) {
                    jQuery(".social-login").trigger('click');
                    sat_flag1 = false;
                } else {
                    if (jQuery('#hd_upload_prescription').length) {
                        if (document.getElementById("hd_upload_prescription").files.length == 0) {
                            if (jQuery('.my_guest_data li').length == 0) {
                                sat_flag1 = false;
                                if (jQuery('.presc_error1').length == 0) {
                                    var msh = '<span class="presc_error1" style="color:red;font-size: 13px;">Please Choose File!</span>';
                                    jQuery(msh).insertAfter(".previous_prescriptions");
                                    jQuery('.presc_error1').delay(3000).fadeOut();
                                }
                            } else {
                                if (jQuery("#cart_home_delivery_form input:checkbox:checked").length > 0) {} else {
                                    sat_flag1 = false;
                                    if (jQuery('.presc_error1').length == 0) {
                                        var msh = '<span class="presc_error1" style="color:red;font-size: 13px;">Please Choose File!</span>';
                                        jQuery(msh).insertAfter(".previous_prescriptions");
                                        jQuery('.presc_error1').delay(3000).fadeOut();
                                    }
                                }
                            }
                        }
                    }
                }
            } else { /* customer is logged in  */
                if (jQuery('#hd_upload_prescription').length) {
                    if (document.getElementById("hd_upload_prescription").files.length == 0) {
                        if (jQuery('.my_guest_data li').length == 0) {
                            sat_flag1 = false;
                            if (jQuery('.presc_error1').length == 0) {
                                var msh = '<span class="presc_error1" style="color:red;font-size: 13px;">Please Choose File!</span>';
                                jQuery(msh).insertAfter(".previous_prescriptions");
                                jQuery('.presc_error1').delay(3000).fadeOut();
                            }
                        } else {
                            if (jQuery("#cart_home_delivery_form input:checkbox:checked").length > 0) {
                                var selted = jQuery("#cart_home_delivery_form input:checkbox:checked").next('label').text();
                                var tyui = selted.split('.').pop();
                                var fileExtension = ['jpeg', 'jpg', 'doc', 'png', 'docx', 'pdf'];
                                fileExtension.some(function(val, index) {
                                    if (tyui.trim() == val) {
                                        sat_flag1 = true;
                                        return true;
                                    } else {
                                        sat_flag1 = false;
                                        return false;
                                    }
                                });
                                if (sat_flag1 == false) {
                                    if (jQuery('.presc_error3').length == 0) {
                                        var msh = '<span class="presc_error3" style="color:red;font-size: 13px;">Please upload valid file.</span>';
                                        jQuery(msh).insertAfter(".previous_prescriptions");
                                        jQuery('.presc_error3').delay(3000).fadeOut();
                                    }
                                }
                            } else {
                                sat_flag1 = false;
                                if (jQuery('.presc_error1').length == 0) {
                                    var msh = '<span class="presc_error1" style="color:red;font-size: 13px;">Please Choose File!</span>';
                                    jQuery(msh).insertAfter(".previous_prescriptions");
                                    jQuery('.presc_error1').delay(3000).fadeOut();
                                }
                            }
                        }
                    }
                }
            }
            if(sat_flag1 == true){
               localStorage.setItem("upload_presc", "true");
            }else{
               localStorage.setItem("upload_presc", "false");
            }
            return sat_flag1;
        });
        jQuery(document).on('click', '.more_prescriptions input', function() {
            var sat_flag2 = true;
            if (cust_flag == "") {
                if (guest_mobile == "" || guest_mobile == null) {
                    jQuery(".social-login").trigger('click');
                    sat_flag2 = false;
                }
            }
            return sat_flag2;
        });
        /* Clicking on choose prescription, restriction if customer not verified code ends here*/

        /* Guest User logout functionality code starts here */
        jQuery(document).on('click', '.guest_logout', function() {
            var guest_delete_url = jQuery('#guest_delete_url').val();
            $.ajax({
                showLoader: false,
                url: guest_delete_url,
                data: {
                    "mo": ""
                },
                type: "POST"
            }).done(function(data) {
                if (data == 'delete') {
                    localStorage.removeItem('guest_contact');
                    window.location.href = base_url;
                }
            });
        });
        /* Guest User logout functionality code ends here */

        /* Blocking Proceed to checkout for not logged in customer from cart page code starts here */
        jQuery(document).on('click', '.checkout-methods-items .item .primary', function(evt) {
            evt.stopPropagation();
            if (cust_flag == "" || cust_flag == null) { /* here customer is not logged in */
                if (guest_mobile == "" || guest_mobile == null) {
                    jQuery(".social-login").trigger("click");
                } else {
                    var pharma_url = jQuery('#pharma_url').val();
                    $.ajax({
                        showLoader: false,
                        url: pharma_url,
                        data: {
                            "gu": ""
                        },
                        type: "POST"
                    }).done(function(data) {
                        if (data == 'false') {
                            var msh = '<span class="presc_error" style="color:red;font-size: 13px;">Please upload Prescription first</span>';
                            jQuery(".checkout-methods-items").prepend(msh);
                        } else {
                            window.location.href = base_url + "checkout";
                        }
                    });
                }
            } else { /* here customer is logged in */

                /* upload prescrption validation */
                if (jQuery('#hd_upload_prescription').length) {
                    if (document.getElementById("hd_upload_prescription").files.length == 0) {
                        if (jQuery('.my_guest_data li').length == 0) {
                            if (jQuery('.presc_error2').length == 0) {
                                var msh = '<span class="presc_error2" style="color:red;font-size: 13px;">Please upload prescription</span>';
                                jQuery(".checkout-methods-items").prepend(msh);
                                jQuery('.presc_error2').delay(3000).fadeOut();
                            }
                        } else {
                            if (jQuery("#cart_home_delivery_form input:checkbox:checked").length > 0) {
                                var selted1 = jQuery("#cart_home_delivery_form input:checkbox:checked").next('label').text()
                                var fileExtension = ['jpeg', 'jpg', 'png', 'doc', 'docx', 'pdf'];       
                                var first_half = selted1.split('.').pop();
                                fileExtension.some(function(val, index) {
                                    if (first_half.trim() == val) {
                                        sat_flag3 = true;
                                        return true;
                                    } else {
                                        sat_flag3 = false;
                                        return false;
                                    }
                                });
                                
                                if (sat_flag3 == false) {
                                    if (jQuery('.presc_error6').length == 0) {
                                        var msh = '<span class="presc_error6" style="color:red;font-size: 13px;">Please upload valid file.</span>';
                                        jQuery(".checkout-methods-items").prepend(msh);
                                        jQuery('.presc_error6').delay(3000).fadeOut();
                                    }
                                } else {
                                    if(localStorage.getItem('upload_presc') == 'true'){
                                     window.location.href = base_url + "checkout";   
                                    }
                                    else if(localStorage.getItem('upload_presc') == null || localStorage.getItem('upload_presc') == "" || localStorage.getItem('upload_presc') == 'false'){
                                        if (jQuery('.presc_erroro').length == 0) {
                                        var msh = '<span class="presc_erroro" style="color:red;font-size: 13px;">Please upload file first.</span>';
                                        jQuery(".checkout-methods-items").prepend(msh);
                                        jQuery('.presc_erroro').delay(3000).fadeOut();
                                    }
                                    }
                                }


                            } else {
                                if (jQuery('.presc_error2').length == 0) {
                                    var msh = '<span class="presc_error2" style="color:red;font-size: 13px;">Please upload prescription</span>';
                                    jQuery(".checkout-methods-items").prepend(msh);
                                    jQuery('.presc_error2').delay(3000).fadeOut();
                                }
                            }
                        }
                    } else {
                        if (jQuery("#cart_home_delivery_form input:checkbox:checked").length > 0) {
                            window.location.href = base_url + "checkout";
                        } else {
                            if (jQuery('.presc_error2').length == 0) {
                                var msh = '<span class="presc_error2" style="color:red;font-size: 13px;">Please upload prescription</span>';
                                jQuery(".checkout-methods-items").prepend(msh);
                            }
                        }
                    }
                } else {
                    window.location.href = base_url + "checkout";
                }
                /* upload prescrption validation */
            }
            var otp_telephone = jQuery('#co-shipping-form').find('input[name="telephone"]').val();
        });
        /* Blocking Proceed to checkout for not logged in customer from cart page code ends here */

        /* Login with otp, guest checkout coding starts here */

        // jQuery(document).on('click', '.authorization-link .social-login', function() {
        //     jQuery(".login-otp").hide();
        //     jQuery(".verify_otp").hide();
        //     jQuery('.guest_form_verify').hide();
        //     jQuery('.main_login_otp').show();
        //     jQuery(".social-login-customer-authentication").show();
        //     jQuery(".fieldset.login").show();
        //     jQuery('#log_otp').show();
        //     jQuery('.social-login-authentication-channel').show();
        // });

        jQuery(document).on('click', '.verify-back-btn', function() {
            jQuery(".verify_otp").hide();
            jQuery('#log_otp').show();
            jQuery(".main_login_otp").show();
            jQuery('.login').show();
        });
        jQuery(document).on('click', '#guest_otp', function() {
            jQuery(".verify_otp").show();
            jQuery(".guest_form_verify").show();
            jQuery('.login-otp').hide();
            jQuery(".main_login_otp").hide();
        });
        jQuery(document).on('click', '#get_my_otp', function(evt) {
            var otp_telephone = jQuery('#mobile_no').val();
            var my_otp_url = jQuery('#my_otp_url').val();
            if (otp_telephone == "") {
                jQuery('<div class="message-error error message"><div>Please enter mobile number.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                jQuery('.message-error').delay(4000).fadeOut();
            } else if (otp_telephone.length < 10 || otp_telephone.length > 10) {
                jQuery('<div class="message-error error message"><div>Please enter 10 digit mobile number.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                jQuery('.message-error').delay(4000).fadeOut();
            } else {
                $.ajax({
                    showLoader: true,
                    url: my_otp_url,
                    data: {
                        "my_mobile": otp_telephone
                    },
                    type: "POST"
                }).done(function(data) {
                    if (data == "sent") {
                        jQuery('#my_otp').show();
                        jQuery("#mobile_no").prop("readonly", true);
                        jQuery('<div class="message-success success message"><div>OTP Sent successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-success').delay(4000).fadeOut();
                        jQuery('#get_my_otp').hide();
                        jQuery('#my_verify').show();
                    }else if(data == "blocked"){
                        jQuery('<div class="message-error error message"><div>Your Account is locked for 15 minutes.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    }
                });
            }
        });
        jQuery(document).on('click', '.my_resend', function() {
            var otp_telephone = jQuery('#mobile_no').val();
            var my_otp_url = jQuery('#my_otp_url').val();
            $.ajax({
                showLoader: true,
                url: my_otp_url,
                data: {
                    "my_mobile": otp_telephone
                },
                type: "POST"
            }).done(function(data) {
                if (data == "sent") {
                    jQuery('<div class="message-success success message"><div>OTP resent successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                    jQuery('.message-success').delay(4000).fadeOut();
                }
            });
        });
        jQuery(document).on('click', '#my_verify', function(evt) {
            var otp_telephone = jQuery('#mobile_no').val();
            var my_check_otp = jQuery('#my_check_otp').val();
            var my_verify_url = jQuery('#my_verify_url').val();
            if (my_check_otp) {
                $.ajax({
                    showLoader: true,
                    url: my_verify_url,
                    data: {
                        "otp_telephone": otp_telephone,
                        "my_check_otp": my_check_otp
                    },
                    type: "POST"
                }).done(function(data) {
                    if (data == "not matched") {
                        jQuery('<div class="message-error error message"><div>Please enter correct OTP.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    } else if (data == "") {
                        jQuery('<div class="message-success success message"><div>Verified successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-success').delay(4000).fadeOut();
                        var d1 = new Date();
                        localStorage.setItem("guest_contact", otp_telephone + '-' + d1);
                        location.reload();
                    } else if (data == "blocked") {
                        jQuery('<div class="message-error error message"><div>Your account is locked for 15 minutes.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    }
                });
            } else {
                jQuery('<div class="message-error error message"><div>Please enter OTP.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                jQuery('.message-error').delay(4000).fadeOut();
            }

        });
        /*guest checkout verification ends here*/
        jQuery(document).on('click', '#get-otp', function() {
            var mobile = jQuery('#mobile').val();
            var mobile_url = jQuery('#mobile_url').val();
            if (mobile == "") {
                jQuery('<div class="message-error error message"><div>Please enter mobile number.</div></div>').prependTo(".social-login-customer-authentication .block-content");
               jQuery('.message-error').delay(4000).fadeOut();
            } else if (mobile.length < 10 || mobile.length > 10) {
                jQuery('<div class="message-error error message"><div>Please enter 10 digit mobile number.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                jQuery('.message-error').delay(4000).fadeOut();
            } else {
                $.ajax({
                    showLoader: true,
                    url: mobile_url,
                    data: {
                        "mobile": mobile
                    },
                    type: "POST"
                }).done(function(data) {
                    if (data == 'not') {
                        jQuery('<div class="message-error error message"><div>Mobile Number is not registered.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    } else if (data == "sent") {
                        jQuery('.otp').show();
                        jQuery("#mobile").prop("readonly", true);
                        jQuery('<div class="message-success success message"><div>OTP sent successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-success').delay(4000).fadeOut();
                        jQuery('#get-otp').hide();
                        jQuery('#verify').show();
                    }else if (data == "blocked") {
                        jQuery('<div class="message-error error message"><div>Your Mobile No. is locked for 15 minutes.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    }
                });
            }
        });
        jQuery(document).on('click', '.resend', function() {
            var mobile = jQuery('#mobile').val();
            var mobile_url = jQuery('#mobile_url').val();
            $.ajax({
                showLoader: true,
                url: mobile_url,
                data: {
                    "mobile": mobile
                },
                type: "POST"
            }).done(function(data) {
                if (data == "not") {
                    jQuery('<div class="message-error error message"><div>Mobile Number is not registered.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                    jQuery('.message-error').delay(4000).fadeOut();
                } else if (data == "sent") {
                    jQuery('<div class="message-success success message"><div>OTP resent successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                    jQuery('.message-success').delay(4000).fadeOut();
                }
            });
        });
        jQuery(document).on('click', '#verify', function() {
            var mobile = jQuery('#mobile').val();
            var otp = jQuery('#otp').val();
            var verify_url = jQuery('#verify_url').val();
            if (otp) {
                $.ajax({
                    showLoader: true,
                    url: verify_url,
                    data: {
                        "mobile": mobile,
                        "otp": otp
                    },
                    type: "POST"
                }).done(function(data) {
                    if (data == "not matched") {
                        jQuery('<div class="message-error error message"><div>Please enter correct OTP.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    }else if (data == "blocked") {
                        jQuery('<div class="message-error error message"><div>Your account is locked for 15 minutes.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-error').delay(4000).fadeOut();
                    }else if(data == "") {
                        jQuery('<div class="message-success success message"><div>Logged in successfully.</div></div>').prependTo(".social-login-customer-authentication .block-content");
                        jQuery('.message-success').delay(4000).fadeOut();
                        window.location.reload();
                    }
                });
            } else {
               jQuery('<div class="message-error error message"><div>Please enter OTP.</div></div>').prependTo(".social-login-customer-authentication .block-content");
               jQuery('.message-error').delay(4000).fadeOut();
            }
        });

        jQuery(document).on('change', '#mobile_number', function() {
            var mob = jQuery('.cont').val();
            var url_cont = jQuery('.url_cont').val();
            jQuery.ajax({
                showLoader: true,
                url: url_cont,
                data: {
                    "mobile": mob
                },
                type: "POST"
            }).done(function(data) {
                if (data == 'no') {} else if (data == "yes") {
                    jQuery('.mob-error').show();
                    setTimeout(showpanel1, 3000);
                }
            });
        })

        function showpanel1() {
            jQuery('.mob-error').fadeOut();
            jQuery(".cont").val('');
        }

        jQuery("#log_otp").click(function() {
            jQuery(".verify_otp").hide();
            jQuery("#log_otp").hide();
            jQuery(".fieldset.login").hide();
            jQuery(".fieldset.login-otp").show();
        });
        jQuery(".back-btn").click(function() {
            jQuery("#log_otp").show();
            jQuery(".fieldset.login").show();
            jQuery(".fieldset.login-otp").hide();
        });
        /* Login with otp, guest checkout coding ends here */

        /* Healing Card Validation starts here */
        jQuery(".showcart").click(function() {
            jQuery('input.item-qty').each(function(ele, vl) {
                var heal = jQuery(vl).data('cart-item-id');
                var str2 = 'HEA0265';
                if (heal.indexOf(str2) != -1) {
                    jQuery(vl).attr('readonly', true);
                }
            });

            /* Healing card products redirect to cart page */
            jQuery('a.action.edit').each(function(ele, vl) {
                var heal = jQuery(vl).data('cart-item-id');
                var heal_sku_count = heal.split('-').length;
                var str2 = 'HEA0265';

                if (heal.indexOf(str2) != -1) {

                    if (heal_sku_count == '1') {
                        jQuery(this).attr("href", window.location.origin + "/checkout/cart/");
                    }

                }

            });

            /* Healing Card Validation ends here */

            /* subscribe & save products restrict checkout button if total less than 600 starts here */
            var retail_url = $("#retail_url").val();
            $.ajax({
                showLoader: true,
                url: retail_url,
                data: {},
                type: "POST"
            }).done(function(data) {
                if (data == '1') {
                    var obj = $(".error");
                    if (obj.text() == "") {
                        jQuery("#minicart-content-wrapper .block-content .actions:first").prepend('<span class="error" style="color: red;font-size: 13px;">Cart Value should be minimum Rs. 600. please add more products.</span>');
                        jQuery('#top-cart-btn-checkout').prop('disabled', true);
                    }
                } else {}
            });
            /* subscribe & save products restrict checkout button if total less than 600 ends here */
        });

        /* guest checkout user autofill mobile starts here*/
        jQuery(document).on("change", 'input[name="telephone"]', function() {
            var tele = jQuery('input[name="telephone"]').val();
            jQuery('input[name="mobile_otp"]').val(tele);
        });
        /* guest checkout user autofill mobile ends here*/

        /* appending % in filter values on layered navigation starts here */
        jQuery(".Discount .filter-options-content .items li a .price").each(function() {
            var values = jQuery(this).text();
            var element = values.split("₹");
            var per = '%';
            jQuery(this).text(element['1'] + per);
        });
        jQuery(".Package  .filter-options-content .items li a span.price").each(function() {
            var values1 = jQuery(this).text();
            var stat1 = /\d/.test(values1);
            if (stat1 == true) {
                var element1 = values1.split("₹");
                var per1 = 'gm';
                jQuery(this).text(element1['1'] + per1);
            }
        });
        /* appending % in filter values on layered navigation ends here */

        /* Add subscribe & save menu starts here */
        $("#dm li.level0.nav-6").addClass('parent');
        $("#om ul li:nth-child(6)").append('<ul style="display:none;"><li class="level1 nav-1-2 parent"><a href="' + jQuery('#base_url').val() + 'mycard/product/prodlist"><span>Subscription Products</span></a></li></ul>');
        $("#dm li.level0.nav-6").append('<ul class="level0 submenu ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 1148px; left: 0px;" aria-hidden="true"><li class="ui-menu-item all-category"><a href="' + jQuery('#base_url').val() + 'special-offers.html" class="ui-state-focus">All Special Offers</a></li><li class="ui-menu-item all-category"><a href="' + jQuery('#base_url').val() + 'mycard/product/prodlist">Subscription Products</a></li></ul>');
        /* Add subscribe & save menu ends here */
    });

});