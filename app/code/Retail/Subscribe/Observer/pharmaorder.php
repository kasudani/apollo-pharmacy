<?php
namespace Retail\Subscribe\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Bootstrap;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\Order;
 
class pharmaorder implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    protected $storeManager;
    protected $order;  
    protected $_transportBuilder;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager      = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->order = $order;
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }
 
    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {  
        $orderId = $observer->getEvent()->getOrderIds();
        $order = $this->order->load($orderId);

        if(!empty($order->getData('guest_verification'))){
            $shippingAddressObj = $order->getShippingAddress();
            $shippingAddressArray = $shippingAddressObj->getData();
            $firstname="";
            $lastname="";
            $telephone="";
            $street="";
            $city="";
            $email="";
            $region="";
            $postcode="";
            if(!empty($shippingAddressArray)){

                $firstname = $shippingAddressArray['firstname'];
                $lastname = $shippingAddressArray['lastname'];
                $telephone = $shippingAddressArray['telephone'];
                $street = $shippingAddressArray['street'];
                $city = $shippingAddressArray['city'];
                $email = $shippingAddressArray['email'];
                $region = $shippingAddressArray['region'];
                $postcode = $shippingAddressArray['postcode'];
                $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();

                $sql = "Update ordermedicine_data Set oname = '".$firstname." ".$lastname."',oemail = '".$email."', oaddress = '".$street."', ocontact= '".$telephone."',ostate = '".$region."', ocity='".$city."',opincode='".$postcode."', oorderstatus='".$order->getData('status')."' where oemail =".$order->getData('guest_verification');
                $connection->query($sql);
               // echo "done";
            }
        }        
    }
}