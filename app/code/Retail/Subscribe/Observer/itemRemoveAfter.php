<?php
namespace Retail\Subscribe\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Bootstrap;
use \Magento\Framework\App\Config\ScopeConfigInterface;
 
class itemRemoveAfter implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager; 

    protected $_subscribeFactory;  
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Retail\Subscribe\Model\SubscribeFactory $cu,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_subscribeFactory = $cu;
        $this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }
 
    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {  
        $model = $this->_subscribeFactory->create();
        $collection = $model->getCollection();
        $blockIns = $this->_objectManager->get('Retail\Subscribe\Block\Rewrite\Customer\Account\Customer');
        $cust_id = $blockIns->getLoggedinCustomerId();
        $blockInstance = $this->_objectManager->get('Retail\Subscribe\Block\Index\Index');
        $prod = $observer->getQuoteItem()->getProduct();
        $prod_id = $prod->getId();
        $model = $this->_subscribeFactory->create();
        $collection = $model->getCollection();
        $collection->addFieldToFilter('customer_id', $cust_id);
        $collection->addFieldToFilter('product_id', $prod_id);
        $collection->addFieldToFilter('flag', 0);
        foreach($collection as $rtu){
        $model->load($rtu['id'],'id');
        $model->delete();
        }
    }
}
?>