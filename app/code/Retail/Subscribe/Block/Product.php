<?php

namespace Retail\Subscribe\Block;

class Product extends \Magento\Framework\View\Element\Template
{
protected $product;  

  public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ProductFactory $product
    ) {
        $this->product = $product;
        parent::__construct($context);
    }

    public function getProduct($id)
    {
        return $this->product->create()->load($id);
    }

}
?>