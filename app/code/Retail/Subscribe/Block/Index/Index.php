<?php
namespace Retail\Subscribe\Block\Index;

class Index extends \Magento\Framework\View\Element\Template {

    public function __construct(
        \Retail\Subscribe\Model\ResourceModel\Subscribe $resource) {       
         $this->_connection = $resource->getConnection();
    }
            public function getTableData()
        {
            $myTable = $this->_connection->getTableName('product_subscription');
            $sql     = $this->_connection->select('*')->from(
                 ["tn" => $myTable]
            ); 
            $result  = $this->_connection->fetchAll($sql);
            return $result;
        }
}