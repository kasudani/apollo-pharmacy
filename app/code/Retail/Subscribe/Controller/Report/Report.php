<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Retail\Subscribe\Controller\Report;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
 
class Report extends \Magento\Framework\App\Action\Action
{

/**
* @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
*/
protected $_inlineTranslation;
 
protected $_transportBuilder;
 
protected $_template;
 
protected $_storeManager;
    
public function __construct(
    Context $context,
    \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    \Retail\Subscribe\Model\Mail\TransportBuilder $transportBuilder,
    \Magento\Store\Model\StoreManagerInterface $storeManager
) {
     $this->_inlineTranslation = $inlineTranslation;
     $this->_transportBuilder = $transportBuilder;
     $this->_storeManager = $storeManager;
     parent::__construct($context);
}
public function execute()
{
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
     $heading = [
         __('Magento Order Id'),
         __('Order Date'),
         __('Customer Name'),
         __('Customer Mail ID'),
         __('Customer Address'),
         __('Customer City'),
         __('Customer Mobile No'),
         __('Product Code'),
         __('Product Name'),
         __('Payment Mode'),
         __('MRP'),
         __('Special Price'),
         __('Discounted Price'),
         __('Coupon Discount'),
         __('Order QTY'),
         __('COD Charges'),
         __('Shipping Cost'),
         __('Before Discount'),
         __('After Discount'),
         __('FirstTime/Repeat'),
         __('CRM Agent'),
         __('Coupons Code'),
         __('Order Status')
     ];
     $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
     $mediaPath = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath();
     $outputFile = $mediaPath."custom_reports"."ListProducts".date('Ymd_His').".csv";
     $handle = fopen($outputFile, 'w');
     fputcsv($handle, $heading);
        $fromDate = date('Y-m-d H:i:s', strtotime('-30 days'));
        $toDate = date('Y-m-d H:i:s', strtotime('-1 day'));
        $orderDatamodel = $objectManager->get('Magento\Sales\Model\Order')->getCollection()
                              ->addAttributeToFilter('created_at', array('from'=>$fromDate, 'to'=>$toDate));
     foreach($orderDatamodel as $orderDatamodel1){
        $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($orderDatamodel1->getIncrementId());
        $before_discount = $order->getSubtotal()+$order->getShippingAmount()+$order->getPaychargeFee();
           if($order->getPaychargeFee() == null){
            $cod_cost = 0;
              }else{
             $cod_cost = $order->getPaychargeFee();
              }
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodTitle = $method->getTitle(); //Payment Method
        $Coupon_Discount = $order->getBaseDiscountAmount();
        $billingAddressArray = $orderDatamodel1->getBillingAddress()->getData();
        $customer_address = $billingAddressArray['street'].','. $billingAddressArray['city'].','. $billingAddressArray['region'].','. $billingAddressArray['postcode']; //Customer Address(Billing)
        $customer_city = $billingAddressArray['city']; //customer City
        $customer_mobile = $billingAddressArray['telephone']; //customer Mobile
        $orderItems = $order->getAllItems();
            foreach ($orderItems as $item) {
                $discount_price = ($item->getOriginalPrice()-$item->getPrice());
             $row = [
                 $order->getIncrementId(),
                 $order->getCreatedAt(),
                 $order->getCustomerFirstName()." ".$order->getCustomerLastName(),
                 $order->getCustomerEmail(),
                 $customer_address,
                 $customer_city,
                 $customer_mobile,
                 $item->getProduct()->getsku(),
                 $item->getProduct()->getName(),
                 $methodTitle,
                 $item->getOriginalPrice(),
                 $item->getPrice(),
                 $discount_price,
                 $Coupon_Discount,
                 $item->getQtyOrdered(),
                 $cod_cost,
                 $order->getShippingAmount(),
                 $before_discount,
                 $order->getGrandtotal(),
                 "",
                 "",
                 $order->getCouponCode(),
                 $order->getStatusLabel()
             ];
        }     fputcsv($handle, $row);
     }
     $this->sendMail($outputFile);
}
 
public function sendMail($file)
{
    $this->_inlineTranslation->suspend();
    $this->_transportBuilder->setTemplateIdentifier(12)
         ->setTemplateOptions(
            [
                'area' => 'frontend',
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setTemplateVars($emailTemplateVariables)
                ->setFrom([
                    'name' => 'Apollo Pharmacy',
                    'email' => 'manish@theretailinsights.com',
                ])
        ->addTo('manish@theretailinsights.com', 'Manish Mishra')
        ->addAttachment(file_get_contents($file)); //Attachment goes here.
        try {
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        } catch (\Exception $e) {
            echo $e->getMessage(); 
        }
    }
}
?>