<?php
namespace Retail\Subscribe\Controller\Grid;

ob_start();

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Framework\App\Action\Action
{

     /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    protected $_subscribeFactory;  

    protected $_transportBuilder;

    protected $_messageManager;
    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */

	public function __construct(
		\Magento\Framework\Message\ManagerInterface $messageManager,
	    \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
		\Magento\Framework\ObjectManagerInterface $objectManager,
	    \Retail\Subscribe\Model\SubscribeFactory $cu)
	{
		$this->_messageManager = $messageManager;
		$this->_subscribeFactory = $cu;
        $this->_transportBuilder = $transportBuilder;
		$this->_objectManager = $objectManager;
		parent::__construct($context);
	}

  public function execute()
  {
		$prod_id = $this->getRequest()->getParam('id');
		$blockIns = $this->_objectManager->get('Retail\Subscribe\Block\Rewrite\Customer\Account\Customer');
		$cust_id = $blockIns->getLoggedinCustomerId();
		$model = $this->_subscribeFactory->create();
		$collection = $model->getCollection();
		$collection->addFieldToFilter('customer_id', $cust_id);
        $collection->addFieldToFilter('product_id', $prod_id);
        foreach($collection as $rtu){
        	$model->load($rtu['id'],'id');
        	$model->delete();
        	// $model->setFlag(0);
        	// $model->save();
        }
        $transport = $this->_transportBuilder->setTemplateIdentifier(5)
                ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
                ->setTemplateVars(
                    [
                         'product_name'=>"product->getName()",
                         'base_url'=>"baseUrl",
                         'checkout_url'=>"checkout/cart"
                    ]
                )
                ->setFrom('general')
                ->addTo('manish@theretailinsights.com')
                ->getTransport();
                $transport->sendMessage();
        $message = 'You have sucessfully unsubscribed.';
        $this->_messageManager->addSuccess($message);
        $this->_redirect('subscribe/customer/index');
  }
}

?>