<?php
namespace Retail\Subscribe\Controller\Send;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Email extends \Magento\Framework\App\Action\Action
{

  protected $storeManager;
  protected $customerRepositoryInterface;
  protected $quoteModel;
  protected $productRepository;
  protected $_subscribeFactory; 
  protected $_transportBuilder;

   public function __construct(
    /* Add below dependencies */
    \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
    \Magento\Quote\Model\Quote $quoteModel,
    \Retail\Subscribe\Model\SubscribeFactory $cu,
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
) {
    
    $this->storeManager                 = $storeManager;
    $this->_transportBuilder            = $transportBuilder;
    $this->_customerRepositoryInterface = $customerRepositoryInterface;
    $this->quoteModel                   = $quoteModel;
    $this->_subscribeFactory            = $cu;
    $this->productRepository            = $productRepository;
    parent::__construct($context);
}
    
    public function execute() 
    {   
      try {
          $current_date = date("Y/m/d");
          $final_notify_date =  date('Y/m/d', strtotime("+25 days"));
          $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
          $model = $this->_subscribeFactory->create();
          $collection = $model->getCollection();
          $collection->addFieldToFilter('flag', 1);
          if(!empty($collection->getData())){
            foreach($collection as $rtu){
              $notify_date = $rtu['last_notify_date'];
            if(strtotime($current_date) == strtotime($notify_date)){
                $model->load($rtu['id'],'id');
                $cust_id = $rtu['customer_id'];
                $prod_id = $rtu['product_id'];
                $qty =     $rtu['qty'];
                $dis =     $rtu['discount'];
                $sub_period = $rtu['sub_period'];
                $final_sub_period = $sub_period -1;
                $customer = $this->_customerRepositoryInterface->getById($cust_id);
                $cust_email = $customer->getEmail();
                $quote    = $this->quoteModel->loadByCustomer($customer);
                if (!$quote->getId()) {
                    $quote->setCustomer($customer);
                    $quote->setIsActive(1);
                    $quote->setStoreId($this->storeManager->getStore()->getId());
                }
                $product = $this->productRepository->getById($prod_id);
                $quoteItem = $quote->addProduct($product, $qty);
                $items = $quote->getAllItems();
                $custom_price = $product->getFinalPrice();
                $setting_price = ($custom_price*$dis)/100;
                $setting_price = $custom_price - $setting_price;
                $quoteItem->setCustomPrice($setting_price);
                $quoteItem->setOriginalCustomPrice($setting_price);
                $quote->collectTotals()->save();
                
                $transport = $this->_transportBuilder->setTemplateIdentifier(5)
                ->setTemplateOptions(['area' => 'frontend', 'store' =>1])
                ->setTemplateVars(
                    [
                         'product_name'=>$product->getName(),
                         'base_url'=>$baseUrl,
                         'checkout_url'=>$baseUrl."checkout/cart"
                    ]
                )
                ->setFrom('general')
                ->addTo($cust_email)
                ->getTransport();
                $transport->sendMessage();
                  if($final_sub_period == 0){
                  $model->setData('sub_period',$final_sub_period);
                  $model->setData('last_notify_date',$final_notify_date);
                  $model->setData('flag',0);
                  $model->save();
                  }else{
                  $model->setData('sub_period',$final_sub_period);
                  $model->setData('last_notify_date',$final_notify_date);
                  $model->save(); 
                  }
           }
        }
      }
        else{
          echo "empty";
        }
      } catch (\Exception $e) {
          echo $e->getMessage(); 
          echo "error";
      }
    }  
}
?>