<?php

namespace Retail\Subscribe\Controller\Discount;

class Calculate extends \Magento\Framework\App\Action\Action
{
    protected $_jsonFactory;
    protected $_productRepository;
    protected $_productFactory;
    protected $_calculateDiscountHelper;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Retail\Subscribe\Helper\CalculateDiscount $calculateDiscount
    ) {
        parent::__construct($context);
        $this->_jsonFactory = $jsonFactory;
        $this->_productRepository = $productRepository;
        $this->_productFactory = $productFactory;
        $this->_calculateDiscountHelper = $calculateDiscount;
    }

   

    public function execute() {

        $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/discountController.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $product_id  = $this->getRequest()->getParam('product_id');
        $product_qty = $this->getRequest()->getParam('product_qty');
        $product_sku = $this->getRequest()->getParam('product_sku');

        $logger->info('product sku ==> ' . $product_sku);
        if($product_sku != ''){
            // $product_id = $this->_productRepository->
            $product1 = $this->_productFactory->create();
            $product_id = $product1->getIdBySku($product_sku);
            $logger->info('product id ==> ' . $product_id);
        }

        $logger->info('caculate discount...');

        $logger->info("product id ==> " . $product_id);
        $logger->info("product qty ==> " . $product_qty);

        # if values are null than exit
        if($product_id == '' || $product_qty == '') {
            $logger->info(" product details are null ... ");
            $response = $this->_jsonFactory->create()->setData([
                'success' => false,
                'id' => $product_id,
                'qty' => $product_qty,
                'discounterPrice'  => '',
                'appliedDiscount'  => ''
            ]);
        }
        else {
            # else if correct are than calculate discount

            $logger->info("called discount helper ... ");
            $discountedResult = $this->_calculateDiscountHelper->calculate($product_id,$product_qty);
            $logger->info($discountedResult);

            $response = $this->_jsonFactory->create()->setData([
                'success' => true,
                'id' => $product_id,
                'qty' => $product_qty,
                'discounterPrice'  => $discountedResult['final_price'],
                'appliedDiscount'  => $discountedResult['applied_discount']
            ]);
        }
        
        $logger->info("response");

        return $response;
    }
}