<?php
namespace Retail\Subscribe\Controller\Index;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Test extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    
    protected $_objectManager;
    
    protected $_subscribeFactory;
    
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Retail\Subscribe\Model\SubscribeFactory $cu)
    {
        $this->_pageFactory      = $pageFactory;
        $this->_subscribeFactory = $cu;
        parent::__construct($context);
    }
    public function execute()
    {
        $data          = $this->getRequest()->getPostValue();
        $current_date  = date("Y/m/d");
        $notify_date   = date('Y/m/d', strtotime("+25 days"));
        $end_date      = date('Y/m/d', strtotime("+" . $data['sub_period'] . " months", strtotime($current_date)));
        $data['start_date'] = $current_date;
        $data['last_notify_date'] = $notify_date;
        $data['end_date'] = $end_date;

        if(isset($data['id'])){
            $model      = $this->_subscribeFactory->create();
            $model->load($data['id'], 'id');
            $model->setData('start_date', $current_date);
            $model->setData('last_notify_date', $notify_date);
            $model->setData('end_date', $end_date);
            $model->setData('sub_period', $data['sub_period']);
            $model->setData('discount', $data['discount']);
            $model->save();
            echo "updated";
            die;
        }
        $data_prod_id  = $data['product_id'];
        $data_cust_id  = $data['customer_id'];
        $quoteInstance = $this->_objectManager->get('Retail\Subscribe\Block\QuoteItem');
        $quote         = $quoteInstance->getCheckoutSession()->getQuote();
        $items         = $quote->getAllItems();
        $save_ids      = array();
        foreach ($items as $item) {
            $quote_prodid = $item->getProductId();
            array_push($save_ids, $quote_prodid);
        }
        $model      = $this->_subscribeFactory->create();
        $collection = $model->getCollection();
        if (in_array($data_prod_id, $save_ids)) { //if product is in cart;
            $collection->addFieldToFilter('customer_id', $data_cust_id);
            $collection->addFieldToFilter('product_id', $data_prod_id);
            if (empty($collection->getData())) {
                $model->setData($data)->save();
                echo "submitted";
            } else {
                foreach ($collection as $rtu) {
                    $model->load($rtu['id'], 'id');
                    if ($model->getFlag() == 0) {
                        $model->setFlag(0);
                        $model->setData('start_date', $current_date);
                        $model->setData('last_notify_date', $notify_date);
                        $model->setData('end_date', $end_date);
                        $model->setData('qty', $data['qty']);
                        $model->setData('sub_period', $data['sub_period']);
                        $model->setData('discount', $data['discount']);
                        $model->save();
                        echo "already";
                    } elseif ($model->getFlag() == 1) {
                        echo "alreadys";
                    }
                }
            }
        } elseif (!in_array($data_prod_id, $save_ids)) { //if product is not in cart
            $collection->addFieldToFilter('customer_id', $data_cust_id);
            $collection->addFieldToFilter('product_id', $data_prod_id);
            if (empty($collection->getData())) {
                $model->setData($data)->save();
                echo "submitted";
            } else {
                foreach ($collection as $rtu) {
                    $model->load($rtu['id'], 'id');
                    if ($model->getFlag() == 0) {
                        $model->setFlag(0);
                        $model->setData('start_date', $current_date);
                        $model->setData('last_notify_date', $notify_date);
                        $model->setData('end_date', $end_date);
                        $model->setData('qty', $data['qty']);
                        $model->setData('sub_period', $data['sub_period']);
                        $model->setData('discount', $data['discount']);
                        $model->save();
                        echo "submitted";
                    } elseif ($model->getFlag() == 1) {
                        echo "alreadys";
                    }
                }
            }
        }
    }
}
