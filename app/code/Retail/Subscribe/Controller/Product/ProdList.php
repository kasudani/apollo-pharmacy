<?php
namespace Retail\Subscribe\Controller\Product;
use Retail\Subscribe\Block\Product\CustomList;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class ProdList extends Action
{
    protected $pageFactory;
    protected $productCollection;
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->productCollection = $collectionFactory->create();
        parent::__construct($context);
    }
    public function execute()
    {
        $result = $this->pageFactory->create();
        $this->productCollection->addFieldToSelect('*');
        $this->productCollection->addAttributeToFilter('ss_plan','488');
        $collection = $this->productCollection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        $list = $result->getLayout()->getBlock('custom.product.list');
        $list->setProductCollection($collection);
        return $result;
    }
}