<?php
namespace Retail\Subscribe\Model\ResourceModel;

class Subscribe extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('product_subscription', 'id');
    }
}
