<?php

namespace Retail\Subscribe\Model\ResourceModel\Subscribe;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Retail\Subscribe\Model\Subscribe', 'Retail\Subscribe\Model\ResourceModel\Subscribe');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }

}
