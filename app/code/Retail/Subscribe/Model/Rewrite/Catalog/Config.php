<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Retail\Subscribe\Model\Rewrite\Catalog;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Config extends \Magento\Catalog\Model\Config
{    

        public function getAttributeUsedForSortByArray()
            {
                unset($options['position']);
                unset($options['name']);
                unset($options['price']);
               // $options['position'] = __('Position');
                $options['rating_summary'] = __('Rating');
                $options['price'] =  __('Price: Low to High');
                $options['price_desc'] = __('Price: High to Low');
                $options['name'] =  __('Products Name: A-Z');
                $options['name_desc'] = __('Products Name: Z-A');
               /* foreach ($this->getAttributesUsedForSortBy() as $attribute) {
                    /* @var $attribute \Magento\Eav\Model\Entity\Attribute\AbstractAttribute */
                   /* $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
                }*/  
                return $options;
            }
}
